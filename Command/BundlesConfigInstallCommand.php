<?php

namespace IiMedias\VoteBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use IiMedias\AdminBundle\Model\LocaleQuery;

class BundlesConfigInstallCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('bundles:pbfvote:config')
            ->setDescription('Récupère la configuration pour l\'administration de iimedias')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        MenuElementQuery::addElement('admin', 'pbfvote', 'admin.menu.pbfvote', 'address-book-o', 'pbf_vote_admin_index', array());

        $translations['admin.menu.pbfvote']                   = array('fr' => 'Concours', 'en' => 'Contest', 'Competición');
        $translations['admin.pbfvote.index.breadcrumb']       = array('fr' => 'Concours', 'en' => 'Contest', 'es' => 'Competición');
        $translations['admin.pbfvote.index.voters.current']   = array('fr' => 'Concours en cours', 'en' => 'Current Contest', 'es' => 'Current');
        $translations['admin.pbfvote.index.voters.title']     = array('fr' => 'Votants', 'en' => 'Voters', 'es' => 'Jurado');
        $translations['admin.pbfvote.index.voters.add']       = array('fr' => 'Ajouter', 'en' => 'Add', 'es' => 'Añadir');
        $translations['admin.pbfvote.index.candidates.title'] = array('fr' => 'Participants', 'en' => 'Candidates', 'es' => 'Parejas');
        $translations['admin.pbfvote.index.candidates.add']   = array('fr' => 'Ajouter', 'en' => 'Add', 'es' => 'Añadir');
        $translations['admin.pbfvote.index.contests.title']   = array('fr' => 'Concours', 'en' => 'Contests', 'es' => 'competiciones');
        $translations['admin.pbfvote.index.contests.add']     = array('fr' => 'Ajouter', 'en' => 'Add', 'es' => 'Añadir');
        LocaleQuery::addTranslations($translations);
    }

}
