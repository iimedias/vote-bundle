<?php

namespace IiMedias\VoteBundle\Command;

use IiMedias\VoteBundle\Model\Voter;
use IiMedias\VoteBundle\Model\VoterQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use IiMedias\AdminBundle\Model\LocaleQuery;
use \Exception;

class VoteDisConnectCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('IiMedias:vote:disconnect')
            ->addArgument('username')
            ->setDescription('Inscription du juré')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $voter = VoterQuery::create()
                ->filterByUsername($input->getArgument('username'))
                ->findOne();

            if (!is_null($voter)) {
                $voter
                    ->setConnection(false)
                    ->save();
            }
            $output->write(1);
        }
        catch(Exception $e) {
            $output->write(0);
        }
    }

}
