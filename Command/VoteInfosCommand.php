<?php

namespace IiMedias\VoteBundle\Command;

use IiMedias\VoteBundle\Model\ContestCandidate;
use IiMedias\VoteBundle\Model\ContestQuery;
use IiMedias\VoteBundle\Model\ScoreQuery;
use IiMedias\VoteBundle\Model\ContestCandidateQuery;
use IiMedias\VoteBundle\Model\Voter;
use IiMedias\VoteBundle\Model\VoterQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use IiMedias\AdminBundle\Model\LocaleQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use \Exception;

class VoteInfosCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:vote:infos')
//            ->addArgument('username')
            ->setDescription('Inscription du juré')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $jsonResponse = array(
            'currenttab' => 'wait',
            'voters' => array(
                'nopending' => array(),
                'online'    => array(),
                'offline'   => array(),
                'pending'   => array(),
            ),
            'contest' => array(),
            'contestCandidates' => array(),
            'notes' => array(),
            'finalNote' => array(
                'name' => null,
                'slug' => null,
                'scene' => null,
                'technic' => null,
                'couple' => null,
                'music' => null,
                'style' => null,
                'total' => null,
            ),
            'sort' => array(),
        );

        $voters = VoterQuery::create()
            ->orderByUsername()
            ->find()
        ;

        foreach ($voters as $voter) {
            $voterInfos = array(
                'id'          => $voter->getId(),
                'username'    => $voter->getUsername(),
                'displayname' => $voter->getDisplayName(),
                'country'     => $voter->getCountry(),
                'online'      => $voter->getConnection(),
                'enable'      => $voter->getEnable(),
            );
            if ($voter->getEnable() == true && $voter->getConnection() == true) {
                $jsonResponse['voters']['nopending'][$voter->getSlug()] = $voterInfos;
                $jsonResponse['voters']['online'][$voter->getSlug()] = $voterInfos;
            }
            elseif ($voter->getEnable() == true && $voter->getConnection() == false) {
                $jsonResponse['voters']['nopending'][$voter->getSlug()] = $voterInfos;
                $jsonResponse['voters']['offline'][$voter->getSlug()] = $voterInfos;
            }
            else {
                $jsonResponse['voters']['pending'][$voter->getSlug()] = $voterInfos;
            }
        }

        $currentContest = ContestQuery::create()
            ->filterByCurrent(true)
            ->findOne();
        if (!is_null($currentContest)) {
            $jsonResponse['currenttab'] = 'contest';
            $jsonResponse['contest']    = array(
                'name' => $currentContest->getName(),
                'slug' => $currentContest->getSlug(),
            );
            $contestCandidates = ContestCandidateQuery::create()
                ->filterByEnable(true)
                ->filterByContest($currentContest)
                ->find();
            foreach ($contestCandidates as $contestCandidate) {
                $jsonResponse['contestCandidates'][$contestCandidate->getContest()->getSlug()][$contestCandidate->getCandidate()->getSlug()] = array(
                    'name'    => $contestCandidate->getCandidate()->getName(),
                    'slug'    => $contestCandidate->getCandidate()->getSlug(),
                    'country' => $contestCandidate->getCandidate()->getCountry(),
                    'notes'   => array(
                        'full' => array(
                            'scene' => $contestCandidate->getSceneScore(),
                            'technic' => $contestCandidate->getTechnicScore(),
                            'couple' => $contestCandidate->getCoupleScore(),
                            'music' => $contestCandidate->getMusicScore(),
                            'style' => $contestCandidate->getStyleScore(),
                            'total' => $contestCandidate->getTotalScore(),
                        ),
                    ),
                );
                foreach ($voters as $voter) {
                    $scoreArray = array(
                        'scene' => 0,
                        'technic' => 0,
                        'couple' => 0,
                        'music' => 0,
                        'style' => 0,
                        'total' => 0,
                    );
                    $voterScore = ScoreQuery::create()
                        ->filterByVoter($voter)
                        ->filterByContestCandidate($contestCandidate)
                        ->findOne()
                    ;
                    if (!is_null($voterScore)) {
                        $scoreArray = array(
                            'scene' => $voterScore->getSceneScore(),
                            'technic' => $voterScore->getTechnicScore(),
                            'couple' => $voterScore->getCoupleScore(),
                            'music' => $voterScore->getMusicScore(),
                            'style' => $voterScore->getStyleScore(),
                            'total' => $voterScore->getTotalScore(),
                        );
                    }

                    $jsonResponse['contestCandidates'][$contestCandidate->getContest()->getSlug()][$contestCandidate->getCandidate()->getSlug()]['notes'][$voter->getSlug()] = $scoreArray;
                }
            }
        }

        $currentDanceContestCandidate = ContestCandidateQuery::create()
            ->useContestQuery()
                ->filterByCurrent(true)
            ->endUse()
            ->filterByShowDance(true)
            ->_or()
            ->filterByShowVote(true)
            ->_or()
            ->filterByShowResult(true)
            ->_or()
            ->filterByShowSort(true)
            ->findOne();
        if (!is_null($currentDanceContestCandidate)) {
            $jsonResponse['currenttab'] = 'candidatedance';
            if ($currentDanceContestCandidate->getShowVote() == true) {
                $jsonResponse['currenttab'] = 'candidatevote';
            }
            if ($currentDanceContestCandidate->getShowResult() == true) {
                $jsonResponse['currenttab'] = 'candidateresult';
            }
            if ($currentDanceContestCandidate->getShowSort() == true) {
                $jsonResponse['currenttab'] = 'candidatepodium';
            }
            $jsonResponse['candidate']  = array(
                'id'      => $currentDanceContestCandidate->getCandidate()->getId(),
                'name'    => $currentDanceContestCandidate->getCandidate()->getName(),
                'slug'    => $currentDanceContestCandidate->getCandidate()->getSlug(),
                'country' => $currentDanceContestCandidate->getCandidate()->getCountry(),
            );
        }
        
        $currentResultsContestCandidate = ContestCandidateQuery::create()
            ->filterByShowResult(true)
            ->findOne();
//        if (!is_null($currentResultsContestCandidate) && $currentResultsContestCandidate->getTotalScore() > 0) {
//            $jsonResponse['currenttab'] = 'candidateresults';
//            $jsonResponse['finalNote'] = array(
//                'name' => $currentResultsContestCandidate->getCandidate()->getName(),
//                'slug' => $currentResultsContestCandidate->getCandidate()->getName(),
//                'scene' => $currentResultsContestCandidate->getSceneScore(),
//                'technic' => $currentResultsContestCandidate->getTechnicScore(),
//                'couple' => $currentResultsContestCandidate->getCoupleScore(),
//                'music' => $currentResultsContestCandidate->getMusicScore(),
//                'style' => $currentResultsContestCandidate->getStyleScore(),
//                'total' => $currentResultsContestCandidate->getTotalScore(),
//            );
//        }
        
        $currentSortContestCandidate = ContestCandidateQuery::create()
            ->filterByShowSort(true)
            ->findOne();
//        if (is_null($currentSortContestCandidate)) {
//            $jsonResponse['currenttab'] = 'candidatesort';
//            $sortContestCandidates = ContestCandidateQuery::create()
//                ->filterByTotalScore(0, Criteria::GREATER_THAN)
//                ->orderByTotalScore(Criteria::DESC)
//                ->find();
//            $count = 0;
//            foreach ($sortContestCandidates as $sortContestCandidate) {
//                $count++;
//                $jsonResponse['sort'][] = array(
//                    'place'  => $count,
//                    'name'   => $sortContestCandidate->getCandidate()->getName(),
//                    'points' => $sortContestCandidate->getTotalScore(),
//                );
//            }
//            //dump($sortsContestCandidate->count());
//            
//        }

        $output->write(json_encode($jsonResponse));
    }
}
