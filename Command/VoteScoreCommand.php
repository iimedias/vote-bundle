<?php

namespace IiMedias\VoteBundle\Command;

use IiMedias\VoteBundle\Model\ScoreQuery;
use IiMedias\VoteBundle\Model\ContestCandidateQuery;
use IiMedias\VoteBundle\Model\Voter;
use IiMedias\VoteBundle\Model\VoterQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use IiMedias\AdminBundle\Model\LocaleQuery;
use \Exception;

class VoteScoreCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:vote:vote')
            ->addArgument('username')
            ->addArgument('type')
            ->addArgument('score')
            ->setDescription('Enregistrement d\'une note')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currentContestCandidate = ContestCandidateQuery::create()
            ->filterByShowDance(true)
            ->_or()
            ->filterByShowVote(true)
            ->findOne();

        $score = ScoreQuery::create()
            ->filterByContestCandidate($currentContestCandidate)
            ->useVoterQuery()
                ->filterByUsername($input->getArgument('username'))
            ->endUse()
            ->findOne()
        ;

        switch ($input->getArgument('type')) {
            case 'scene':
                $score
                    ->setSceneScore($input->getArgument('score'))
                    ->save();
                break;
            case 'technic':
                $score
                    ->setTechnicScore($input->getArgument('score'))
                    ->save();
                break;
            case 'couple':
                $score
                    ->setCoupleScore($input->getArgument('score'))
                    ->save();
                break;
            case 'music':
                $score
                    ->setMusicScore($input->getArgument('score'))
                    ->save();
                break;
            case 'style':
                $score
                    ->setStyleScore($input->getArgument('score'))
                    ->save();
                break;
        }
        dump($score->getSceneScore());
        if ($score->getSceneScore() != 0 && $score->getTechnicScore() != 0 && $score->getCoupleScore() != 0 && $score->getMusicScore() != 0 && $score->getStyleScore() != 0) {
            $score
                ->setTotalScore($score->getSceneScore() + $score->getTechnicScore() + $score->getCoupleScore() + $score->getMusicScore() + $score->getStyleScore())
                ->save()
            ;
        }
    }
}
