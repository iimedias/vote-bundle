<?php

namespace IiMedias\VoteBundle\Controller;

use IiMedias\VoteBundle\Form\Type\CandidateType;
use IiMedias\VoteBundle\Form\Type\ContestType;
use IiMedias\VoteBundle\Form\Type\VoterType;
use IiMedias\VoteBundle\Model\CandidateQuery;
use IiMedias\VoteBundle\Model\ContestQuery;
use IiMedias\VoteBundle\Model\VoterQuery;
use IiMedias\VoteBundle\Model\Candidate;
use IiMedias\VoteBundle\Model\Contest;
use IiMedias\VoteBundle\Model\ContestCandidate;
use IiMedias\VoteBundle\Model\ContestCandidateQuery;
use IiMedias\VoteBundle\Model\Score;
use IiMedias\VoteBundle\Model\ScoreQuery;
use IiMedias\VoteBundle\Model\Voter;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use IiMedias\StaffBundle\Model\StaffGroup;
use IiMedias\StaffBundle\Model\StaffGroupQuery;
use IiMedias\StaffBundle\Form\Type\StaffGroupType;
use IiMedias\StaffBundle\Model\StaffElement;
use IiMedias\StaffBundle\Model\StaffElementQuery;
use IiMedias\StaffBundle\Form\Type\StaffElementType;
use \DateTime;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AdminController extends Controller
{
    /**
     * Administration des votes
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/vote", name="iimedias_vote_admin_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $voters = VoterQuery::create()
            ->filterByEnable(true)
            ->orderByUsername(Criteria::ASC)
            ->find();
        $candidates = CandidateQuery::create()
            ->orderByEnable(Criteria::DESC)
            ->orderByName(Criteria::ASC)
            ->find();
        $contests = ContestQuery::create()
            ->orderByEnable(Criteria::DESC)
            ->orderByCurrent(Criteria::DESC)
            ->orderByName(Criteria::ASC)
            ->find();
        $currentContest = ContestQuery::create()
            ->filterByCurrent(true)
            ->findOne();
        if (!is_null($currentContest)) {
            $contestCandidates = ContestCandidateQuery::create()
                ->filterByContest($currentContest)
                ->filterByEnable(true)
                ->find();
        }
        else {
            $contestCandidates = array();
        }

        return $this->render('IiMediasVoteBundle:Admin:index.html.twig', array(
                'currentNav'        => 'vote',
                'voters'            => $voters,
                'candidates'        => $candidates,
                'contests'          => $contests,
                'currentContest'    => $currentContest,
                'contestCandidates' => $contestCandidates,
        ));
    }

    /**
     * Edition d'un votant
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/vote/voter/{voterId}/edit", name="iimedias_vote_admin_editvoter", requirements={"_locale"="\w{2}", "voterId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("voter", class="IiMedias\VoteBundle\Model\Voter", options={"mapping"={"voterId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function editVoter(Request $request, Voter $voter)
    {
        $form = $this->createForm(
            VoterType::class,
            $voter,
            array(
                'action' => $request->getUri(),
            )
        );

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            $voter->save();
            return $this->redirect(
                $this->generateUrl(
                    'iimedias_vote_admin_index'
                )
            );
        }

        return $this->render('IiMediasVoteBundle:Admin:editVoter.html.twig', array(
                'currentNav' => 'vote',
                'voter'      => $voter,
                'form'       => $form->createView(),
        ));
    }

    /**
     * Formulaire d'un candidat
     *
     * @access public
     * @since 1.0.0 16/07/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param string $mode
     * @Route("/admin/{_locale}/vote/candidate/create", name="iimedias_vote_admin_createcandidate", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "mode"="create", "candidateId"=0})
     * @Route("/admin/{_locale}/vote/candidate/{candidateId}/edit", name="iimedias_vote_admin_editcandidate", requirements={"_locale"="\w{2}", "candidateId"="\d+"}, defaults={"_locale"="fr", "mode"="edit"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function formCandidate(Request $request, $mode, $candidateId)
    {
        switch ($mode) {
            case 'edit':
                $candidate = CandidateQuery::create()
                    ->filterById($candidateId)
                    ->findOne();
                break;
            case 'create':
            default:
            $candidate = new Candidate();
                break;
        }

        $form = $this->createForm(
            CandidateType::class,
            $candidate,
            array(
                'action' => $request->getUri(),
            )
        );

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            $candidate->save();
            return $this->redirectToRoute('iimedias_vote_admin_index');
        }

        return $this->render(
            'IiMediasVoteBundle:Admin:formCandidate.html.twig',
            array(
                'currentNav' => 'vote',
                'mode'       => $mode,
                'candidate'  => $candidate,
                'form'       => $form->createView(),
            )
        );
    }

    /**
     * Formulaire d'un concours
     *
     * @access public
     * @since 1.0.0 16/07/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param string $mode
     * @Route("/admin/{_locale}/vote/contest/create", name="iimedias_vote_admin_createcontest", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "mode"="create", "contestId"=0})
     * @Route("/admin/{_locale}/vote/contest/{contestId}/edit", name="iimedias_vote_admin_editcontest", requirements={"_locale"="\w{2}", "contestId"="\d+"}, defaults={"_locale"="fr", "mode"="edit"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function formContest(Request $request, $mode, $contestId)
    {
        switch ($mode) {
            case 'edit':
                $contest = ContestQuery::create()
                    ->filterById($contestId)
                    ->findOne();
                break;
            case 'create':
            default:
                $contest = new Contest();
                break;
        }

        $contestCandidates = ContestCandidateQuery::create()
            ->filterByContest($contest)
            ->filterByEnable(true)
            ->find();

        $candidateList = array();
        foreach ($contestCandidates as $contestCandidate) {
            $candidateList[] = $contestCandidate->getCandidate()->getId();
        }
        $contest->setCandidateList($candidateList);

        $form = $this->createForm(
            ContestType::class,
            $contest,
            array(
                'action' => $request->getUri(),
            )
        );

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            $contest->save();

            foreach ($contestCandidates as $contestCandidate) {
                $contestCandidate
                    ->setEnable(false)
                    ->save();
            }

            foreach ($contest->getCandidateList() as $candidateId) {
                $contestCandidate = ContestCandidateQuery::create()
                    ->filterByContest($contest)
                    ->filterByCandidateId($candidateId)
                    ->findOne();
                if (is_null($contestCandidate)) {
                    $contestCandidate = new ContestCandidate();
                    $contestCandidate
                        ->setContest($contest)
                        ->setCandidateId($candidateId)
                        ->save();
                }
                $contestCandidate
                    ->setEnable(true)
                    ->save();
            }

            return $this->redirectToRoute('iimedias_vote_admin_index');
        }

        return $this->render(
            'IiMediasVoteBundle:Admin:formContest.html.twig',
            array(
                'currentNav' => 'vote',
                'mode'       => $mode,
                'contest'    => $contest,
                'form'       => $form->createView(),
            )
        );
    }

    /**
     * Lancer ou stoper un concours
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/vote/contest/{contestId}/{mode}", name="iimedias_vote_admin_startstopcontest", requirements={"_locale"="\w{2}", "contestId"="\d+", "mode"="(start|stop)"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function startStopContest($contestId, $mode)
    {
        $contests = ContestQuery::create()
            ->find();

        foreach ($contests as $contest) {
            $newCurrent = ($contest->getId() == $contestId && $mode == 'start') ? true : false;
            $contest
                ->setCurrent($newCurrent)
                ->save();
        }

        return $this->redirectToRoute('iimedias_vote_admin_index');
    }

    /**
     * Contest candidate action
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/vote/contest/candidate/{contestCandidateId}/{mode}", name="iimedias_vote_admin_contestcandidateaction", requirements={"_locale"="\w{2}", "contestCandidateId"="\d+", "mode"="(dance|vote|results|calc|sort|clean)"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function contestCandidateAction($contestCandidateId, $mode) {
        $contestCandidates       = ContestCandidateQuery::create()
//            ->useContestQuery()
//                ->filterByCurrent(true)
//            ->endUse()
            ->find();
        foreach ($contestCandidates as $contestCandidate) {
            if ($contestCandidate->getId() != $contestCandidateId) {
                $contestCandidate
                    ->setShowDance(false)
                    ->setShowVote(false)
                    ->setShowResult(false)
                    ->setShowSort(false)
                    ->save();
            }
        }
        $contestCandidates       = ContestCandidateQuery::create()
            ->useContestQuery()
                ->filterByCurrent(true)
            ->endUse()
            ->find();

        $currentContestCandidate = ContestCandidateQuery::create()
            ->filterById($contestCandidateId)
            ->findOne();
        switch ($mode) {
            case 'sort':
                foreach ($contestCandidates as $contestCandidate) {
                    $scores = $contestCandidate->getScores();
                    $finalScoreScene = 0;
                    $finalScoreTechnic = 0;
                    $finalScoreCouple = 0;
                    $finalScoreMusic = 0;
                    $finalScoreStyle = 0;
                    $finalScoreTotal = 0;
                    foreach ($scores as $score) {
                        $finalScoreScene += $score->getSceneScore();
                        $finalScoreTechnic += $score->getTechnicScore();
                        $finalScoreCouple += $score->getCoupleScore();
                        $finalScoreMusic += $score->getMusicScore();
                        $finalScoreStyle += $score->getStyleScore();
                        $finalScoreTotal += $score->getTotalScore();
                    }
                    $currentContestCandidate
                        ->setSceneScore($finalScoreScene)
                        ->setTechnicScore($finalScoreTechnic)
                        ->setCoupleScore($finalScoreCouple)
                        ->setMusicScore($finalScoreMusic)
                        ->setStyleScore($finalScoreStyle)
                        ->setTotalScore($finalScoreTotal)
                        ->save();
                }
                if ($contestCandidateId != 0) {
                    $currentContestCandidate
                        ->setShowDance(false)
                        ->setShowVote(false)
                        ->setShowResult(false)
                        ->setShowSort(true)
                        ->save();
                }
                break;
            case 'results':
                if ($contestCandidateId != 0) {
                    $scores = $currentContestCandidate->getScores();
                    $finalScoreScene = 0;
                    $finalScoreTechnic = 0;
                    $finalScoreCouple = 0;
                    $finalScoreMusic = 0;
                    $finalScoreStyle = 0;
                    $finalScoreTotal = 0;
                    foreach ($scores as $score) {
                        $finalScoreScene += $score->getSceneScore();
                        $finalScoreTechnic += $score->getTechnicScore();
                        $finalScoreCouple += $score->getCoupleScore();
                        $finalScoreMusic += $score->getMusicScore();
                        $finalScoreStyle += $score->getStyleScore();
                        $finalScoreTotal += $score->getTotalScore();
                    }
                    $currentContestCandidate
                        ->setSceneScore($finalScoreScene)
                        ->setTechnicScore($finalScoreTechnic)
                        ->setCoupleScore($finalScoreCouple)
                        ->setMusicScore($finalScoreMusic)
                        ->setStyleScore($finalScoreStyle)
                        ->setTotalScore($finalScoreTotal)
                        ->setShowDance(false)
                        ->setShowVote(false)
                        ->setShowResult(true)
                        ->setShowSort(false)
                        ->save();
                }
                break;
            case 'vote':
                if ($contestCandidateId != 0) {
                    $currentContestCandidate
                        ->setShowDance(false)
                        ->setShowVote(true)
                        ->setShowResult(false)
                        ->setShowSort(false)
                        ->save();
                }
                break;
            case 'dance':
                if ($contestCandidateId != 0) {
                    $currentContestCandidate
                        ->setShowDance(true)
                        ->setShowVote(false)
                        ->setShowResult(false)
                        ->setShowSort(false)
                        ->save();
                    $voters = VoterQuery::create()
                        ->filterByEnable(true)
                        ->orderByUsername(Criteria::ASC)
                        ->find();
                    foreach ($voters as $voter) {
                        $score = ScoreQuery::create()
                            ->filterByContestCandidate($currentContestCandidate)
                            ->filterByVoter($voter)
                            ->findOne();
                        if (is_null($score)) {
                            $score = new Score();
                            $score
                                ->setContestCandidate($currentContestCandidate)
                                ->setVoter($voter)
                                ->save();
                        }
                    }
                }
                break;
            case 'clean':
            default:
        }

        return $this->redirect(
            $this->generateUrl(
                'iimedias_vote_admin_index'
            )
        );
    }
}
