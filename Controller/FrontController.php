<?php

namespace IiMedias\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use IiMedias\StaffBundle\Model\StaffGroup;
use IiMedias\StaffBundle\Model\StaffGroupQuery;
use IiMedias\StaffBundle\Form\Type\StaffGroupType;
use IiMedias\StaffBundle\Model\StaffElement;
use IiMedias\StaffBundle\Model\StaffElementQuery;
use IiMedias\StaffBundle\Form\Type\StaffElementType;
use \DateTime;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class FrontController extends Controller
{
    /**
     * Administration des votes
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/{_locale}/vote", name="pbf_vote_front_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('IiMediasVoteBundle:Front:index.html.twig', array(
                'currentNav'  => 'pbfvote',
        ));
    }

    /**
     * Administration des votes
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/{_locale}/vote/video", name="pbf_vote_front_video", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function video()
    {
        return $this->render('IiMediasVoteBundle:Front:public.html.twig', array(
            'currentNav'  => 'pbfvote',
        ));
    }
}
