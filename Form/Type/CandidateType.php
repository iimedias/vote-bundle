<?php

namespace IiMedias\VoteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class CandidateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'required'   => true,
                    'label'      => 'Nom affiché',
                    'attr'       => array(
                        'placeholder' => 'Nom affiché',
                    ),
                )
            )
            ->add(
                'country',
                CountryType::class,
                array(
                    'required'   => false,
                    'label'      => 'Pays',
//                    'choices'    => array(
//                        'Belgique'               => 'be',
//                        'Allemagne'              => 'de',
//                        'République Dominicaine' => 'do',
//                        'Espagne'                => 'es',
//                        'France'                 => 'fr',
//                        'Israël'                 => 'il',
//                        'Italie'                 => 'it',
//                    ),
                    'label_attr' => array(
                        'class' => 'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'attr'       => array(
                        'placeholder' => 'Pays',
                        'class'       => 'form-control',
                    ),
                )
            )
            ->add(
                'enable',
                ChoiceType::class,
                array(
                    'required'   => true,
                    'label'      => 'Autoriser',
                    'choices'    => array(
                        'Oui' => 1,
                        'Non' => 0,
                    ),
                    'label_attr' => array(
                        'class' => 'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'attr'       => array(
                        'placeholder' => 'Autoriser',
                        'class'       => 'form-control',
                    ),
                )
            )
            ->add(
                'submit',
                SubmitType::class,
                array(
                    'label' => 'Enregistrer',
                    'attr'  => array(
                        'placeholder' => 'Enregistrer',
                        'class'       => 'btn btn-primary',
                    ),
                )
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\VoteBundle\Model\Candidate',
                'name'       => 'candidate',
        ));
    }
}
