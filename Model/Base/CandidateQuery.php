<?php

namespace IiMedias\VoteBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VoteBundle\Model\Candidate as ChildCandidate;
use IiMedias\VoteBundle\Model\CandidateQuery as ChildCandidateQuery;
use IiMedias\VoteBundle\Model\Map\CandidateTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vote_candidate_votcdd' table.
 *
 *
 *
 * @method     ChildCandidateQuery orderById($order = Criteria::ASC) Order by the votcdd_id column
 * @method     ChildCandidateQuery orderByName($order = Criteria::ASC) Order by the votcdd_name column
 * @method     ChildCandidateQuery orderByCountry($order = Criteria::ASC) Order by the votcdd_country column
 * @method     ChildCandidateQuery orderByEnable($order = Criteria::ASC) Order by the votcdd_enable column
 * @method     ChildCandidateQuery orderByCreatedAt($order = Criteria::ASC) Order by the votcdd_created_at column
 * @method     ChildCandidateQuery orderByUpdatedAt($order = Criteria::ASC) Order by the votcdd_updated_at column
 * @method     ChildCandidateQuery orderByVotcddSlug($order = Criteria::ASC) Order by the votcdd_slug column
 *
 * @method     ChildCandidateQuery groupById() Group by the votcdd_id column
 * @method     ChildCandidateQuery groupByName() Group by the votcdd_name column
 * @method     ChildCandidateQuery groupByCountry() Group by the votcdd_country column
 * @method     ChildCandidateQuery groupByEnable() Group by the votcdd_enable column
 * @method     ChildCandidateQuery groupByCreatedAt() Group by the votcdd_created_at column
 * @method     ChildCandidateQuery groupByUpdatedAt() Group by the votcdd_updated_at column
 * @method     ChildCandidateQuery groupByVotcddSlug() Group by the votcdd_slug column
 *
 * @method     ChildCandidateQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCandidateQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCandidateQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCandidateQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCandidateQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCandidateQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCandidateQuery leftJoinContestCandidate($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContestCandidate relation
 * @method     ChildCandidateQuery rightJoinContestCandidate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContestCandidate relation
 * @method     ChildCandidateQuery innerJoinContestCandidate($relationAlias = null) Adds a INNER JOIN clause to the query using the ContestCandidate relation
 *
 * @method     ChildCandidateQuery joinWithContestCandidate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContestCandidate relation
 *
 * @method     ChildCandidateQuery leftJoinWithContestCandidate() Adds a LEFT JOIN clause and with to the query using the ContestCandidate relation
 * @method     ChildCandidateQuery rightJoinWithContestCandidate() Adds a RIGHT JOIN clause and with to the query using the ContestCandidate relation
 * @method     ChildCandidateQuery innerJoinWithContestCandidate() Adds a INNER JOIN clause and with to the query using the ContestCandidate relation
 *
 * @method     \IiMedias\VoteBundle\Model\ContestCandidateQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCandidate findOne(ConnectionInterface $con = null) Return the first ChildCandidate matching the query
 * @method     ChildCandidate findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCandidate matching the query, or a new ChildCandidate object populated from the query conditions when no match is found
 *
 * @method     ChildCandidate findOneById(int $votcdd_id) Return the first ChildCandidate filtered by the votcdd_id column
 * @method     ChildCandidate findOneByName(string $votcdd_name) Return the first ChildCandidate filtered by the votcdd_name column
 * @method     ChildCandidate findOneByCountry(string $votcdd_country) Return the first ChildCandidate filtered by the votcdd_country column
 * @method     ChildCandidate findOneByEnable(string $votcdd_enable) Return the first ChildCandidate filtered by the votcdd_enable column
 * @method     ChildCandidate findOneByCreatedAt(string $votcdd_created_at) Return the first ChildCandidate filtered by the votcdd_created_at column
 * @method     ChildCandidate findOneByUpdatedAt(string $votcdd_updated_at) Return the first ChildCandidate filtered by the votcdd_updated_at column
 * @method     ChildCandidate findOneByVotcddSlug(string $votcdd_slug) Return the first ChildCandidate filtered by the votcdd_slug column *

 * @method     ChildCandidate requirePk($key, ConnectionInterface $con = null) Return the ChildCandidate by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCandidate requireOne(ConnectionInterface $con = null) Return the first ChildCandidate matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCandidate requireOneById(int $votcdd_id) Return the first ChildCandidate filtered by the votcdd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCandidate requireOneByName(string $votcdd_name) Return the first ChildCandidate filtered by the votcdd_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCandidate requireOneByCountry(string $votcdd_country) Return the first ChildCandidate filtered by the votcdd_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCandidate requireOneByEnable(string $votcdd_enable) Return the first ChildCandidate filtered by the votcdd_enable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCandidate requireOneByCreatedAt(string $votcdd_created_at) Return the first ChildCandidate filtered by the votcdd_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCandidate requireOneByUpdatedAt(string $votcdd_updated_at) Return the first ChildCandidate filtered by the votcdd_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCandidate requireOneByVotcddSlug(string $votcdd_slug) Return the first ChildCandidate filtered by the votcdd_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCandidate[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCandidate objects based on current ModelCriteria
 * @method     ChildCandidate[]|ObjectCollection findById(int $votcdd_id) Return ChildCandidate objects filtered by the votcdd_id column
 * @method     ChildCandidate[]|ObjectCollection findByName(string $votcdd_name) Return ChildCandidate objects filtered by the votcdd_name column
 * @method     ChildCandidate[]|ObjectCollection findByCountry(string $votcdd_country) Return ChildCandidate objects filtered by the votcdd_country column
 * @method     ChildCandidate[]|ObjectCollection findByEnable(string $votcdd_enable) Return ChildCandidate objects filtered by the votcdd_enable column
 * @method     ChildCandidate[]|ObjectCollection findByCreatedAt(string $votcdd_created_at) Return ChildCandidate objects filtered by the votcdd_created_at column
 * @method     ChildCandidate[]|ObjectCollection findByUpdatedAt(string $votcdd_updated_at) Return ChildCandidate objects filtered by the votcdd_updated_at column
 * @method     ChildCandidate[]|ObjectCollection findByVotcddSlug(string $votcdd_slug) Return ChildCandidate objects filtered by the votcdd_slug column
 * @method     ChildCandidate[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CandidateQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VoteBundle\Model\Base\CandidateQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VoteBundle\\Model\\Candidate', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCandidateQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCandidateQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCandidateQuery) {
            return $criteria;
        }
        $query = new ChildCandidateQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCandidate|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CandidateTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CandidateTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCandidate A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT votcdd_id, votcdd_name, votcdd_country, votcdd_enable, votcdd_created_at, votcdd_updated_at, votcdd_slug FROM vote_candidate_votcdd WHERE votcdd_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCandidate $obj */
            $obj = new ChildCandidate();
            $obj->hydrate($row);
            CandidateTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCandidate|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the votcdd_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE votcdd_id = 1234
     * $query->filterById(array(12, 34)); // WHERE votcdd_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE votcdd_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_ID, $id, $comparison);
    }

    /**
     * Filter the query on the votcdd_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE votcdd_name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE votcdd_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the votcdd_country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE votcdd_country = 'fooValue'
     * $query->filterByCountry('%fooValue%', Criteria::LIKE); // WHERE votcdd_country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the votcdd_enable column
     *
     * Example usage:
     * <code>
     * $query->filterByEnable('fooValue');   // WHERE votcdd_enable = 'fooValue'
     * $query->filterByEnable('%fooValue%', Criteria::LIKE); // WHERE votcdd_enable LIKE '%fooValue%'
     * </code>
     *
     * @param     string $enable The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByEnable($enable = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($enable)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_ENABLE, $enable, $comparison);
    }

    /**
     * Filter the query on the votcdd_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE votcdd_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE votcdd_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE votcdd_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the votcdd_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE votcdd_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE votcdd_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE votcdd_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the votcdd_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByVotcddSlug('fooValue');   // WHERE votcdd_slug = 'fooValue'
     * $query->filterByVotcddSlug('%fooValue%', Criteria::LIKE); // WHERE votcdd_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $votcddSlug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByVotcddSlug($votcddSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($votcddSlug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_SLUG, $votcddSlug, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VoteBundle\Model\ContestCandidate object
     *
     * @param \IiMedias\VoteBundle\Model\ContestCandidate|ObjectCollection $contestCandidate the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCandidateQuery The current query, for fluid interface
     */
    public function filterByContestCandidate($contestCandidate, $comparison = null)
    {
        if ($contestCandidate instanceof \IiMedias\VoteBundle\Model\ContestCandidate) {
            return $this
                ->addUsingAlias(CandidateTableMap::COL_VOTCDD_ID, $contestCandidate->getCandidateId(), $comparison);
        } elseif ($contestCandidate instanceof ObjectCollection) {
            return $this
                ->useContestCandidateQuery()
                ->filterByPrimaryKeys($contestCandidate->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContestCandidate() only accepts arguments of type \IiMedias\VoteBundle\Model\ContestCandidate or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContestCandidate relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function joinContestCandidate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContestCandidate');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContestCandidate');
        }

        return $this;
    }

    /**
     * Use the ContestCandidate relation ContestCandidate object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VoteBundle\Model\ContestCandidateQuery A secondary query class using the current class as primary query
     */
    public function useContestCandidateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContestCandidate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContestCandidate', '\IiMedias\VoteBundle\Model\ContestCandidateQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCandidate $candidate Object to remove from the list of results
     *
     * @return $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function prune($candidate = null)
    {
        if ($candidate) {
            $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_ID, $candidate->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vote_candidate_votcdd table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CandidateTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CandidateTableMap::clearInstancePool();
            CandidateTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CandidateTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CandidateTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CandidateTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CandidateTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CandidateTableMap::COL_VOTCDD_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CandidateTableMap::COL_VOTCDD_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CandidateTableMap::COL_VOTCDD_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CandidateTableMap::COL_VOTCDD_CREATED_AT);
    }

    // sluggable behavior

    /**
     * Filter the query on the slug column
     *
     * @param     string $slug The value to use as filter.
     *
     * @return    $this|ChildCandidateQuery The current query, for fluid interface
     */
    public function filterBySlug($slug)
    {
        return $this->addUsingAlias(CandidateTableMap::COL_VOTCDD_SLUG, $slug, Criteria::EQUAL);
    }

    /**
     * Find one object based on its slug
     *
     * @param     string $slug The value to use as filter.
     * @param     ConnectionInterface $con The optional connection object
     *
     * @return    ChildCandidate the result, formatted by the current formatter
     */
    public function findOneBySlug($slug, $con = null)
    {
        return $this->filterBySlug($slug)->findOne($con);
    }

} // CandidateQuery
