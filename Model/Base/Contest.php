<?php

namespace IiMedias\VoteBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VoteBundle\Model\Contest as ChildContest;
use IiMedias\VoteBundle\Model\ContestCandidate as ChildContestCandidate;
use IiMedias\VoteBundle\Model\ContestCandidateQuery as ChildContestCandidateQuery;
use IiMedias\VoteBundle\Model\ContestQuery as ChildContestQuery;
use IiMedias\VoteBundle\Model\Map\ContestCandidateTableMap;
use IiMedias\VoteBundle\Model\Map\ContestTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'vote_contest_votcts' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VoteBundle.Model.Base
 */
abstract class Contest implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VoteBundle\\Model\\Map\\ContestTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the votcts_id field.
     *
     * @var        int
     */
    protected $votcts_id;

    /**
     * The value for the votcts_name field.
     *
     * @var        string
     */
    protected $votcts_name;

    /**
     * The value for the votcts_current field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $votcts_current;

    /**
     * The value for the votcts_enable field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $votcts_enable;

    /**
     * The value for the votcts_created_at field.
     *
     * @var        DateTime
     */
    protected $votcts_created_at;

    /**
     * The value for the votcts_updated_at field.
     *
     * @var        DateTime
     */
    protected $votcts_updated_at;

    /**
     * The value for the votcts_slug field.
     *
     * @var        string
     */
    protected $votcts_slug;

    /**
     * @var        ObjectCollection|ChildContestCandidate[] Collection to store aggregation of ChildContestCandidate objects.
     */
    protected $collContestCandidates;
    protected $collContestCandidatesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContestCandidate[]
     */
    protected $contestCandidatesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->votcts_current = false;
        $this->votcts_enable = true;
    }

    /**
     * Initializes internal state of IiMedias\VoteBundle\Model\Base\Contest object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Contest</code> instance.  If
     * <code>obj</code> is an instance of <code>Contest</code>, delegates to
     * <code>equals(Contest)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Contest The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [votcts_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->votcts_id;
    }

    /**
     * Get the [votcts_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->votcts_name;
    }

    /**
     * Get the [votcts_current] column value.
     *
     * @return boolean
     */
    public function getCurrent()
    {
        return $this->votcts_current;
    }

    /**
     * Get the [votcts_current] column value.
     *
     * @return boolean
     */
    public function isCurrent()
    {
        return $this->getCurrent();
    }

    /**
     * Get the [votcts_enable] column value.
     *
     * @return boolean
     */
    public function getEnable()
    {
        return $this->votcts_enable;
    }

    /**
     * Get the [votcts_enable] column value.
     *
     * @return boolean
     */
    public function isEnable()
    {
        return $this->getEnable();
    }

    /**
     * Get the [optionally formatted] temporal [votcts_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->votcts_created_at;
        } else {
            return $this->votcts_created_at instanceof \DateTimeInterface ? $this->votcts_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [votcts_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->votcts_updated_at;
        } else {
            return $this->votcts_updated_at instanceof \DateTimeInterface ? $this->votcts_updated_at->format($format) : null;
        }
    }

    /**
     * Get the [votcts_slug] column value.
     *
     * @return string
     */
    public function getVotctsSlug()
    {
        return $this->votcts_slug;
    }

    /**
     * Set the value of [votcts_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votcts_id !== $v) {
            $this->votcts_id = $v;
            $this->modifiedColumns[ContestTableMap::COL_VOTCTS_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [votcts_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->votcts_name !== $v) {
            $this->votcts_name = $v;
            $this->modifiedColumns[ContestTableMap::COL_VOTCTS_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Sets the value of the [votcts_current] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object (for fluent API support)
     */
    public function setCurrent($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votcts_current !== $v) {
            $this->votcts_current = $v;
            $this->modifiedColumns[ContestTableMap::COL_VOTCTS_CURRENT] = true;
        }

        return $this;
    } // setCurrent()

    /**
     * Sets the value of the [votcts_enable] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object (for fluent API support)
     */
    public function setEnable($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votcts_enable !== $v) {
            $this->votcts_enable = $v;
            $this->modifiedColumns[ContestTableMap::COL_VOTCTS_ENABLE] = true;
        }

        return $this;
    } // setEnable()

    /**
     * Sets the value of [votcts_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->votcts_created_at !== null || $dt !== null) {
            if ($this->votcts_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->votcts_created_at->format("Y-m-d H:i:s.u")) {
                $this->votcts_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContestTableMap::COL_VOTCTS_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [votcts_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->votcts_updated_at !== null || $dt !== null) {
            if ($this->votcts_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->votcts_updated_at->format("Y-m-d H:i:s.u")) {
                $this->votcts_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContestTableMap::COL_VOTCTS_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Set the value of [votcts_slug] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object (for fluent API support)
     */
    public function setVotctsSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->votcts_slug !== $v) {
            $this->votcts_slug = $v;
            $this->modifiedColumns[ContestTableMap::COL_VOTCTS_SLUG] = true;
        }

        return $this;
    } // setVotctsSlug()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->votcts_current !== false) {
                return false;
            }

            if ($this->votcts_enable !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ContestTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votcts_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ContestTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votcts_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ContestTableMap::translateFieldName('Current', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votcts_current = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ContestTableMap::translateFieldName('Enable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votcts_enable = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ContestTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->votcts_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ContestTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->votcts_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ContestTableMap::translateFieldName('VotctsSlug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votcts_slug = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = ContestTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VoteBundle\\Model\\Contest'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContestTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildContestQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collContestCandidates = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Contest::setDeleted()
     * @see Contest::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildContestQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            // sluggable behavior

            if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_SLUG) && $this->getVotctsSlug()) {
                $this->setVotctsSlug($this->makeSlugUnique($this->getVotctsSlug()));
            } elseif (!$this->getVotctsSlug()) {
                $this->setVotctsSlug($this->createSlug());
            }
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ContestTableMap::COL_VOTCTS_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(ContestTableMap::COL_VOTCTS_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ContestTableMap::COL_VOTCTS_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ContestTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->contestCandidatesScheduledForDeletion !== null) {
                if (!$this->contestCandidatesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VoteBundle\Model\ContestCandidateQuery::create()
                        ->filterByPrimaryKeys($this->contestCandidatesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contestCandidatesScheduledForDeletion = null;
                }
            }

            if ($this->collContestCandidates !== null) {
                foreach ($this->collContestCandidates as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ContestTableMap::COL_VOTCTS_ID] = true;
        if (null !== $this->votcts_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ContestTableMap::COL_VOTCTS_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'votcts_id';
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'votcts_name';
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_CURRENT)) {
            $modifiedColumns[':p' . $index++]  = 'votcts_current';
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_ENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'votcts_enable';
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'votcts_created_at';
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'votcts_updated_at';
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'votcts_slug';
        }

        $sql = sprintf(
            'INSERT INTO vote_contest_votcts (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'votcts_id':
                        $stmt->bindValue($identifier, $this->votcts_id, PDO::PARAM_INT);
                        break;
                    case 'votcts_name':
                        $stmt->bindValue($identifier, $this->votcts_name, PDO::PARAM_STR);
                        break;
                    case 'votcts_current':
                        $stmt->bindValue($identifier, (int) $this->votcts_current, PDO::PARAM_INT);
                        break;
                    case 'votcts_enable':
                        $stmt->bindValue($identifier, (int) $this->votcts_enable, PDO::PARAM_INT);
                        break;
                    case 'votcts_created_at':
                        $stmt->bindValue($identifier, $this->votcts_created_at ? $this->votcts_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'votcts_updated_at':
                        $stmt->bindValue($identifier, $this->votcts_updated_at ? $this->votcts_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'votcts_slug':
                        $stmt->bindValue($identifier, $this->votcts_slug, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContestTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getCurrent();
                break;
            case 3:
                return $this->getEnable();
                break;
            case 4:
                return $this->getCreatedAt();
                break;
            case 5:
                return $this->getUpdatedAt();
                break;
            case 6:
                return $this->getVotctsSlug();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Contest'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Contest'][$this->hashCode()] = true;
        $keys = ContestTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getCurrent(),
            $keys[3] => $this->getEnable(),
            $keys[4] => $this->getCreatedAt(),
            $keys[5] => $this->getUpdatedAt(),
            $keys[6] => $this->getVotctsSlug(),
        );
        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collContestCandidates) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contestCandidates';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'vote_contest_candidate_votccds';
                        break;
                    default:
                        $key = 'ContestCandidates';
                }

                $result[$key] = $this->collContestCandidates->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VoteBundle\Model\Contest
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContestTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VoteBundle\Model\Contest
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setCurrent($value);
                break;
            case 3:
                $this->setEnable($value);
                break;
            case 4:
                $this->setCreatedAt($value);
                break;
            case 5:
                $this->setUpdatedAt($value);
                break;
            case 6:
                $this->setVotctsSlug($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ContestTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCurrent($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setEnable($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCreatedAt($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUpdatedAt($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setVotctsSlug($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ContestTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_ID)) {
            $criteria->add(ContestTableMap::COL_VOTCTS_ID, $this->votcts_id);
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_NAME)) {
            $criteria->add(ContestTableMap::COL_VOTCTS_NAME, $this->votcts_name);
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_CURRENT)) {
            $criteria->add(ContestTableMap::COL_VOTCTS_CURRENT, $this->votcts_current);
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_ENABLE)) {
            $criteria->add(ContestTableMap::COL_VOTCTS_ENABLE, $this->votcts_enable);
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_CREATED_AT)) {
            $criteria->add(ContestTableMap::COL_VOTCTS_CREATED_AT, $this->votcts_created_at);
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_UPDATED_AT)) {
            $criteria->add(ContestTableMap::COL_VOTCTS_UPDATED_AT, $this->votcts_updated_at);
        }
        if ($this->isColumnModified(ContestTableMap::COL_VOTCTS_SLUG)) {
            $criteria->add(ContestTableMap::COL_VOTCTS_SLUG, $this->votcts_slug);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildContestQuery::create();
        $criteria->add(ContestTableMap::COL_VOTCTS_ID, $this->votcts_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (votcts_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VoteBundle\Model\Contest (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setCurrent($this->getCurrent());
        $copyObj->setEnable($this->getEnable());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setVotctsSlug($this->getVotctsSlug());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getContestCandidates() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContestCandidate($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VoteBundle\Model\Contest Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ContestCandidate' == $relationName) {
            $this->initContestCandidates();
            return;
        }
    }

    /**
     * Clears out the collContestCandidates collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContestCandidates()
     */
    public function clearContestCandidates()
    {
        $this->collContestCandidates = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContestCandidates collection loaded partially.
     */
    public function resetPartialContestCandidates($v = true)
    {
        $this->collContestCandidatesPartial = $v;
    }

    /**
     * Initializes the collContestCandidates collection.
     *
     * By default this just sets the collContestCandidates collection to an empty array (like clearcollContestCandidates());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContestCandidates($overrideExisting = true)
    {
        if (null !== $this->collContestCandidates && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContestCandidateTableMap::getTableMap()->getCollectionClassName();

        $this->collContestCandidates = new $collectionClassName;
        $this->collContestCandidates->setModel('\IiMedias\VoteBundle\Model\ContestCandidate');
    }

    /**
     * Gets an array of ChildContestCandidate objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildContest is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContestCandidate[] List of ChildContestCandidate objects
     * @throws PropelException
     */
    public function getContestCandidates(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContestCandidatesPartial && !$this->isNew();
        if (null === $this->collContestCandidates || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContestCandidates) {
                // return empty collection
                $this->initContestCandidates();
            } else {
                $collContestCandidates = ChildContestCandidateQuery::create(null, $criteria)
                    ->filterByContest($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContestCandidatesPartial && count($collContestCandidates)) {
                        $this->initContestCandidates(false);

                        foreach ($collContestCandidates as $obj) {
                            if (false == $this->collContestCandidates->contains($obj)) {
                                $this->collContestCandidates->append($obj);
                            }
                        }

                        $this->collContestCandidatesPartial = true;
                    }

                    return $collContestCandidates;
                }

                if ($partial && $this->collContestCandidates) {
                    foreach ($this->collContestCandidates as $obj) {
                        if ($obj->isNew()) {
                            $collContestCandidates[] = $obj;
                        }
                    }
                }

                $this->collContestCandidates = $collContestCandidates;
                $this->collContestCandidatesPartial = false;
            }
        }

        return $this->collContestCandidates;
    }

    /**
     * Sets a collection of ChildContestCandidate objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contestCandidates A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildContest The current object (for fluent API support)
     */
    public function setContestCandidates(Collection $contestCandidates, ConnectionInterface $con = null)
    {
        /** @var ChildContestCandidate[] $contestCandidatesToDelete */
        $contestCandidatesToDelete = $this->getContestCandidates(new Criteria(), $con)->diff($contestCandidates);


        $this->contestCandidatesScheduledForDeletion = $contestCandidatesToDelete;

        foreach ($contestCandidatesToDelete as $contestCandidateRemoved) {
            $contestCandidateRemoved->setContest(null);
        }

        $this->collContestCandidates = null;
        foreach ($contestCandidates as $contestCandidate) {
            $this->addContestCandidate($contestCandidate);
        }

        $this->collContestCandidates = $contestCandidates;
        $this->collContestCandidatesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContestCandidate objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContestCandidate objects.
     * @throws PropelException
     */
    public function countContestCandidates(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContestCandidatesPartial && !$this->isNew();
        if (null === $this->collContestCandidates || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContestCandidates) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContestCandidates());
            }

            $query = ChildContestCandidateQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByContest($this)
                ->count($con);
        }

        return count($this->collContestCandidates);
    }

    /**
     * Method called to associate a ChildContestCandidate object to this object
     * through the ChildContestCandidate foreign key attribute.
     *
     * @param  ChildContestCandidate $l ChildContestCandidate
     * @return $this|\IiMedias\VoteBundle\Model\Contest The current object (for fluent API support)
     */
    public function addContestCandidate(ChildContestCandidate $l)
    {
        if ($this->collContestCandidates === null) {
            $this->initContestCandidates();
            $this->collContestCandidatesPartial = true;
        }

        if (!$this->collContestCandidates->contains($l)) {
            $this->doAddContestCandidate($l);

            if ($this->contestCandidatesScheduledForDeletion and $this->contestCandidatesScheduledForDeletion->contains($l)) {
                $this->contestCandidatesScheduledForDeletion->remove($this->contestCandidatesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContestCandidate $contestCandidate The ChildContestCandidate object to add.
     */
    protected function doAddContestCandidate(ChildContestCandidate $contestCandidate)
    {
        $this->collContestCandidates[]= $contestCandidate;
        $contestCandidate->setContest($this);
    }

    /**
     * @param  ChildContestCandidate $contestCandidate The ChildContestCandidate object to remove.
     * @return $this|ChildContest The current object (for fluent API support)
     */
    public function removeContestCandidate(ChildContestCandidate $contestCandidate)
    {
        if ($this->getContestCandidates()->contains($contestCandidate)) {
            $pos = $this->collContestCandidates->search($contestCandidate);
            $this->collContestCandidates->remove($pos);
            if (null === $this->contestCandidatesScheduledForDeletion) {
                $this->contestCandidatesScheduledForDeletion = clone $this->collContestCandidates;
                $this->contestCandidatesScheduledForDeletion->clear();
            }
            $this->contestCandidatesScheduledForDeletion[]= clone $contestCandidate;
            $contestCandidate->setContest(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Contest is new, it will return
     * an empty collection; or if this Contest has previously
     * been saved, it will retrieve related ContestCandidates from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Contest.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContestCandidate[] List of ChildContestCandidate objects
     */
    public function getContestCandidatesJoinCandidate(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContestCandidateQuery::create(null, $criteria);
        $query->joinWith('Candidate', $joinBehavior);

        return $this->getContestCandidates($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->votcts_id = null;
        $this->votcts_name = null;
        $this->votcts_current = null;
        $this->votcts_enable = null;
        $this->votcts_created_at = null;
        $this->votcts_updated_at = null;
        $this->votcts_slug = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collContestCandidates) {
                foreach ($this->collContestCandidates as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collContestCandidates = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ContestTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildContest The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ContestTableMap::COL_VOTCTS_UPDATED_AT] = true;

        return $this;
    }

    // sluggable behavior

    /**
     * Wrap the setter for slug value
     *
     * @param   string
     * @return  $this|Contest
     */
    public function setSlug($v)
    {
        return $this->setVotctsSlug($v);
    }

    /**
     * Wrap the getter for slug value
     *
     * @return  string
     */
    public function getSlug()
    {
        return $this->getVotctsSlug();
    }

    /**
     * Create a unique slug based on the object
     *
     * @return string The object slug
     */
    protected function createSlug()
    {
        $slug = $this->createRawSlug();
        $slug = $this->limitSlugSize($slug);
        $slug = $this->makeSlugUnique($slug);

        return $slug;
    }

    /**
     * Create the slug from the appropriate columns
     *
     * @return string
     */
    protected function createRawSlug()
    {
        return '' . $this->cleanupSlugPart($this->getName()) . '';
    }

    /**
     * Cleanup a string to make a slug of it
     * Removes special characters, replaces blanks with a separator, and trim it
     *
     * @param     string $slug        the text to slugify
     * @param     string $replacement the separator used by slug
     * @return    string               the slugified text
     */
    protected static function cleanupSlugPart($slug, $replacement = '-')
    {
        // transliterate
        if (function_exists('iconv')) {
            $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
        }

        // lowercase
        if (function_exists('mb_strtolower')) {
            $slug = mb_strtolower($slug);
        } else {
            $slug = strtolower($slug);
        }

        // remove accents resulting from OSX's iconv
        $slug = str_replace(array('\'', '`', '^'), '', $slug);

        // replace non letter or digits with separator
        $slug = preg_replace('/[^\w\/]+/u', $replacement, $slug);

        // trim
        $slug = trim($slug, $replacement);

        if (empty($slug)) {
            return 'n-a';
        }

        return $slug;
    }


    /**
     * Make sure the slug is short enough to accommodate the column size
     *
     * @param    string $slug            the slug to check
     *
     * @return string                        the truncated slug
     */
    protected static function limitSlugSize($slug, $incrementReservedSpace = 3)
    {
        // check length, as suffix could put it over maximum
        if (strlen($slug) > (255 - $incrementReservedSpace)) {
            $slug = substr($slug, 0, 255 - $incrementReservedSpace);
        }

        return $slug;
    }


    /**
     * Get the slug, ensuring its uniqueness
     *
     * @param    string $slug            the slug to check
     * @param    string $separator       the separator used by slug
     * @param    int    $alreadyExists   false for the first try, true for the second, and take the high count + 1
     * @return   string                   the unique slug
     */
    protected function makeSlugUnique($slug, $separator = '-', $alreadyExists = false)
    {
        if (!$alreadyExists) {
            $slug2 = $slug;
        } else {
            $slug2 = $slug . $separator;
        }

        $adapter = \Propel\Runtime\Propel::getServiceContainer()->getAdapter('default');
        $col = 'q.VotctsSlug';
        $compare = $alreadyExists ? $adapter->compareRegex($col, '?') : sprintf('%s = ?', $col);

        $query = \IiMedias\VoteBundle\Model\ContestQuery::create('q')
            ->where($compare, $alreadyExists ? '^' . $slug2 . '[0-9]+$' : $slug2)
            ->prune($this)
        ;

        if (!$alreadyExists) {
            $count = $query->count();
            if ($count > 0) {
                return $this->makeSlugUnique($slug, $separator, true);
            }

            return $slug2;
        }

        $adapter = \Propel\Runtime\Propel::getServiceContainer()->getAdapter('default');
        // Already exists
        $object = $query
            ->addDescendingOrderByColumn($adapter->strLength('votcts_slug'))
            ->addDescendingOrderByColumn('votcts_slug')
        ->findOne();

        // First duplicate slug
        if (null == $object) {
            return $slug2 . '1';
        }

        $slugNum = substr($object->getVotctsSlug(), strlen($slug) + 1);
        if (0 == $slugNum[0]) {
            $slugNum[0] = 1;
        }

        return $slug2 . ($slugNum + 1);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
