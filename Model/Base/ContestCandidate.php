<?php

namespace IiMedias\VoteBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VoteBundle\Model\Candidate as ChildCandidate;
use IiMedias\VoteBundle\Model\CandidateQuery as ChildCandidateQuery;
use IiMedias\VoteBundle\Model\Contest as ChildContest;
use IiMedias\VoteBundle\Model\ContestCandidate as ChildContestCandidate;
use IiMedias\VoteBundle\Model\ContestCandidateQuery as ChildContestCandidateQuery;
use IiMedias\VoteBundle\Model\ContestQuery as ChildContestQuery;
use IiMedias\VoteBundle\Model\Score as ChildScore;
use IiMedias\VoteBundle\Model\ScoreQuery as ChildScoreQuery;
use IiMedias\VoteBundle\Model\Map\ContestCandidateTableMap;
use IiMedias\VoteBundle\Model\Map\ScoreTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'vote_contest_candidate_votccd' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VoteBundle.Model.Base
 */
abstract class ContestCandidate implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VoteBundle\\Model\\Map\\ContestCandidateTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the votccd_id field.
     *
     * @var        int
     */
    protected $votccd_id;

    /**
     * The value for the votccd_votcts_id field.
     *
     * @var        int
     */
    protected $votccd_votcts_id;

    /**
     * The value for the votccd_votcdd_id field.
     *
     * @var        int
     */
    protected $votccd_votcdd_id;

    /**
     * The value for the votccd_enable field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $votccd_enable;

    /**
     * The value for the votccd_show_dance field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $votccd_show_dance;

    /**
     * The value for the votccd_show_vote field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $votccd_show_vote;

    /**
     * The value for the votccd_show_result field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $votccd_show_result;

    /**
     * The value for the votccd_show_sort field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $votccd_show_sort;

    /**
     * The value for the votccd_scene_score field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $votccd_scene_score;

    /**
     * The value for the votccd_technic_score field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $votccd_technic_score;

    /**
     * The value for the votccd_couple_score field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $votccd_couple_score;

    /**
     * The value for the votccd_music_score field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $votccd_music_score;

    /**
     * The value for the votccd_style_score field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $votccd_style_score;

    /**
     * The value for the votccd_total_score field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $votccd_total_score;

    /**
     * The value for the votccd_created_at field.
     *
     * @var        DateTime
     */
    protected $votccd_created_at;

    /**
     * The value for the votccd_updated_at field.
     *
     * @var        DateTime
     */
    protected $votccd_updated_at;

    /**
     * @var        ChildContest
     */
    protected $aContest;

    /**
     * @var        ChildCandidate
     */
    protected $aCandidate;

    /**
     * @var        ObjectCollection|ChildScore[] Collection to store aggregation of ChildScore objects.
     */
    protected $collScores;
    protected $collScoresPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildScore[]
     */
    protected $scoresScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->votccd_enable = true;
        $this->votccd_show_dance = false;
        $this->votccd_show_vote = false;
        $this->votccd_show_result = false;
        $this->votccd_show_sort = false;
        $this->votccd_scene_score = 0;
        $this->votccd_technic_score = 0;
        $this->votccd_couple_score = 0;
        $this->votccd_music_score = 0;
        $this->votccd_style_score = 0;
        $this->votccd_total_score = 0;
    }

    /**
     * Initializes internal state of IiMedias\VoteBundle\Model\Base\ContestCandidate object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ContestCandidate</code> instance.  If
     * <code>obj</code> is an instance of <code>ContestCandidate</code>, delegates to
     * <code>equals(ContestCandidate)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ContestCandidate The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [votccd_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->votccd_id;
    }

    /**
     * Get the [votccd_votcts_id] column value.
     *
     * @return int
     */
    public function getContestId()
    {
        return $this->votccd_votcts_id;
    }

    /**
     * Get the [votccd_votcdd_id] column value.
     *
     * @return int
     */
    public function getCandidateId()
    {
        return $this->votccd_votcdd_id;
    }

    /**
     * Get the [votccd_enable] column value.
     *
     * @return boolean
     */
    public function getEnable()
    {
        return $this->votccd_enable;
    }

    /**
     * Get the [votccd_enable] column value.
     *
     * @return boolean
     */
    public function isEnable()
    {
        return $this->getEnable();
    }

    /**
     * Get the [votccd_show_dance] column value.
     *
     * @return boolean
     */
    public function getShowDance()
    {
        return $this->votccd_show_dance;
    }

    /**
     * Get the [votccd_show_dance] column value.
     *
     * @return boolean
     */
    public function isShowDance()
    {
        return $this->getShowDance();
    }

    /**
     * Get the [votccd_show_vote] column value.
     *
     * @return boolean
     */
    public function getShowVote()
    {
        return $this->votccd_show_vote;
    }

    /**
     * Get the [votccd_show_vote] column value.
     *
     * @return boolean
     */
    public function isShowVote()
    {
        return $this->getShowVote();
    }

    /**
     * Get the [votccd_show_result] column value.
     *
     * @return boolean
     */
    public function getShowResult()
    {
        return $this->votccd_show_result;
    }

    /**
     * Get the [votccd_show_result] column value.
     *
     * @return boolean
     */
    public function isShowResult()
    {
        return $this->getShowResult();
    }

    /**
     * Get the [votccd_show_sort] column value.
     *
     * @return boolean
     */
    public function getShowSort()
    {
        return $this->votccd_show_sort;
    }

    /**
     * Get the [votccd_show_sort] column value.
     *
     * @return boolean
     */
    public function isShowSort()
    {
        return $this->getShowSort();
    }

    /**
     * Get the [votccd_scene_score] column value.
     *
     * @return int
     */
    public function getSceneScore()
    {
        return $this->votccd_scene_score;
    }

    /**
     * Get the [votccd_technic_score] column value.
     *
     * @return int
     */
    public function getTechnicScore()
    {
        return $this->votccd_technic_score;
    }

    /**
     * Get the [votccd_couple_score] column value.
     *
     * @return int
     */
    public function getCoupleScore()
    {
        return $this->votccd_couple_score;
    }

    /**
     * Get the [votccd_music_score] column value.
     *
     * @return int
     */
    public function getMusicScore()
    {
        return $this->votccd_music_score;
    }

    /**
     * Get the [votccd_style_score] column value.
     *
     * @return int
     */
    public function getStyleScore()
    {
        return $this->votccd_style_score;
    }

    /**
     * Get the [votccd_total_score] column value.
     *
     * @return int
     */
    public function getTotalScore()
    {
        return $this->votccd_total_score;
    }

    /**
     * Get the [optionally formatted] temporal [votccd_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->votccd_created_at;
        } else {
            return $this->votccd_created_at instanceof \DateTimeInterface ? $this->votccd_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [votccd_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->votccd_updated_at;
        } else {
            return $this->votccd_updated_at instanceof \DateTimeInterface ? $this->votccd_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [votccd_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_id !== $v) {
            $this->votccd_id = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [votccd_votcts_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setContestId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_votcts_id !== $v) {
            $this->votccd_votcts_id = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID] = true;
        }

        if ($this->aContest !== null && $this->aContest->getId() !== $v) {
            $this->aContest = null;
        }

        return $this;
    } // setContestId()

    /**
     * Set the value of [votccd_votcdd_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setCandidateId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_votcdd_id !== $v) {
            $this->votccd_votcdd_id = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID] = true;
        }

        if ($this->aCandidate !== null && $this->aCandidate->getId() !== $v) {
            $this->aCandidate = null;
        }

        return $this;
    } // setCandidateId()

    /**
     * Sets the value of the [votccd_enable] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setEnable($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votccd_enable !== $v) {
            $this->votccd_enable = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_ENABLE] = true;
        }

        return $this;
    } // setEnable()

    /**
     * Sets the value of the [votccd_show_dance] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setShowDance($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votccd_show_dance !== $v) {
            $this->votccd_show_dance = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_SHOW_DANCE] = true;
        }

        return $this;
    } // setShowDance()

    /**
     * Sets the value of the [votccd_show_vote] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setShowVote($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votccd_show_vote !== $v) {
            $this->votccd_show_vote = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_SHOW_VOTE] = true;
        }

        return $this;
    } // setShowVote()

    /**
     * Sets the value of the [votccd_show_result] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setShowResult($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votccd_show_result !== $v) {
            $this->votccd_show_result = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_SHOW_RESULT] = true;
        }

        return $this;
    } // setShowResult()

    /**
     * Sets the value of the [votccd_show_sort] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setShowSort($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votccd_show_sort !== $v) {
            $this->votccd_show_sort = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_SHOW_SORT] = true;
        }

        return $this;
    } // setShowSort()

    /**
     * Set the value of [votccd_scene_score] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setSceneScore($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_scene_score !== $v) {
            $this->votccd_scene_score = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE] = true;
        }

        return $this;
    } // setSceneScore()

    /**
     * Set the value of [votccd_technic_score] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setTechnicScore($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_technic_score !== $v) {
            $this->votccd_technic_score = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE] = true;
        }

        return $this;
    } // setTechnicScore()

    /**
     * Set the value of [votccd_couple_score] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setCoupleScore($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_couple_score !== $v) {
            $this->votccd_couple_score = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE] = true;
        }

        return $this;
    } // setCoupleScore()

    /**
     * Set the value of [votccd_music_score] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setMusicScore($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_music_score !== $v) {
            $this->votccd_music_score = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE] = true;
        }

        return $this;
    } // setMusicScore()

    /**
     * Set the value of [votccd_style_score] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setStyleScore($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_style_score !== $v) {
            $this->votccd_style_score = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE] = true;
        }

        return $this;
    } // setStyleScore()

    /**
     * Set the value of [votccd_total_score] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setTotalScore($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votccd_total_score !== $v) {
            $this->votccd_total_score = $v;
            $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE] = true;
        }

        return $this;
    } // setTotalScore()

    /**
     * Sets the value of [votccd_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->votccd_created_at !== null || $dt !== null) {
            if ($this->votccd_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->votccd_created_at->format("Y-m-d H:i:s.u")) {
                $this->votccd_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [votccd_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->votccd_updated_at !== null || $dt !== null) {
            if ($this->votccd_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->votccd_updated_at->format("Y-m-d H:i:s.u")) {
                $this->votccd_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->votccd_enable !== true) {
                return false;
            }

            if ($this->votccd_show_dance !== false) {
                return false;
            }

            if ($this->votccd_show_vote !== false) {
                return false;
            }

            if ($this->votccd_show_result !== false) {
                return false;
            }

            if ($this->votccd_show_sort !== false) {
                return false;
            }

            if ($this->votccd_scene_score !== 0) {
                return false;
            }

            if ($this->votccd_technic_score !== 0) {
                return false;
            }

            if ($this->votccd_couple_score !== 0) {
                return false;
            }

            if ($this->votccd_music_score !== 0) {
                return false;
            }

            if ($this->votccd_style_score !== 0) {
                return false;
            }

            if ($this->votccd_total_score !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ContestCandidateTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ContestCandidateTableMap::translateFieldName('ContestId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_votcts_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ContestCandidateTableMap::translateFieldName('CandidateId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_votcdd_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ContestCandidateTableMap::translateFieldName('Enable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_enable = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ContestCandidateTableMap::translateFieldName('ShowDance', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_show_dance = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ContestCandidateTableMap::translateFieldName('ShowVote', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_show_vote = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ContestCandidateTableMap::translateFieldName('ShowResult', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_show_result = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ContestCandidateTableMap::translateFieldName('ShowSort', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_show_sort = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ContestCandidateTableMap::translateFieldName('SceneScore', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_scene_score = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ContestCandidateTableMap::translateFieldName('TechnicScore', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_technic_score = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ContestCandidateTableMap::translateFieldName('CoupleScore', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_couple_score = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ContestCandidateTableMap::translateFieldName('MusicScore', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_music_score = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ContestCandidateTableMap::translateFieldName('StyleScore', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_style_score = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ContestCandidateTableMap::translateFieldName('TotalScore', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votccd_total_score = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ContestCandidateTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->votccd_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ContestCandidateTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->votccd_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = ContestCandidateTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VoteBundle\\Model\\ContestCandidate'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aContest !== null && $this->votccd_votcts_id !== $this->aContest->getId()) {
            $this->aContest = null;
        }
        if ($this->aCandidate !== null && $this->votccd_votcdd_id !== $this->aCandidate->getId()) {
            $this->aCandidate = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContestCandidateTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildContestCandidateQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aContest = null;
            $this->aCandidate = null;
            $this->collScores = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ContestCandidate::setDeleted()
     * @see ContestCandidate::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestCandidateTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildContestCandidateQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestCandidateTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ContestCandidateTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aContest !== null) {
                if ($this->aContest->isModified() || $this->aContest->isNew()) {
                    $affectedRows += $this->aContest->save($con);
                }
                $this->setContest($this->aContest);
            }

            if ($this->aCandidate !== null) {
                if ($this->aCandidate->isModified() || $this->aCandidate->isNew()) {
                    $affectedRows += $this->aCandidate->save($con);
                }
                $this->setCandidate($this->aCandidate);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->scoresScheduledForDeletion !== null) {
                if (!$this->scoresScheduledForDeletion->isEmpty()) {
                    \IiMedias\VoteBundle\Model\ScoreQuery::create()
                        ->filterByPrimaryKeys($this->scoresScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->scoresScheduledForDeletion = null;
                }
            }

            if ($this->collScores !== null) {
                foreach ($this->collScores as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_ID] = true;
        if (null !== $this->votccd_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ContestCandidateTableMap::COL_VOTCCD_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_id';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_votcts_id';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_votcdd_id';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_ENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_enable';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SHOW_DANCE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_show_dance';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SHOW_VOTE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_show_vote';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SHOW_RESULT)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_show_result';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SHOW_SORT)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_show_sort';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_scene_score';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_technic_score';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_couple_score';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_music_score';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_style_score';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_total_score';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_created_at';
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'votccd_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO vote_contest_candidate_votccd (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'votccd_id':
                        $stmt->bindValue($identifier, $this->votccd_id, PDO::PARAM_INT);
                        break;
                    case 'votccd_votcts_id':
                        $stmt->bindValue($identifier, $this->votccd_votcts_id, PDO::PARAM_INT);
                        break;
                    case 'votccd_votcdd_id':
                        $stmt->bindValue($identifier, $this->votccd_votcdd_id, PDO::PARAM_INT);
                        break;
                    case 'votccd_enable':
                        $stmt->bindValue($identifier, (int) $this->votccd_enable, PDO::PARAM_INT);
                        break;
                    case 'votccd_show_dance':
                        $stmt->bindValue($identifier, (int) $this->votccd_show_dance, PDO::PARAM_INT);
                        break;
                    case 'votccd_show_vote':
                        $stmt->bindValue($identifier, (int) $this->votccd_show_vote, PDO::PARAM_INT);
                        break;
                    case 'votccd_show_result':
                        $stmt->bindValue($identifier, (int) $this->votccd_show_result, PDO::PARAM_INT);
                        break;
                    case 'votccd_show_sort':
                        $stmt->bindValue($identifier, (int) $this->votccd_show_sort, PDO::PARAM_INT);
                        break;
                    case 'votccd_scene_score':
                        $stmt->bindValue($identifier, $this->votccd_scene_score, PDO::PARAM_INT);
                        break;
                    case 'votccd_technic_score':
                        $stmt->bindValue($identifier, $this->votccd_technic_score, PDO::PARAM_INT);
                        break;
                    case 'votccd_couple_score':
                        $stmt->bindValue($identifier, $this->votccd_couple_score, PDO::PARAM_INT);
                        break;
                    case 'votccd_music_score':
                        $stmt->bindValue($identifier, $this->votccd_music_score, PDO::PARAM_INT);
                        break;
                    case 'votccd_style_score':
                        $stmt->bindValue($identifier, $this->votccd_style_score, PDO::PARAM_INT);
                        break;
                    case 'votccd_total_score':
                        $stmt->bindValue($identifier, $this->votccd_total_score, PDO::PARAM_INT);
                        break;
                    case 'votccd_created_at':
                        $stmt->bindValue($identifier, $this->votccd_created_at ? $this->votccd_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'votccd_updated_at':
                        $stmt->bindValue($identifier, $this->votccd_updated_at ? $this->votccd_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContestCandidateTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getContestId();
                break;
            case 2:
                return $this->getCandidateId();
                break;
            case 3:
                return $this->getEnable();
                break;
            case 4:
                return $this->getShowDance();
                break;
            case 5:
                return $this->getShowVote();
                break;
            case 6:
                return $this->getShowResult();
                break;
            case 7:
                return $this->getShowSort();
                break;
            case 8:
                return $this->getSceneScore();
                break;
            case 9:
                return $this->getTechnicScore();
                break;
            case 10:
                return $this->getCoupleScore();
                break;
            case 11:
                return $this->getMusicScore();
                break;
            case 12:
                return $this->getStyleScore();
                break;
            case 13:
                return $this->getTotalScore();
                break;
            case 14:
                return $this->getCreatedAt();
                break;
            case 15:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ContestCandidate'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ContestCandidate'][$this->hashCode()] = true;
        $keys = ContestCandidateTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getContestId(),
            $keys[2] => $this->getCandidateId(),
            $keys[3] => $this->getEnable(),
            $keys[4] => $this->getShowDance(),
            $keys[5] => $this->getShowVote(),
            $keys[6] => $this->getShowResult(),
            $keys[7] => $this->getShowSort(),
            $keys[8] => $this->getSceneScore(),
            $keys[9] => $this->getTechnicScore(),
            $keys[10] => $this->getCoupleScore(),
            $keys[11] => $this->getMusicScore(),
            $keys[12] => $this->getStyleScore(),
            $keys[13] => $this->getTotalScore(),
            $keys[14] => $this->getCreatedAt(),
            $keys[15] => $this->getUpdatedAt(),
        );
        if ($result[$keys[14]] instanceof \DateTimeInterface) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTimeInterface) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aContest) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contest';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'vote_contest_votcts';
                        break;
                    default:
                        $key = 'Contest';
                }

                $result[$key] = $this->aContest->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCandidate) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'candidate';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'vote_candidate_votcdd';
                        break;
                    default:
                        $key = 'Candidate';
                }

                $result[$key] = $this->aCandidate->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collScores) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'scores';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'vote_score_votscos';
                        break;
                    default:
                        $key = 'Scores';
                }

                $result[$key] = $this->collScores->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContestCandidateTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setContestId($value);
                break;
            case 2:
                $this->setCandidateId($value);
                break;
            case 3:
                $this->setEnable($value);
                break;
            case 4:
                $this->setShowDance($value);
                break;
            case 5:
                $this->setShowVote($value);
                break;
            case 6:
                $this->setShowResult($value);
                break;
            case 7:
                $this->setShowSort($value);
                break;
            case 8:
                $this->setSceneScore($value);
                break;
            case 9:
                $this->setTechnicScore($value);
                break;
            case 10:
                $this->setCoupleScore($value);
                break;
            case 11:
                $this->setMusicScore($value);
                break;
            case 12:
                $this->setStyleScore($value);
                break;
            case 13:
                $this->setTotalScore($value);
                break;
            case 14:
                $this->setCreatedAt($value);
                break;
            case 15:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ContestCandidateTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setContestId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCandidateId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setEnable($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setShowDance($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setShowVote($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setShowResult($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setShowSort($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setSceneScore($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTechnicScore($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCoupleScore($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setMusicScore($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setStyleScore($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setTotalScore($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCreatedAt($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setUpdatedAt($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ContestCandidateTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_ID)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_ID, $this->votccd_id);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID, $this->votccd_votcts_id);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID, $this->votccd_votcdd_id);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_ENABLE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_ENABLE, $this->votccd_enable);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SHOW_DANCE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_SHOW_DANCE, $this->votccd_show_dance);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SHOW_VOTE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_SHOW_VOTE, $this->votccd_show_vote);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SHOW_RESULT)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_SHOW_RESULT, $this->votccd_show_result);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SHOW_SORT)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_SHOW_SORT, $this->votccd_show_sort);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE, $this->votccd_scene_score);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE, $this->votccd_technic_score);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE, $this->votccd_couple_score);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE, $this->votccd_music_score);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE, $this->votccd_style_score);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE, $this->votccd_total_score);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT, $this->votccd_created_at);
        }
        if ($this->isColumnModified(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT)) {
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT, $this->votccd_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildContestCandidateQuery::create();
        $criteria->add(ContestCandidateTableMap::COL_VOTCCD_ID, $this->votccd_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (votccd_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VoteBundle\Model\ContestCandidate (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setContestId($this->getContestId());
        $copyObj->setCandidateId($this->getCandidateId());
        $copyObj->setEnable($this->getEnable());
        $copyObj->setShowDance($this->getShowDance());
        $copyObj->setShowVote($this->getShowVote());
        $copyObj->setShowResult($this->getShowResult());
        $copyObj->setShowSort($this->getShowSort());
        $copyObj->setSceneScore($this->getSceneScore());
        $copyObj->setTechnicScore($this->getTechnicScore());
        $copyObj->setCoupleScore($this->getCoupleScore());
        $copyObj->setMusicScore($this->getMusicScore());
        $copyObj->setStyleScore($this->getStyleScore());
        $copyObj->setTotalScore($this->getTotalScore());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getScores() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addScore($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VoteBundle\Model\ContestCandidate Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildContest object.
     *
     * @param  ChildContest $v
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContest(ChildContest $v = null)
    {
        if ($v === null) {
            $this->setContestId(NULL);
        } else {
            $this->setContestId($v->getId());
        }

        $this->aContest = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildContest object, it will not be re-added.
        if ($v !== null) {
            $v->addContestCandidate($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildContest object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildContest The associated ChildContest object.
     * @throws PropelException
     */
    public function getContest(ConnectionInterface $con = null)
    {
        if ($this->aContest === null && ($this->votccd_votcts_id != 0)) {
            $this->aContest = ChildContestQuery::create()->findPk($this->votccd_votcts_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContest->addContestCandidates($this);
             */
        }

        return $this->aContest;
    }

    /**
     * Declares an association between this object and a ChildCandidate object.
     *
     * @param  ChildCandidate $v
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCandidate(ChildCandidate $v = null)
    {
        if ($v === null) {
            $this->setCandidateId(NULL);
        } else {
            $this->setCandidateId($v->getId());
        }

        $this->aCandidate = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCandidate object, it will not be re-added.
        if ($v !== null) {
            $v->addContestCandidate($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCandidate object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCandidate The associated ChildCandidate object.
     * @throws PropelException
     */
    public function getCandidate(ConnectionInterface $con = null)
    {
        if ($this->aCandidate === null && ($this->votccd_votcdd_id != 0)) {
            $this->aCandidate = ChildCandidateQuery::create()->findPk($this->votccd_votcdd_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCandidate->addContestCandidates($this);
             */
        }

        return $this->aCandidate;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Score' == $relationName) {
            $this->initScores();
            return;
        }
    }

    /**
     * Clears out the collScores collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addScores()
     */
    public function clearScores()
    {
        $this->collScores = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collScores collection loaded partially.
     */
    public function resetPartialScores($v = true)
    {
        $this->collScoresPartial = $v;
    }

    /**
     * Initializes the collScores collection.
     *
     * By default this just sets the collScores collection to an empty array (like clearcollScores());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initScores($overrideExisting = true)
    {
        if (null !== $this->collScores && !$overrideExisting) {
            return;
        }

        $collectionClassName = ScoreTableMap::getTableMap()->getCollectionClassName();

        $this->collScores = new $collectionClassName;
        $this->collScores->setModel('\IiMedias\VoteBundle\Model\Score');
    }

    /**
     * Gets an array of ChildScore objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildContestCandidate is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildScore[] List of ChildScore objects
     * @throws PropelException
     */
    public function getScores(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collScoresPartial && !$this->isNew();
        if (null === $this->collScores || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collScores) {
                // return empty collection
                $this->initScores();
            } else {
                $collScores = ChildScoreQuery::create(null, $criteria)
                    ->filterByContestCandidate($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collScoresPartial && count($collScores)) {
                        $this->initScores(false);

                        foreach ($collScores as $obj) {
                            if (false == $this->collScores->contains($obj)) {
                                $this->collScores->append($obj);
                            }
                        }

                        $this->collScoresPartial = true;
                    }

                    return $collScores;
                }

                if ($partial && $this->collScores) {
                    foreach ($this->collScores as $obj) {
                        if ($obj->isNew()) {
                            $collScores[] = $obj;
                        }
                    }
                }

                $this->collScores = $collScores;
                $this->collScoresPartial = false;
            }
        }

        return $this->collScores;
    }

    /**
     * Sets a collection of ChildScore objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $scores A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildContestCandidate The current object (for fluent API support)
     */
    public function setScores(Collection $scores, ConnectionInterface $con = null)
    {
        /** @var ChildScore[] $scoresToDelete */
        $scoresToDelete = $this->getScores(new Criteria(), $con)->diff($scores);


        $this->scoresScheduledForDeletion = $scoresToDelete;

        foreach ($scoresToDelete as $scoreRemoved) {
            $scoreRemoved->setContestCandidate(null);
        }

        $this->collScores = null;
        foreach ($scores as $score) {
            $this->addScore($score);
        }

        $this->collScores = $scores;
        $this->collScoresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Score objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Score objects.
     * @throws PropelException
     */
    public function countScores(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collScoresPartial && !$this->isNew();
        if (null === $this->collScores || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collScores) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getScores());
            }

            $query = ChildScoreQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByContestCandidate($this)
                ->count($con);
        }

        return count($this->collScores);
    }

    /**
     * Method called to associate a ChildScore object to this object
     * through the ChildScore foreign key attribute.
     *
     * @param  ChildScore $l ChildScore
     * @return $this|\IiMedias\VoteBundle\Model\ContestCandidate The current object (for fluent API support)
     */
    public function addScore(ChildScore $l)
    {
        if ($this->collScores === null) {
            $this->initScores();
            $this->collScoresPartial = true;
        }

        if (!$this->collScores->contains($l)) {
            $this->doAddScore($l);

            if ($this->scoresScheduledForDeletion and $this->scoresScheduledForDeletion->contains($l)) {
                $this->scoresScheduledForDeletion->remove($this->scoresScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildScore $score The ChildScore object to add.
     */
    protected function doAddScore(ChildScore $score)
    {
        $this->collScores[]= $score;
        $score->setContestCandidate($this);
    }

    /**
     * @param  ChildScore $score The ChildScore object to remove.
     * @return $this|ChildContestCandidate The current object (for fluent API support)
     */
    public function removeScore(ChildScore $score)
    {
        if ($this->getScores()->contains($score)) {
            $pos = $this->collScores->search($score);
            $this->collScores->remove($pos);
            if (null === $this->scoresScheduledForDeletion) {
                $this->scoresScheduledForDeletion = clone $this->collScores;
                $this->scoresScheduledForDeletion->clear();
            }
            $this->scoresScheduledForDeletion[]= clone $score;
            $score->setContestCandidate(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ContestCandidate is new, it will return
     * an empty collection; or if this ContestCandidate has previously
     * been saved, it will retrieve related Scores from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ContestCandidate.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildScore[] List of ChildScore objects
     */
    public function getScoresJoinVoter(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildScoreQuery::create(null, $criteria);
        $query->joinWith('Voter', $joinBehavior);

        return $this->getScores($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aContest) {
            $this->aContest->removeContestCandidate($this);
        }
        if (null !== $this->aCandidate) {
            $this->aCandidate->removeContestCandidate($this);
        }
        $this->votccd_id = null;
        $this->votccd_votcts_id = null;
        $this->votccd_votcdd_id = null;
        $this->votccd_enable = null;
        $this->votccd_show_dance = null;
        $this->votccd_show_vote = null;
        $this->votccd_show_result = null;
        $this->votccd_show_sort = null;
        $this->votccd_scene_score = null;
        $this->votccd_technic_score = null;
        $this->votccd_couple_score = null;
        $this->votccd_music_score = null;
        $this->votccd_style_score = null;
        $this->votccd_total_score = null;
        $this->votccd_created_at = null;
        $this->votccd_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collScores) {
                foreach ($this->collScores as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collScores = null;
        $this->aContest = null;
        $this->aCandidate = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ContestCandidateTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildContestCandidate The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
