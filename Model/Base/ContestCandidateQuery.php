<?php

namespace IiMedias\VoteBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VoteBundle\Model\ContestCandidate as ChildContestCandidate;
use IiMedias\VoteBundle\Model\ContestCandidateQuery as ChildContestCandidateQuery;
use IiMedias\VoteBundle\Model\Map\ContestCandidateTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vote_contest_candidate_votccd' table.
 *
 *
 *
 * @method     ChildContestCandidateQuery orderById($order = Criteria::ASC) Order by the votccd_id column
 * @method     ChildContestCandidateQuery orderByContestId($order = Criteria::ASC) Order by the votccd_votcts_id column
 * @method     ChildContestCandidateQuery orderByCandidateId($order = Criteria::ASC) Order by the votccd_votcdd_id column
 * @method     ChildContestCandidateQuery orderByEnable($order = Criteria::ASC) Order by the votccd_enable column
 * @method     ChildContestCandidateQuery orderByShowDance($order = Criteria::ASC) Order by the votccd_show_dance column
 * @method     ChildContestCandidateQuery orderByShowVote($order = Criteria::ASC) Order by the votccd_show_vote column
 * @method     ChildContestCandidateQuery orderByShowResult($order = Criteria::ASC) Order by the votccd_show_result column
 * @method     ChildContestCandidateQuery orderByShowSort($order = Criteria::ASC) Order by the votccd_show_sort column
 * @method     ChildContestCandidateQuery orderBySceneScore($order = Criteria::ASC) Order by the votccd_scene_score column
 * @method     ChildContestCandidateQuery orderByTechnicScore($order = Criteria::ASC) Order by the votccd_technic_score column
 * @method     ChildContestCandidateQuery orderByCoupleScore($order = Criteria::ASC) Order by the votccd_couple_score column
 * @method     ChildContestCandidateQuery orderByMusicScore($order = Criteria::ASC) Order by the votccd_music_score column
 * @method     ChildContestCandidateQuery orderByStyleScore($order = Criteria::ASC) Order by the votccd_style_score column
 * @method     ChildContestCandidateQuery orderByTotalScore($order = Criteria::ASC) Order by the votccd_total_score column
 * @method     ChildContestCandidateQuery orderByCreatedAt($order = Criteria::ASC) Order by the votccd_created_at column
 * @method     ChildContestCandidateQuery orderByUpdatedAt($order = Criteria::ASC) Order by the votccd_updated_at column
 *
 * @method     ChildContestCandidateQuery groupById() Group by the votccd_id column
 * @method     ChildContestCandidateQuery groupByContestId() Group by the votccd_votcts_id column
 * @method     ChildContestCandidateQuery groupByCandidateId() Group by the votccd_votcdd_id column
 * @method     ChildContestCandidateQuery groupByEnable() Group by the votccd_enable column
 * @method     ChildContestCandidateQuery groupByShowDance() Group by the votccd_show_dance column
 * @method     ChildContestCandidateQuery groupByShowVote() Group by the votccd_show_vote column
 * @method     ChildContestCandidateQuery groupByShowResult() Group by the votccd_show_result column
 * @method     ChildContestCandidateQuery groupByShowSort() Group by the votccd_show_sort column
 * @method     ChildContestCandidateQuery groupBySceneScore() Group by the votccd_scene_score column
 * @method     ChildContestCandidateQuery groupByTechnicScore() Group by the votccd_technic_score column
 * @method     ChildContestCandidateQuery groupByCoupleScore() Group by the votccd_couple_score column
 * @method     ChildContestCandidateQuery groupByMusicScore() Group by the votccd_music_score column
 * @method     ChildContestCandidateQuery groupByStyleScore() Group by the votccd_style_score column
 * @method     ChildContestCandidateQuery groupByTotalScore() Group by the votccd_total_score column
 * @method     ChildContestCandidateQuery groupByCreatedAt() Group by the votccd_created_at column
 * @method     ChildContestCandidateQuery groupByUpdatedAt() Group by the votccd_updated_at column
 *
 * @method     ChildContestCandidateQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContestCandidateQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContestCandidateQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContestCandidateQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContestCandidateQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContestCandidateQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContestCandidateQuery leftJoinContest($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contest relation
 * @method     ChildContestCandidateQuery rightJoinContest($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contest relation
 * @method     ChildContestCandidateQuery innerJoinContest($relationAlias = null) Adds a INNER JOIN clause to the query using the Contest relation
 *
 * @method     ChildContestCandidateQuery joinWithContest($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Contest relation
 *
 * @method     ChildContestCandidateQuery leftJoinWithContest() Adds a LEFT JOIN clause and with to the query using the Contest relation
 * @method     ChildContestCandidateQuery rightJoinWithContest() Adds a RIGHT JOIN clause and with to the query using the Contest relation
 * @method     ChildContestCandidateQuery innerJoinWithContest() Adds a INNER JOIN clause and with to the query using the Contest relation
 *
 * @method     ChildContestCandidateQuery leftJoinCandidate($relationAlias = null) Adds a LEFT JOIN clause to the query using the Candidate relation
 * @method     ChildContestCandidateQuery rightJoinCandidate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Candidate relation
 * @method     ChildContestCandidateQuery innerJoinCandidate($relationAlias = null) Adds a INNER JOIN clause to the query using the Candidate relation
 *
 * @method     ChildContestCandidateQuery joinWithCandidate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Candidate relation
 *
 * @method     ChildContestCandidateQuery leftJoinWithCandidate() Adds a LEFT JOIN clause and with to the query using the Candidate relation
 * @method     ChildContestCandidateQuery rightJoinWithCandidate() Adds a RIGHT JOIN clause and with to the query using the Candidate relation
 * @method     ChildContestCandidateQuery innerJoinWithCandidate() Adds a INNER JOIN clause and with to the query using the Candidate relation
 *
 * @method     ChildContestCandidateQuery leftJoinScore($relationAlias = null) Adds a LEFT JOIN clause to the query using the Score relation
 * @method     ChildContestCandidateQuery rightJoinScore($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Score relation
 * @method     ChildContestCandidateQuery innerJoinScore($relationAlias = null) Adds a INNER JOIN clause to the query using the Score relation
 *
 * @method     ChildContestCandidateQuery joinWithScore($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Score relation
 *
 * @method     ChildContestCandidateQuery leftJoinWithScore() Adds a LEFT JOIN clause and with to the query using the Score relation
 * @method     ChildContestCandidateQuery rightJoinWithScore() Adds a RIGHT JOIN clause and with to the query using the Score relation
 * @method     ChildContestCandidateQuery innerJoinWithScore() Adds a INNER JOIN clause and with to the query using the Score relation
 *
 * @method     \IiMedias\VoteBundle\Model\ContestQuery|\IiMedias\VoteBundle\Model\CandidateQuery|\IiMedias\VoteBundle\Model\ScoreQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContestCandidate findOne(ConnectionInterface $con = null) Return the first ChildContestCandidate matching the query
 * @method     ChildContestCandidate findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContestCandidate matching the query, or a new ChildContestCandidate object populated from the query conditions when no match is found
 *
 * @method     ChildContestCandidate findOneById(int $votccd_id) Return the first ChildContestCandidate filtered by the votccd_id column
 * @method     ChildContestCandidate findOneByContestId(int $votccd_votcts_id) Return the first ChildContestCandidate filtered by the votccd_votcts_id column
 * @method     ChildContestCandidate findOneByCandidateId(int $votccd_votcdd_id) Return the first ChildContestCandidate filtered by the votccd_votcdd_id column
 * @method     ChildContestCandidate findOneByEnable(boolean $votccd_enable) Return the first ChildContestCandidate filtered by the votccd_enable column
 * @method     ChildContestCandidate findOneByShowDance(boolean $votccd_show_dance) Return the first ChildContestCandidate filtered by the votccd_show_dance column
 * @method     ChildContestCandidate findOneByShowVote(boolean $votccd_show_vote) Return the first ChildContestCandidate filtered by the votccd_show_vote column
 * @method     ChildContestCandidate findOneByShowResult(boolean $votccd_show_result) Return the first ChildContestCandidate filtered by the votccd_show_result column
 * @method     ChildContestCandidate findOneByShowSort(boolean $votccd_show_sort) Return the first ChildContestCandidate filtered by the votccd_show_sort column
 * @method     ChildContestCandidate findOneBySceneScore(int $votccd_scene_score) Return the first ChildContestCandidate filtered by the votccd_scene_score column
 * @method     ChildContestCandidate findOneByTechnicScore(int $votccd_technic_score) Return the first ChildContestCandidate filtered by the votccd_technic_score column
 * @method     ChildContestCandidate findOneByCoupleScore(int $votccd_couple_score) Return the first ChildContestCandidate filtered by the votccd_couple_score column
 * @method     ChildContestCandidate findOneByMusicScore(int $votccd_music_score) Return the first ChildContestCandidate filtered by the votccd_music_score column
 * @method     ChildContestCandidate findOneByStyleScore(int $votccd_style_score) Return the first ChildContestCandidate filtered by the votccd_style_score column
 * @method     ChildContestCandidate findOneByTotalScore(int $votccd_total_score) Return the first ChildContestCandidate filtered by the votccd_total_score column
 * @method     ChildContestCandidate findOneByCreatedAt(string $votccd_created_at) Return the first ChildContestCandidate filtered by the votccd_created_at column
 * @method     ChildContestCandidate findOneByUpdatedAt(string $votccd_updated_at) Return the first ChildContestCandidate filtered by the votccd_updated_at column *

 * @method     ChildContestCandidate requirePk($key, ConnectionInterface $con = null) Return the ChildContestCandidate by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOne(ConnectionInterface $con = null) Return the first ChildContestCandidate matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContestCandidate requireOneById(int $votccd_id) Return the first ChildContestCandidate filtered by the votccd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByContestId(int $votccd_votcts_id) Return the first ChildContestCandidate filtered by the votccd_votcts_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByCandidateId(int $votccd_votcdd_id) Return the first ChildContestCandidate filtered by the votccd_votcdd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByEnable(boolean $votccd_enable) Return the first ChildContestCandidate filtered by the votccd_enable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByShowDance(boolean $votccd_show_dance) Return the first ChildContestCandidate filtered by the votccd_show_dance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByShowVote(boolean $votccd_show_vote) Return the first ChildContestCandidate filtered by the votccd_show_vote column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByShowResult(boolean $votccd_show_result) Return the first ChildContestCandidate filtered by the votccd_show_result column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByShowSort(boolean $votccd_show_sort) Return the first ChildContestCandidate filtered by the votccd_show_sort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneBySceneScore(int $votccd_scene_score) Return the first ChildContestCandidate filtered by the votccd_scene_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByTechnicScore(int $votccd_technic_score) Return the first ChildContestCandidate filtered by the votccd_technic_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByCoupleScore(int $votccd_couple_score) Return the first ChildContestCandidate filtered by the votccd_couple_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByMusicScore(int $votccd_music_score) Return the first ChildContestCandidate filtered by the votccd_music_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByStyleScore(int $votccd_style_score) Return the first ChildContestCandidate filtered by the votccd_style_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByTotalScore(int $votccd_total_score) Return the first ChildContestCandidate filtered by the votccd_total_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByCreatedAt(string $votccd_created_at) Return the first ChildContestCandidate filtered by the votccd_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContestCandidate requireOneByUpdatedAt(string $votccd_updated_at) Return the first ChildContestCandidate filtered by the votccd_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContestCandidate[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContestCandidate objects based on current ModelCriteria
 * @method     ChildContestCandidate[]|ObjectCollection findById(int $votccd_id) Return ChildContestCandidate objects filtered by the votccd_id column
 * @method     ChildContestCandidate[]|ObjectCollection findByContestId(int $votccd_votcts_id) Return ChildContestCandidate objects filtered by the votccd_votcts_id column
 * @method     ChildContestCandidate[]|ObjectCollection findByCandidateId(int $votccd_votcdd_id) Return ChildContestCandidate objects filtered by the votccd_votcdd_id column
 * @method     ChildContestCandidate[]|ObjectCollection findByEnable(boolean $votccd_enable) Return ChildContestCandidate objects filtered by the votccd_enable column
 * @method     ChildContestCandidate[]|ObjectCollection findByShowDance(boolean $votccd_show_dance) Return ChildContestCandidate objects filtered by the votccd_show_dance column
 * @method     ChildContestCandidate[]|ObjectCollection findByShowVote(boolean $votccd_show_vote) Return ChildContestCandidate objects filtered by the votccd_show_vote column
 * @method     ChildContestCandidate[]|ObjectCollection findByShowResult(boolean $votccd_show_result) Return ChildContestCandidate objects filtered by the votccd_show_result column
 * @method     ChildContestCandidate[]|ObjectCollection findByShowSort(boolean $votccd_show_sort) Return ChildContestCandidate objects filtered by the votccd_show_sort column
 * @method     ChildContestCandidate[]|ObjectCollection findBySceneScore(int $votccd_scene_score) Return ChildContestCandidate objects filtered by the votccd_scene_score column
 * @method     ChildContestCandidate[]|ObjectCollection findByTechnicScore(int $votccd_technic_score) Return ChildContestCandidate objects filtered by the votccd_technic_score column
 * @method     ChildContestCandidate[]|ObjectCollection findByCoupleScore(int $votccd_couple_score) Return ChildContestCandidate objects filtered by the votccd_couple_score column
 * @method     ChildContestCandidate[]|ObjectCollection findByMusicScore(int $votccd_music_score) Return ChildContestCandidate objects filtered by the votccd_music_score column
 * @method     ChildContestCandidate[]|ObjectCollection findByStyleScore(int $votccd_style_score) Return ChildContestCandidate objects filtered by the votccd_style_score column
 * @method     ChildContestCandidate[]|ObjectCollection findByTotalScore(int $votccd_total_score) Return ChildContestCandidate objects filtered by the votccd_total_score column
 * @method     ChildContestCandidate[]|ObjectCollection findByCreatedAt(string $votccd_created_at) Return ChildContestCandidate objects filtered by the votccd_created_at column
 * @method     ChildContestCandidate[]|ObjectCollection findByUpdatedAt(string $votccd_updated_at) Return ChildContestCandidate objects filtered by the votccd_updated_at column
 * @method     ChildContestCandidate[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContestCandidateQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VoteBundle\Model\Base\ContestCandidateQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VoteBundle\\Model\\ContestCandidate', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContestCandidateQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContestCandidateQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContestCandidateQuery) {
            return $criteria;
        }
        $query = new ChildContestCandidateQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContestCandidate|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContestCandidateTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContestCandidateTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContestCandidate A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT votccd_id, votccd_votcts_id, votccd_votcdd_id, votccd_enable, votccd_show_dance, votccd_show_vote, votccd_show_result, votccd_show_sort, votccd_scene_score, votccd_technic_score, votccd_couple_score, votccd_music_score, votccd_style_score, votccd_total_score, votccd_created_at, votccd_updated_at FROM vote_contest_candidate_votccd WHERE votccd_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContestCandidate $obj */
            $obj = new ChildContestCandidate();
            $obj->hydrate($row);
            ContestCandidateTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContestCandidate|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the votccd_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE votccd_id = 1234
     * $query->filterById(array(12, 34)); // WHERE votccd_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE votccd_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_ID, $id, $comparison);
    }

    /**
     * Filter the query on the votccd_votcts_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContestId(1234); // WHERE votccd_votcts_id = 1234
     * $query->filterByContestId(array(12, 34)); // WHERE votccd_votcts_id IN (12, 34)
     * $query->filterByContestId(array('min' => 12)); // WHERE votccd_votcts_id > 12
     * </code>
     *
     * @see       filterByContest()
     *
     * @param     mixed $contestId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByContestId($contestId = null, $comparison = null)
    {
        if (is_array($contestId)) {
            $useMinMax = false;
            if (isset($contestId['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID, $contestId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contestId['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID, $contestId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID, $contestId, $comparison);
    }

    /**
     * Filter the query on the votccd_votcdd_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCandidateId(1234); // WHERE votccd_votcdd_id = 1234
     * $query->filterByCandidateId(array(12, 34)); // WHERE votccd_votcdd_id IN (12, 34)
     * $query->filterByCandidateId(array('min' => 12)); // WHERE votccd_votcdd_id > 12
     * </code>
     *
     * @see       filterByCandidate()
     *
     * @param     mixed $candidateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByCandidateId($candidateId = null, $comparison = null)
    {
        if (is_array($candidateId)) {
            $useMinMax = false;
            if (isset($candidateId['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID, $candidateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($candidateId['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID, $candidateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID, $candidateId, $comparison);
    }

    /**
     * Filter the query on the votccd_enable column
     *
     * Example usage:
     * <code>
     * $query->filterByEnable(true); // WHERE votccd_enable = true
     * $query->filterByEnable('yes'); // WHERE votccd_enable = true
     * </code>
     *
     * @param     boolean|string $enable The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByEnable($enable = null, $comparison = null)
    {
        if (is_string($enable)) {
            $enable = in_array(strtolower($enable), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_ENABLE, $enable, $comparison);
    }

    /**
     * Filter the query on the votccd_show_dance column
     *
     * Example usage:
     * <code>
     * $query->filterByShowDance(true); // WHERE votccd_show_dance = true
     * $query->filterByShowDance('yes'); // WHERE votccd_show_dance = true
     * </code>
     *
     * @param     boolean|string $showDance The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByShowDance($showDance = null, $comparison = null)
    {
        if (is_string($showDance)) {
            $showDance = in_array(strtolower($showDance), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_SHOW_DANCE, $showDance, $comparison);
    }

    /**
     * Filter the query on the votccd_show_vote column
     *
     * Example usage:
     * <code>
     * $query->filterByShowVote(true); // WHERE votccd_show_vote = true
     * $query->filterByShowVote('yes'); // WHERE votccd_show_vote = true
     * </code>
     *
     * @param     boolean|string $showVote The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByShowVote($showVote = null, $comparison = null)
    {
        if (is_string($showVote)) {
            $showVote = in_array(strtolower($showVote), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_SHOW_VOTE, $showVote, $comparison);
    }

    /**
     * Filter the query on the votccd_show_result column
     *
     * Example usage:
     * <code>
     * $query->filterByShowResult(true); // WHERE votccd_show_result = true
     * $query->filterByShowResult('yes'); // WHERE votccd_show_result = true
     * </code>
     *
     * @param     boolean|string $showResult The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByShowResult($showResult = null, $comparison = null)
    {
        if (is_string($showResult)) {
            $showResult = in_array(strtolower($showResult), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_SHOW_RESULT, $showResult, $comparison);
    }

    /**
     * Filter the query on the votccd_show_sort column
     *
     * Example usage:
     * <code>
     * $query->filterByShowSort(true); // WHERE votccd_show_sort = true
     * $query->filterByShowSort('yes'); // WHERE votccd_show_sort = true
     * </code>
     *
     * @param     boolean|string $showSort The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByShowSort($showSort = null, $comparison = null)
    {
        if (is_string($showSort)) {
            $showSort = in_array(strtolower($showSort), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_SHOW_SORT, $showSort, $comparison);
    }

    /**
     * Filter the query on the votccd_scene_score column
     *
     * Example usage:
     * <code>
     * $query->filterBySceneScore(1234); // WHERE votccd_scene_score = 1234
     * $query->filterBySceneScore(array(12, 34)); // WHERE votccd_scene_score IN (12, 34)
     * $query->filterBySceneScore(array('min' => 12)); // WHERE votccd_scene_score > 12
     * </code>
     *
     * @param     mixed $sceneScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterBySceneScore($sceneScore = null, $comparison = null)
    {
        if (is_array($sceneScore)) {
            $useMinMax = false;
            if (isset($sceneScore['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE, $sceneScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sceneScore['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE, $sceneScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE, $sceneScore, $comparison);
    }

    /**
     * Filter the query on the votccd_technic_score column
     *
     * Example usage:
     * <code>
     * $query->filterByTechnicScore(1234); // WHERE votccd_technic_score = 1234
     * $query->filterByTechnicScore(array(12, 34)); // WHERE votccd_technic_score IN (12, 34)
     * $query->filterByTechnicScore(array('min' => 12)); // WHERE votccd_technic_score > 12
     * </code>
     *
     * @param     mixed $technicScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByTechnicScore($technicScore = null, $comparison = null)
    {
        if (is_array($technicScore)) {
            $useMinMax = false;
            if (isset($technicScore['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE, $technicScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($technicScore['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE, $technicScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE, $technicScore, $comparison);
    }

    /**
     * Filter the query on the votccd_couple_score column
     *
     * Example usage:
     * <code>
     * $query->filterByCoupleScore(1234); // WHERE votccd_couple_score = 1234
     * $query->filterByCoupleScore(array(12, 34)); // WHERE votccd_couple_score IN (12, 34)
     * $query->filterByCoupleScore(array('min' => 12)); // WHERE votccd_couple_score > 12
     * </code>
     *
     * @param     mixed $coupleScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByCoupleScore($coupleScore = null, $comparison = null)
    {
        if (is_array($coupleScore)) {
            $useMinMax = false;
            if (isset($coupleScore['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE, $coupleScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($coupleScore['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE, $coupleScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE, $coupleScore, $comparison);
    }

    /**
     * Filter the query on the votccd_music_score column
     *
     * Example usage:
     * <code>
     * $query->filterByMusicScore(1234); // WHERE votccd_music_score = 1234
     * $query->filterByMusicScore(array(12, 34)); // WHERE votccd_music_score IN (12, 34)
     * $query->filterByMusicScore(array('min' => 12)); // WHERE votccd_music_score > 12
     * </code>
     *
     * @param     mixed $musicScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByMusicScore($musicScore = null, $comparison = null)
    {
        if (is_array($musicScore)) {
            $useMinMax = false;
            if (isset($musicScore['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE, $musicScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($musicScore['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE, $musicScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE, $musicScore, $comparison);
    }

    /**
     * Filter the query on the votccd_style_score column
     *
     * Example usage:
     * <code>
     * $query->filterByStyleScore(1234); // WHERE votccd_style_score = 1234
     * $query->filterByStyleScore(array(12, 34)); // WHERE votccd_style_score IN (12, 34)
     * $query->filterByStyleScore(array('min' => 12)); // WHERE votccd_style_score > 12
     * </code>
     *
     * @param     mixed $styleScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByStyleScore($styleScore = null, $comparison = null)
    {
        if (is_array($styleScore)) {
            $useMinMax = false;
            if (isset($styleScore['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE, $styleScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($styleScore['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE, $styleScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE, $styleScore, $comparison);
    }

    /**
     * Filter the query on the votccd_total_score column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalScore(1234); // WHERE votccd_total_score = 1234
     * $query->filterByTotalScore(array(12, 34)); // WHERE votccd_total_score IN (12, 34)
     * $query->filterByTotalScore(array('min' => 12)); // WHERE votccd_total_score > 12
     * </code>
     *
     * @param     mixed $totalScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByTotalScore($totalScore = null, $comparison = null)
    {
        if (is_array($totalScore)) {
            $useMinMax = false;
            if (isset($totalScore['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE, $totalScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalScore['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE, $totalScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE, $totalScore, $comparison);
    }

    /**
     * Filter the query on the votccd_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE votccd_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE votccd_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE votccd_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the votccd_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE votccd_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE votccd_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE votccd_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VoteBundle\Model\Contest object
     *
     * @param \IiMedias\VoteBundle\Model\Contest|ObjectCollection $contest The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByContest($contest, $comparison = null)
    {
        if ($contest instanceof \IiMedias\VoteBundle\Model\Contest) {
            return $this
                ->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID, $contest->getId(), $comparison);
        } elseif ($contest instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID, $contest->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByContest() only accepts arguments of type \IiMedias\VoteBundle\Model\Contest or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contest relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function joinContest($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contest');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contest');
        }

        return $this;
    }

    /**
     * Use the Contest relation Contest object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VoteBundle\Model\ContestQuery A secondary query class using the current class as primary query
     */
    public function useContestQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContest($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contest', '\IiMedias\VoteBundle\Model\ContestQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VoteBundle\Model\Candidate object
     *
     * @param \IiMedias\VoteBundle\Model\Candidate|ObjectCollection $candidate The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByCandidate($candidate, $comparison = null)
    {
        if ($candidate instanceof \IiMedias\VoteBundle\Model\Candidate) {
            return $this
                ->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID, $candidate->getId(), $comparison);
        } elseif ($candidate instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID, $candidate->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCandidate() only accepts arguments of type \IiMedias\VoteBundle\Model\Candidate or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Candidate relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function joinCandidate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Candidate');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Candidate');
        }

        return $this;
    }

    /**
     * Use the Candidate relation Candidate object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VoteBundle\Model\CandidateQuery A secondary query class using the current class as primary query
     */
    public function useCandidateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCandidate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Candidate', '\IiMedias\VoteBundle\Model\CandidateQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VoteBundle\Model\Score object
     *
     * @param \IiMedias\VoteBundle\Model\Score|ObjectCollection $score the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContestCandidateQuery The current query, for fluid interface
     */
    public function filterByScore($score, $comparison = null)
    {
        if ($score instanceof \IiMedias\VoteBundle\Model\Score) {
            return $this
                ->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_ID, $score->getContestCandidateId(), $comparison);
        } elseif ($score instanceof ObjectCollection) {
            return $this
                ->useScoreQuery()
                ->filterByPrimaryKeys($score->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByScore() only accepts arguments of type \IiMedias\VoteBundle\Model\Score or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Score relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function joinScore($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Score');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Score');
        }

        return $this;
    }

    /**
     * Use the Score relation Score object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VoteBundle\Model\ScoreQuery A secondary query class using the current class as primary query
     */
    public function useScoreQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinScore($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Score', '\IiMedias\VoteBundle\Model\ScoreQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContestCandidate $contestCandidate Object to remove from the list of results
     *
     * @return $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function prune($contestCandidate = null)
    {
        if ($contestCandidate) {
            $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_ID, $contestCandidate->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vote_contest_candidate_votccd table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestCandidateTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContestCandidateTableMap::clearInstancePool();
            ContestCandidateTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestCandidateTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContestCandidateTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContestCandidateTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContestCandidateTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildContestCandidateQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT);
    }

} // ContestCandidateQuery
