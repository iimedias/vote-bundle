<?php

namespace IiMedias\VoteBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VoteBundle\Model\Contest as ChildContest;
use IiMedias\VoteBundle\Model\ContestQuery as ChildContestQuery;
use IiMedias\VoteBundle\Model\Map\ContestTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vote_contest_votcts' table.
 *
 *
 *
 * @method     ChildContestQuery orderById($order = Criteria::ASC) Order by the votcts_id column
 * @method     ChildContestQuery orderByName($order = Criteria::ASC) Order by the votcts_name column
 * @method     ChildContestQuery orderByCurrent($order = Criteria::ASC) Order by the votcts_current column
 * @method     ChildContestQuery orderByEnable($order = Criteria::ASC) Order by the votcts_enable column
 * @method     ChildContestQuery orderByCreatedAt($order = Criteria::ASC) Order by the votcts_created_at column
 * @method     ChildContestQuery orderByUpdatedAt($order = Criteria::ASC) Order by the votcts_updated_at column
 * @method     ChildContestQuery orderByVotctsSlug($order = Criteria::ASC) Order by the votcts_slug column
 *
 * @method     ChildContestQuery groupById() Group by the votcts_id column
 * @method     ChildContestQuery groupByName() Group by the votcts_name column
 * @method     ChildContestQuery groupByCurrent() Group by the votcts_current column
 * @method     ChildContestQuery groupByEnable() Group by the votcts_enable column
 * @method     ChildContestQuery groupByCreatedAt() Group by the votcts_created_at column
 * @method     ChildContestQuery groupByUpdatedAt() Group by the votcts_updated_at column
 * @method     ChildContestQuery groupByVotctsSlug() Group by the votcts_slug column
 *
 * @method     ChildContestQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContestQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContestQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContestQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContestQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContestQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContestQuery leftJoinContestCandidate($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContestCandidate relation
 * @method     ChildContestQuery rightJoinContestCandidate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContestCandidate relation
 * @method     ChildContestQuery innerJoinContestCandidate($relationAlias = null) Adds a INNER JOIN clause to the query using the ContestCandidate relation
 *
 * @method     ChildContestQuery joinWithContestCandidate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContestCandidate relation
 *
 * @method     ChildContestQuery leftJoinWithContestCandidate() Adds a LEFT JOIN clause and with to the query using the ContestCandidate relation
 * @method     ChildContestQuery rightJoinWithContestCandidate() Adds a RIGHT JOIN clause and with to the query using the ContestCandidate relation
 * @method     ChildContestQuery innerJoinWithContestCandidate() Adds a INNER JOIN clause and with to the query using the ContestCandidate relation
 *
 * @method     \IiMedias\VoteBundle\Model\ContestCandidateQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContest findOne(ConnectionInterface $con = null) Return the first ChildContest matching the query
 * @method     ChildContest findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContest matching the query, or a new ChildContest object populated from the query conditions when no match is found
 *
 * @method     ChildContest findOneById(int $votcts_id) Return the first ChildContest filtered by the votcts_id column
 * @method     ChildContest findOneByName(string $votcts_name) Return the first ChildContest filtered by the votcts_name column
 * @method     ChildContest findOneByCurrent(boolean $votcts_current) Return the first ChildContest filtered by the votcts_current column
 * @method     ChildContest findOneByEnable(boolean $votcts_enable) Return the first ChildContest filtered by the votcts_enable column
 * @method     ChildContest findOneByCreatedAt(string $votcts_created_at) Return the first ChildContest filtered by the votcts_created_at column
 * @method     ChildContest findOneByUpdatedAt(string $votcts_updated_at) Return the first ChildContest filtered by the votcts_updated_at column
 * @method     ChildContest findOneByVotctsSlug(string $votcts_slug) Return the first ChildContest filtered by the votcts_slug column *

 * @method     ChildContest requirePk($key, ConnectionInterface $con = null) Return the ChildContest by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContest requireOne(ConnectionInterface $con = null) Return the first ChildContest matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContest requireOneById(int $votcts_id) Return the first ChildContest filtered by the votcts_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContest requireOneByName(string $votcts_name) Return the first ChildContest filtered by the votcts_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContest requireOneByCurrent(boolean $votcts_current) Return the first ChildContest filtered by the votcts_current column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContest requireOneByEnable(boolean $votcts_enable) Return the first ChildContest filtered by the votcts_enable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContest requireOneByCreatedAt(string $votcts_created_at) Return the first ChildContest filtered by the votcts_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContest requireOneByUpdatedAt(string $votcts_updated_at) Return the first ChildContest filtered by the votcts_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContest requireOneByVotctsSlug(string $votcts_slug) Return the first ChildContest filtered by the votcts_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContest[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContest objects based on current ModelCriteria
 * @method     ChildContest[]|ObjectCollection findById(int $votcts_id) Return ChildContest objects filtered by the votcts_id column
 * @method     ChildContest[]|ObjectCollection findByName(string $votcts_name) Return ChildContest objects filtered by the votcts_name column
 * @method     ChildContest[]|ObjectCollection findByCurrent(boolean $votcts_current) Return ChildContest objects filtered by the votcts_current column
 * @method     ChildContest[]|ObjectCollection findByEnable(boolean $votcts_enable) Return ChildContest objects filtered by the votcts_enable column
 * @method     ChildContest[]|ObjectCollection findByCreatedAt(string $votcts_created_at) Return ChildContest objects filtered by the votcts_created_at column
 * @method     ChildContest[]|ObjectCollection findByUpdatedAt(string $votcts_updated_at) Return ChildContest objects filtered by the votcts_updated_at column
 * @method     ChildContest[]|ObjectCollection findByVotctsSlug(string $votcts_slug) Return ChildContest objects filtered by the votcts_slug column
 * @method     ChildContest[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContestQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VoteBundle\Model\Base\ContestQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VoteBundle\\Model\\Contest', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContestQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContestQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContestQuery) {
            return $criteria;
        }
        $query = new ChildContestQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContest|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContestTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContestTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContest A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT votcts_id, votcts_name, votcts_current, votcts_enable, votcts_created_at, votcts_updated_at, votcts_slug FROM vote_contest_votcts WHERE votcts_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContest $obj */
            $obj = new ChildContest();
            $obj->hydrate($row);
            ContestTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContest|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the votcts_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE votcts_id = 1234
     * $query->filterById(array(12, 34)); // WHERE votcts_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE votcts_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ContestTableMap::COL_VOTCTS_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ContestTableMap::COL_VOTCTS_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_ID, $id, $comparison);
    }

    /**
     * Filter the query on the votcts_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE votcts_name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE votcts_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the votcts_current column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrent(true); // WHERE votcts_current = true
     * $query->filterByCurrent('yes'); // WHERE votcts_current = true
     * </code>
     *
     * @param     boolean|string $current The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterByCurrent($current = null, $comparison = null)
    {
        if (is_string($current)) {
            $current = in_array(strtolower($current), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_CURRENT, $current, $comparison);
    }

    /**
     * Filter the query on the votcts_enable column
     *
     * Example usage:
     * <code>
     * $query->filterByEnable(true); // WHERE votcts_enable = true
     * $query->filterByEnable('yes'); // WHERE votcts_enable = true
     * </code>
     *
     * @param     boolean|string $enable The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterByEnable($enable = null, $comparison = null)
    {
        if (is_string($enable)) {
            $enable = in_array(strtolower($enable), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_ENABLE, $enable, $comparison);
    }

    /**
     * Filter the query on the votcts_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE votcts_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE votcts_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE votcts_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ContestTableMap::COL_VOTCTS_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ContestTableMap::COL_VOTCTS_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the votcts_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE votcts_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE votcts_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE votcts_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ContestTableMap::COL_VOTCTS_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ContestTableMap::COL_VOTCTS_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the votcts_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByVotctsSlug('fooValue');   // WHERE votcts_slug = 'fooValue'
     * $query->filterByVotctsSlug('%fooValue%', Criteria::LIKE); // WHERE votcts_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $votctsSlug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterByVotctsSlug($votctsSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($votctsSlug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_SLUG, $votctsSlug, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VoteBundle\Model\ContestCandidate object
     *
     * @param \IiMedias\VoteBundle\Model\ContestCandidate|ObjectCollection $contestCandidate the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContestQuery The current query, for fluid interface
     */
    public function filterByContestCandidate($contestCandidate, $comparison = null)
    {
        if ($contestCandidate instanceof \IiMedias\VoteBundle\Model\ContestCandidate) {
            return $this
                ->addUsingAlias(ContestTableMap::COL_VOTCTS_ID, $contestCandidate->getContestId(), $comparison);
        } elseif ($contestCandidate instanceof ObjectCollection) {
            return $this
                ->useContestCandidateQuery()
                ->filterByPrimaryKeys($contestCandidate->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContestCandidate() only accepts arguments of type \IiMedias\VoteBundle\Model\ContestCandidate or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContestCandidate relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function joinContestCandidate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContestCandidate');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContestCandidate');
        }

        return $this;
    }

    /**
     * Use the ContestCandidate relation ContestCandidate object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VoteBundle\Model\ContestCandidateQuery A secondary query class using the current class as primary query
     */
    public function useContestCandidateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContestCandidate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContestCandidate', '\IiMedias\VoteBundle\Model\ContestCandidateQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContest $contest Object to remove from the list of results
     *
     * @return $this|ChildContestQuery The current query, for fluid interface
     */
    public function prune($contest = null)
    {
        if ($contest) {
            $this->addUsingAlias(ContestTableMap::COL_VOTCTS_ID, $contest->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vote_contest_votcts table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContestTableMap::clearInstancePool();
            ContestTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContestTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContestTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContestTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildContestQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildContestQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ContestTableMap::COL_VOTCTS_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildContestQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ContestTableMap::COL_VOTCTS_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildContestQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ContestTableMap::COL_VOTCTS_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildContestQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildContestQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ContestTableMap::COL_VOTCTS_CREATED_AT);
    }

    // sluggable behavior

    /**
     * Filter the query on the slug column
     *
     * @param     string $slug The value to use as filter.
     *
     * @return    $this|ChildContestQuery The current query, for fluid interface
     */
    public function filterBySlug($slug)
    {
        return $this->addUsingAlias(ContestTableMap::COL_VOTCTS_SLUG, $slug, Criteria::EQUAL);
    }

    /**
     * Find one object based on its slug
     *
     * @param     string $slug The value to use as filter.
     * @param     ConnectionInterface $con The optional connection object
     *
     * @return    ChildContest the result, formatted by the current formatter
     */
    public function findOneBySlug($slug, $con = null)
    {
        return $this->filterBySlug($slug)->findOne($con);
    }

} // ContestQuery
