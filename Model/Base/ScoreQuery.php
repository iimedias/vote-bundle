<?php

namespace IiMedias\VoteBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VoteBundle\Model\Score as ChildScore;
use IiMedias\VoteBundle\Model\ScoreQuery as ChildScoreQuery;
use IiMedias\VoteBundle\Model\Map\ScoreTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vote_score_votsco' table.
 *
 *
 *
 * @method     ChildScoreQuery orderById($order = Criteria::ASC) Order by the votsco_id column
 * @method     ChildScoreQuery orderByContestCandidateId($order = Criteria::ASC) Order by the votsco_votccd_id column
 * @method     ChildScoreQuery orderByVoterId($order = Criteria::ASC) Order by the votsco_votvot_id column
 * @method     ChildScoreQuery orderBySceneScore($order = Criteria::ASC) Order by the votsco_scene_score column
 * @method     ChildScoreQuery orderByTechnicScore($order = Criteria::ASC) Order by the votsco_technic_score column
 * @method     ChildScoreQuery orderByCoupleScore($order = Criteria::ASC) Order by the votsco_couple_score column
 * @method     ChildScoreQuery orderByMusicScore($order = Criteria::ASC) Order by the votsco_music_score column
 * @method     ChildScoreQuery orderByStyleScore($order = Criteria::ASC) Order by the votsco_style_score column
 * @method     ChildScoreQuery orderByTotalScore($order = Criteria::ASC) Order by the votsco_total_score column
 * @method     ChildScoreQuery orderByVotscoCreatedAt($order = Criteria::ASC) Order by the votsco_created_at column
 * @method     ChildScoreQuery orderByVotscoUpdatedAt($order = Criteria::ASC) Order by the votsco_updated_at column
 *
 * @method     ChildScoreQuery groupById() Group by the votsco_id column
 * @method     ChildScoreQuery groupByContestCandidateId() Group by the votsco_votccd_id column
 * @method     ChildScoreQuery groupByVoterId() Group by the votsco_votvot_id column
 * @method     ChildScoreQuery groupBySceneScore() Group by the votsco_scene_score column
 * @method     ChildScoreQuery groupByTechnicScore() Group by the votsco_technic_score column
 * @method     ChildScoreQuery groupByCoupleScore() Group by the votsco_couple_score column
 * @method     ChildScoreQuery groupByMusicScore() Group by the votsco_music_score column
 * @method     ChildScoreQuery groupByStyleScore() Group by the votsco_style_score column
 * @method     ChildScoreQuery groupByTotalScore() Group by the votsco_total_score column
 * @method     ChildScoreQuery groupByVotscoCreatedAt() Group by the votsco_created_at column
 * @method     ChildScoreQuery groupByVotscoUpdatedAt() Group by the votsco_updated_at column
 *
 * @method     ChildScoreQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildScoreQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildScoreQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildScoreQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildScoreQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildScoreQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildScoreQuery leftJoinContestCandidate($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContestCandidate relation
 * @method     ChildScoreQuery rightJoinContestCandidate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContestCandidate relation
 * @method     ChildScoreQuery innerJoinContestCandidate($relationAlias = null) Adds a INNER JOIN clause to the query using the ContestCandidate relation
 *
 * @method     ChildScoreQuery joinWithContestCandidate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContestCandidate relation
 *
 * @method     ChildScoreQuery leftJoinWithContestCandidate() Adds a LEFT JOIN clause and with to the query using the ContestCandidate relation
 * @method     ChildScoreQuery rightJoinWithContestCandidate() Adds a RIGHT JOIN clause and with to the query using the ContestCandidate relation
 * @method     ChildScoreQuery innerJoinWithContestCandidate() Adds a INNER JOIN clause and with to the query using the ContestCandidate relation
 *
 * @method     ChildScoreQuery leftJoinVoter($relationAlias = null) Adds a LEFT JOIN clause to the query using the Voter relation
 * @method     ChildScoreQuery rightJoinVoter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Voter relation
 * @method     ChildScoreQuery innerJoinVoter($relationAlias = null) Adds a INNER JOIN clause to the query using the Voter relation
 *
 * @method     ChildScoreQuery joinWithVoter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Voter relation
 *
 * @method     ChildScoreQuery leftJoinWithVoter() Adds a LEFT JOIN clause and with to the query using the Voter relation
 * @method     ChildScoreQuery rightJoinWithVoter() Adds a RIGHT JOIN clause and with to the query using the Voter relation
 * @method     ChildScoreQuery innerJoinWithVoter() Adds a INNER JOIN clause and with to the query using the Voter relation
 *
 * @method     \IiMedias\VoteBundle\Model\ContestCandidateQuery|\IiMedias\VoteBundle\Model\VoterQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildScore findOne(ConnectionInterface $con = null) Return the first ChildScore matching the query
 * @method     ChildScore findOneOrCreate(ConnectionInterface $con = null) Return the first ChildScore matching the query, or a new ChildScore object populated from the query conditions when no match is found
 *
 * @method     ChildScore findOneById(int $votsco_id) Return the first ChildScore filtered by the votsco_id column
 * @method     ChildScore findOneByContestCandidateId(int $votsco_votccd_id) Return the first ChildScore filtered by the votsco_votccd_id column
 * @method     ChildScore findOneByVoterId(int $votsco_votvot_id) Return the first ChildScore filtered by the votsco_votvot_id column
 * @method     ChildScore findOneBySceneScore(int $votsco_scene_score) Return the first ChildScore filtered by the votsco_scene_score column
 * @method     ChildScore findOneByTechnicScore(int $votsco_technic_score) Return the first ChildScore filtered by the votsco_technic_score column
 * @method     ChildScore findOneByCoupleScore(int $votsco_couple_score) Return the first ChildScore filtered by the votsco_couple_score column
 * @method     ChildScore findOneByMusicScore(int $votsco_music_score) Return the first ChildScore filtered by the votsco_music_score column
 * @method     ChildScore findOneByStyleScore(int $votsco_style_score) Return the first ChildScore filtered by the votsco_style_score column
 * @method     ChildScore findOneByTotalScore(int $votsco_total_score) Return the first ChildScore filtered by the votsco_total_score column
 * @method     ChildScore findOneByVotscoCreatedAt(string $votsco_created_at) Return the first ChildScore filtered by the votsco_created_at column
 * @method     ChildScore findOneByVotscoUpdatedAt(string $votsco_updated_at) Return the first ChildScore filtered by the votsco_updated_at column *

 * @method     ChildScore requirePk($key, ConnectionInterface $con = null) Return the ChildScore by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOne(ConnectionInterface $con = null) Return the first ChildScore matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildScore requireOneById(int $votsco_id) Return the first ChildScore filtered by the votsco_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByContestCandidateId(int $votsco_votccd_id) Return the first ChildScore filtered by the votsco_votccd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByVoterId(int $votsco_votvot_id) Return the first ChildScore filtered by the votsco_votvot_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneBySceneScore(int $votsco_scene_score) Return the first ChildScore filtered by the votsco_scene_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByTechnicScore(int $votsco_technic_score) Return the first ChildScore filtered by the votsco_technic_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByCoupleScore(int $votsco_couple_score) Return the first ChildScore filtered by the votsco_couple_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByMusicScore(int $votsco_music_score) Return the first ChildScore filtered by the votsco_music_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByStyleScore(int $votsco_style_score) Return the first ChildScore filtered by the votsco_style_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByTotalScore(int $votsco_total_score) Return the first ChildScore filtered by the votsco_total_score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByVotscoCreatedAt(string $votsco_created_at) Return the first ChildScore filtered by the votsco_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildScore requireOneByVotscoUpdatedAt(string $votsco_updated_at) Return the first ChildScore filtered by the votsco_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildScore[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildScore objects based on current ModelCriteria
 * @method     ChildScore[]|ObjectCollection findById(int $votsco_id) Return ChildScore objects filtered by the votsco_id column
 * @method     ChildScore[]|ObjectCollection findByContestCandidateId(int $votsco_votccd_id) Return ChildScore objects filtered by the votsco_votccd_id column
 * @method     ChildScore[]|ObjectCollection findByVoterId(int $votsco_votvot_id) Return ChildScore objects filtered by the votsco_votvot_id column
 * @method     ChildScore[]|ObjectCollection findBySceneScore(int $votsco_scene_score) Return ChildScore objects filtered by the votsco_scene_score column
 * @method     ChildScore[]|ObjectCollection findByTechnicScore(int $votsco_technic_score) Return ChildScore objects filtered by the votsco_technic_score column
 * @method     ChildScore[]|ObjectCollection findByCoupleScore(int $votsco_couple_score) Return ChildScore objects filtered by the votsco_couple_score column
 * @method     ChildScore[]|ObjectCollection findByMusicScore(int $votsco_music_score) Return ChildScore objects filtered by the votsco_music_score column
 * @method     ChildScore[]|ObjectCollection findByStyleScore(int $votsco_style_score) Return ChildScore objects filtered by the votsco_style_score column
 * @method     ChildScore[]|ObjectCollection findByTotalScore(int $votsco_total_score) Return ChildScore objects filtered by the votsco_total_score column
 * @method     ChildScore[]|ObjectCollection findByVotscoCreatedAt(string $votsco_created_at) Return ChildScore objects filtered by the votsco_created_at column
 * @method     ChildScore[]|ObjectCollection findByVotscoUpdatedAt(string $votsco_updated_at) Return ChildScore objects filtered by the votsco_updated_at column
 * @method     ChildScore[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ScoreQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VoteBundle\Model\Base\ScoreQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VoteBundle\\Model\\Score', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildScoreQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildScoreQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildScoreQuery) {
            return $criteria;
        }
        $query = new ChildScoreQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildScore|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ScoreTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ScoreTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildScore A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT votsco_id, votsco_votccd_id, votsco_votvot_id, votsco_scene_score, votsco_technic_score, votsco_couple_score, votsco_music_score, votsco_style_score, votsco_total_score, votsco_created_at, votsco_updated_at FROM vote_score_votsco WHERE votsco_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildScore $obj */
            $obj = new ChildScore();
            $obj->hydrate($row);
            ScoreTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildScore|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the votsco_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE votsco_id = 1234
     * $query->filterById(array(12, 34)); // WHERE votsco_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE votsco_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the votsco_votccd_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContestCandidateId(1234); // WHERE votsco_votccd_id = 1234
     * $query->filterByContestCandidateId(array(12, 34)); // WHERE votsco_votccd_id IN (12, 34)
     * $query->filterByContestCandidateId(array('min' => 12)); // WHERE votsco_votccd_id > 12
     * </code>
     *
     * @see       filterByContestCandidate()
     *
     * @param     mixed $contestCandidateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByContestCandidateId($contestCandidateId = null, $comparison = null)
    {
        if (is_array($contestCandidateId)) {
            $useMinMax = false;
            if (isset($contestCandidateId['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTCCD_ID, $contestCandidateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contestCandidateId['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTCCD_ID, $contestCandidateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTCCD_ID, $contestCandidateId, $comparison);
    }

    /**
     * Filter the query on the votsco_votvot_id column
     *
     * Example usage:
     * <code>
     * $query->filterByVoterId(1234); // WHERE votsco_votvot_id = 1234
     * $query->filterByVoterId(array(12, 34)); // WHERE votsco_votvot_id IN (12, 34)
     * $query->filterByVoterId(array('min' => 12)); // WHERE votsco_votvot_id > 12
     * </code>
     *
     * @see       filterByVoter()
     *
     * @param     mixed $voterId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByVoterId($voterId = null, $comparison = null)
    {
        if (is_array($voterId)) {
            $useMinMax = false;
            if (isset($voterId['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTVOT_ID, $voterId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($voterId['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTVOT_ID, $voterId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTVOT_ID, $voterId, $comparison);
    }

    /**
     * Filter the query on the votsco_scene_score column
     *
     * Example usage:
     * <code>
     * $query->filterBySceneScore(1234); // WHERE votsco_scene_score = 1234
     * $query->filterBySceneScore(array(12, 34)); // WHERE votsco_scene_score IN (12, 34)
     * $query->filterBySceneScore(array('min' => 12)); // WHERE votsco_scene_score > 12
     * </code>
     *
     * @param     mixed $sceneScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterBySceneScore($sceneScore = null, $comparison = null)
    {
        if (is_array($sceneScore)) {
            $useMinMax = false;
            if (isset($sceneScore['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_SCENE_SCORE, $sceneScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sceneScore['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_SCENE_SCORE, $sceneScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_SCENE_SCORE, $sceneScore, $comparison);
    }

    /**
     * Filter the query on the votsco_technic_score column
     *
     * Example usage:
     * <code>
     * $query->filterByTechnicScore(1234); // WHERE votsco_technic_score = 1234
     * $query->filterByTechnicScore(array(12, 34)); // WHERE votsco_technic_score IN (12, 34)
     * $query->filterByTechnicScore(array('min' => 12)); // WHERE votsco_technic_score > 12
     * </code>
     *
     * @param     mixed $technicScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByTechnicScore($technicScore = null, $comparison = null)
    {
        if (is_array($technicScore)) {
            $useMinMax = false;
            if (isset($technicScore['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_TECHNIC_SCORE, $technicScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($technicScore['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_TECHNIC_SCORE, $technicScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_TECHNIC_SCORE, $technicScore, $comparison);
    }

    /**
     * Filter the query on the votsco_couple_score column
     *
     * Example usage:
     * <code>
     * $query->filterByCoupleScore(1234); // WHERE votsco_couple_score = 1234
     * $query->filterByCoupleScore(array(12, 34)); // WHERE votsco_couple_score IN (12, 34)
     * $query->filterByCoupleScore(array('min' => 12)); // WHERE votsco_couple_score > 12
     * </code>
     *
     * @param     mixed $coupleScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByCoupleScore($coupleScore = null, $comparison = null)
    {
        if (is_array($coupleScore)) {
            $useMinMax = false;
            if (isset($coupleScore['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_COUPLE_SCORE, $coupleScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($coupleScore['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_COUPLE_SCORE, $coupleScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_COUPLE_SCORE, $coupleScore, $comparison);
    }

    /**
     * Filter the query on the votsco_music_score column
     *
     * Example usage:
     * <code>
     * $query->filterByMusicScore(1234); // WHERE votsco_music_score = 1234
     * $query->filterByMusicScore(array(12, 34)); // WHERE votsco_music_score IN (12, 34)
     * $query->filterByMusicScore(array('min' => 12)); // WHERE votsco_music_score > 12
     * </code>
     *
     * @param     mixed $musicScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByMusicScore($musicScore = null, $comparison = null)
    {
        if (is_array($musicScore)) {
            $useMinMax = false;
            if (isset($musicScore['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_MUSIC_SCORE, $musicScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($musicScore['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_MUSIC_SCORE, $musicScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_MUSIC_SCORE, $musicScore, $comparison);
    }

    /**
     * Filter the query on the votsco_style_score column
     *
     * Example usage:
     * <code>
     * $query->filterByStyleScore(1234); // WHERE votsco_style_score = 1234
     * $query->filterByStyleScore(array(12, 34)); // WHERE votsco_style_score IN (12, 34)
     * $query->filterByStyleScore(array('min' => 12)); // WHERE votsco_style_score > 12
     * </code>
     *
     * @param     mixed $styleScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByStyleScore($styleScore = null, $comparison = null)
    {
        if (is_array($styleScore)) {
            $useMinMax = false;
            if (isset($styleScore['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_STYLE_SCORE, $styleScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($styleScore['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_STYLE_SCORE, $styleScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_STYLE_SCORE, $styleScore, $comparison);
    }

    /**
     * Filter the query on the votsco_total_score column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalScore(1234); // WHERE votsco_total_score = 1234
     * $query->filterByTotalScore(array(12, 34)); // WHERE votsco_total_score IN (12, 34)
     * $query->filterByTotalScore(array('min' => 12)); // WHERE votsco_total_score > 12
     * </code>
     *
     * @param     mixed $totalScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByTotalScore($totalScore = null, $comparison = null)
    {
        if (is_array($totalScore)) {
            $useMinMax = false;
            if (isset($totalScore['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_TOTAL_SCORE, $totalScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalScore['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_TOTAL_SCORE, $totalScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_TOTAL_SCORE, $totalScore, $comparison);
    }

    /**
     * Filter the query on the votsco_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByVotscoCreatedAt('2011-03-14'); // WHERE votsco_created_at = '2011-03-14'
     * $query->filterByVotscoCreatedAt('now'); // WHERE votsco_created_at = '2011-03-14'
     * $query->filterByVotscoCreatedAt(array('max' => 'yesterday')); // WHERE votsco_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $votscoCreatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByVotscoCreatedAt($votscoCreatedAt = null, $comparison = null)
    {
        if (is_array($votscoCreatedAt)) {
            $useMinMax = false;
            if (isset($votscoCreatedAt['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_CREATED_AT, $votscoCreatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($votscoCreatedAt['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_CREATED_AT, $votscoCreatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_CREATED_AT, $votscoCreatedAt, $comparison);
    }

    /**
     * Filter the query on the votsco_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByVotscoUpdatedAt('2011-03-14'); // WHERE votsco_updated_at = '2011-03-14'
     * $query->filterByVotscoUpdatedAt('now'); // WHERE votsco_updated_at = '2011-03-14'
     * $query->filterByVotscoUpdatedAt(array('max' => 'yesterday')); // WHERE votsco_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $votscoUpdatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function filterByVotscoUpdatedAt($votscoUpdatedAt = null, $comparison = null)
    {
        if (is_array($votscoUpdatedAt)) {
            $useMinMax = false;
            if (isset($votscoUpdatedAt['min'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_UPDATED_AT, $votscoUpdatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($votscoUpdatedAt['max'])) {
                $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_UPDATED_AT, $votscoUpdatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_UPDATED_AT, $votscoUpdatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VoteBundle\Model\ContestCandidate object
     *
     * @param \IiMedias\VoteBundle\Model\ContestCandidate|ObjectCollection $contestCandidate The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildScoreQuery The current query, for fluid interface
     */
    public function filterByContestCandidate($contestCandidate, $comparison = null)
    {
        if ($contestCandidate instanceof \IiMedias\VoteBundle\Model\ContestCandidate) {
            return $this
                ->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTCCD_ID, $contestCandidate->getId(), $comparison);
        } elseif ($contestCandidate instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTCCD_ID, $contestCandidate->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByContestCandidate() only accepts arguments of type \IiMedias\VoteBundle\Model\ContestCandidate or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContestCandidate relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function joinContestCandidate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContestCandidate');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContestCandidate');
        }

        return $this;
    }

    /**
     * Use the ContestCandidate relation ContestCandidate object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VoteBundle\Model\ContestCandidateQuery A secondary query class using the current class as primary query
     */
    public function useContestCandidateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContestCandidate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContestCandidate', '\IiMedias\VoteBundle\Model\ContestCandidateQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VoteBundle\Model\Voter object
     *
     * @param \IiMedias\VoteBundle\Model\Voter|ObjectCollection $voter The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildScoreQuery The current query, for fluid interface
     */
    public function filterByVoter($voter, $comparison = null)
    {
        if ($voter instanceof \IiMedias\VoteBundle\Model\Voter) {
            return $this
                ->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTVOT_ID, $voter->getId(), $comparison);
        } elseif ($voter instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ScoreTableMap::COL_VOTSCO_VOTVOT_ID, $voter->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByVoter() only accepts arguments of type \IiMedias\VoteBundle\Model\Voter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Voter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function joinVoter($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Voter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Voter');
        }

        return $this;
    }

    /**
     * Use the Voter relation Voter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VoteBundle\Model\VoterQuery A secondary query class using the current class as primary query
     */
    public function useVoterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVoter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Voter', '\IiMedias\VoteBundle\Model\VoterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildScore $score Object to remove from the list of results
     *
     * @return $this|ChildScoreQuery The current query, for fluid interface
     */
    public function prune($score = null)
    {
        if ($score) {
            $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_ID, $score->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vote_score_votsco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ScoreTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ScoreTableMap::clearInstancePool();
            ScoreTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ScoreTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ScoreTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ScoreTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ScoreTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildScoreQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildScoreQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ScoreTableMap::COL_VOTSCO_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildScoreQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ScoreTableMap::COL_VOTSCO_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildScoreQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ScoreTableMap::COL_VOTSCO_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildScoreQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ScoreTableMap::COL_VOTSCO_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildScoreQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ScoreTableMap::COL_VOTSCO_CREATED_AT);
    }

} // ScoreQuery
