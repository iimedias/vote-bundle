<?php

namespace IiMedias\VoteBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VoteBundle\Model\Score as ChildScore;
use IiMedias\VoteBundle\Model\ScoreQuery as ChildScoreQuery;
use IiMedias\VoteBundle\Model\Voter as ChildVoter;
use IiMedias\VoteBundle\Model\VoterQuery as ChildVoterQuery;
use IiMedias\VoteBundle\Model\Map\ScoreTableMap;
use IiMedias\VoteBundle\Model\Map\VoterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'vote_voter_votvot' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VoteBundle.Model.Base
 */
abstract class Voter implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VoteBundle\\Model\\Map\\VoterTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the votvot_id field.
     *
     * @var        int
     */
    protected $votvot_id;

    /**
     * The value for the votvot_username field.
     *
     * @var        string
     */
    protected $votvot_username;

    /**
     * The value for the votvot_display_name field.
     *
     * @var        string
     */
    protected $votvot_display_name;

    /**
     * The value for the votvot_country field.
     *
     * @var        string
     */
    protected $votvot_country;

    /**
     * The value for the votvot_connection field.
     *
     * @var        boolean
     */
    protected $votvot_connection;

    /**
     * The value for the votvot_enable field.
     *
     * @var        boolean
     */
    protected $votvot_enable;

    /**
     * The value for the votvot_created_at field.
     *
     * @var        DateTime
     */
    protected $votvot_created_at;

    /**
     * The value for the votvot_updated_at field.
     *
     * @var        DateTime
     */
    protected $votvot_updated_at;

    /**
     * The value for the votvot_slug field.
     *
     * @var        string
     */
    protected $votvot_slug;

    /**
     * @var        ObjectCollection|ChildScore[] Collection to store aggregation of ChildScore objects.
     */
    protected $collScores;
    protected $collScoresPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildScore[]
     */
    protected $scoresScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VoteBundle\Model\Base\Voter object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Voter</code> instance.  If
     * <code>obj</code> is an instance of <code>Voter</code>, delegates to
     * <code>equals(Voter)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Voter The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [votvot_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->votvot_id;
    }

    /**
     * Get the [votvot_username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->votvot_username;
    }

    /**
     * Get the [votvot_display_name] column value.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->votvot_display_name;
    }

    /**
     * Get the [votvot_country] column value.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->votvot_country;
    }

    /**
     * Get the [votvot_connection] column value.
     *
     * @return boolean
     */
    public function getConnection()
    {
        return $this->votvot_connection;
    }

    /**
     * Get the [votvot_connection] column value.
     *
     * @return boolean
     */
    public function isConnection()
    {
        return $this->getConnection();
    }

    /**
     * Get the [votvot_enable] column value.
     *
     * @return boolean
     */
    public function getEnable()
    {
        return $this->votvot_enable;
    }

    /**
     * Get the [votvot_enable] column value.
     *
     * @return boolean
     */
    public function isEnable()
    {
        return $this->getEnable();
    }

    /**
     * Get the [optionally formatted] temporal [votvot_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->votvot_created_at;
        } else {
            return $this->votvot_created_at instanceof \DateTimeInterface ? $this->votvot_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [votvot_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->votvot_updated_at;
        } else {
            return $this->votvot_updated_at instanceof \DateTimeInterface ? $this->votvot_updated_at->format($format) : null;
        }
    }

    /**
     * Get the [votvot_slug] column value.
     *
     * @return string
     */
    public function getVotvotSlug()
    {
        return $this->votvot_slug;
    }

    /**
     * Set the value of [votvot_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->votvot_id !== $v) {
            $this->votvot_id = $v;
            $this->modifiedColumns[VoterTableMap::COL_VOTVOT_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [votvot_username] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->votvot_username !== $v) {
            $this->votvot_username = $v;
            $this->modifiedColumns[VoterTableMap::COL_VOTVOT_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [votvot_display_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setDisplayName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->votvot_display_name !== $v) {
            $this->votvot_display_name = $v;
            $this->modifiedColumns[VoterTableMap::COL_VOTVOT_DISPLAY_NAME] = true;
        }

        return $this;
    } // setDisplayName()

    /**
     * Set the value of [votvot_country] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->votvot_country !== $v) {
            $this->votvot_country = $v;
            $this->modifiedColumns[VoterTableMap::COL_VOTVOT_COUNTRY] = true;
        }

        return $this;
    } // setCountry()

    /**
     * Sets the value of the [votvot_connection] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setConnection($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votvot_connection !== $v) {
            $this->votvot_connection = $v;
            $this->modifiedColumns[VoterTableMap::COL_VOTVOT_CONNECTION] = true;
        }

        return $this;
    } // setConnection()

    /**
     * Sets the value of the [votvot_enable] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setEnable($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->votvot_enable !== $v) {
            $this->votvot_enable = $v;
            $this->modifiedColumns[VoterTableMap::COL_VOTVOT_ENABLE] = true;
        }

        return $this;
    } // setEnable()

    /**
     * Sets the value of [votvot_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->votvot_created_at !== null || $dt !== null) {
            if ($this->votvot_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->votvot_created_at->format("Y-m-d H:i:s.u")) {
                $this->votvot_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[VoterTableMap::COL_VOTVOT_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [votvot_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->votvot_updated_at !== null || $dt !== null) {
            if ($this->votvot_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->votvot_updated_at->format("Y-m-d H:i:s.u")) {
                $this->votvot_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[VoterTableMap::COL_VOTVOT_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Set the value of [votvot_slug] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function setVotvotSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->votvot_slug !== $v) {
            $this->votvot_slug = $v;
            $this->modifiedColumns[VoterTableMap::COL_VOTVOT_SLUG] = true;
        }

        return $this;
    } // setVotvotSlug()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : VoterTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votvot_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : VoterTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votvot_username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : VoterTableMap::translateFieldName('DisplayName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votvot_display_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : VoterTableMap::translateFieldName('Country', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votvot_country = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : VoterTableMap::translateFieldName('Connection', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votvot_connection = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : VoterTableMap::translateFieldName('Enable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votvot_enable = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : VoterTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->votvot_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : VoterTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->votvot_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : VoterTableMap::translateFieldName('VotvotSlug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->votvot_slug = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = VoterTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VoteBundle\\Model\\Voter'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VoterTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildVoterQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collScores = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Voter::setDeleted()
     * @see Voter::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(VoterTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildVoterQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(VoterTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            // sluggable behavior

            if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_SLUG) && $this->getVotvotSlug()) {
                $this->setVotvotSlug($this->makeSlugUnique($this->getVotvotSlug()));
            } elseif (!$this->getVotvotSlug()) {
                $this->setVotvotSlug($this->createSlug());
            }
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(VoterTableMap::COL_VOTVOT_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(VoterTableMap::COL_VOTVOT_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(VoterTableMap::COL_VOTVOT_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                VoterTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->scoresScheduledForDeletion !== null) {
                if (!$this->scoresScheduledForDeletion->isEmpty()) {
                    \IiMedias\VoteBundle\Model\ScoreQuery::create()
                        ->filterByPrimaryKeys($this->scoresScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->scoresScheduledForDeletion = null;
                }
            }

            if ($this->collScores !== null) {
                foreach ($this->collScores as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[VoterTableMap::COL_VOTVOT_ID] = true;
        if (null !== $this->votvot_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . VoterTableMap::COL_VOTVOT_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_id';
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_username';
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_DISPLAY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_display_name';
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_country';
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_CONNECTION)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_connection';
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_ENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_enable';
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_created_at';
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_updated_at';
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'votvot_slug';
        }

        $sql = sprintf(
            'INSERT INTO vote_voter_votvot (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'votvot_id':
                        $stmt->bindValue($identifier, $this->votvot_id, PDO::PARAM_INT);
                        break;
                    case 'votvot_username':
                        $stmt->bindValue($identifier, $this->votvot_username, PDO::PARAM_STR);
                        break;
                    case 'votvot_display_name':
                        $stmt->bindValue($identifier, $this->votvot_display_name, PDO::PARAM_STR);
                        break;
                    case 'votvot_country':
                        $stmt->bindValue($identifier, $this->votvot_country, PDO::PARAM_STR);
                        break;
                    case 'votvot_connection':
                        $stmt->bindValue($identifier, (int) $this->votvot_connection, PDO::PARAM_INT);
                        break;
                    case 'votvot_enable':
                        $stmt->bindValue($identifier, (int) $this->votvot_enable, PDO::PARAM_INT);
                        break;
                    case 'votvot_created_at':
                        $stmt->bindValue($identifier, $this->votvot_created_at ? $this->votvot_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'votvot_updated_at':
                        $stmt->bindValue($identifier, $this->votvot_updated_at ? $this->votvot_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'votvot_slug':
                        $stmt->bindValue($identifier, $this->votvot_slug, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = VoterTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getDisplayName();
                break;
            case 3:
                return $this->getCountry();
                break;
            case 4:
                return $this->getConnection();
                break;
            case 5:
                return $this->getEnable();
                break;
            case 6:
                return $this->getCreatedAt();
                break;
            case 7:
                return $this->getUpdatedAt();
                break;
            case 8:
                return $this->getVotvotSlug();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Voter'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Voter'][$this->hashCode()] = true;
        $keys = VoterTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getDisplayName(),
            $keys[3] => $this->getCountry(),
            $keys[4] => $this->getConnection(),
            $keys[5] => $this->getEnable(),
            $keys[6] => $this->getCreatedAt(),
            $keys[7] => $this->getUpdatedAt(),
            $keys[8] => $this->getVotvotSlug(),
        );
        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collScores) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'scores';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'vote_score_votscos';
                        break;
                    default:
                        $key = 'Scores';
                }

                $result[$key] = $this->collScores->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VoteBundle\Model\Voter
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = VoterTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VoteBundle\Model\Voter
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setDisplayName($value);
                break;
            case 3:
                $this->setCountry($value);
                break;
            case 4:
                $this->setConnection($value);
                break;
            case 5:
                $this->setEnable($value);
                break;
            case 6:
                $this->setCreatedAt($value);
                break;
            case 7:
                $this->setUpdatedAt($value);
                break;
            case 8:
                $this->setVotvotSlug($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = VoterTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUsername($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDisplayName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCountry($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setConnection($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setEnable($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCreatedAt($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setUpdatedAt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setVotvotSlug($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(VoterTableMap::DATABASE_NAME);

        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_ID)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_ID, $this->votvot_id);
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_USERNAME)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_USERNAME, $this->votvot_username);
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_DISPLAY_NAME)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_DISPLAY_NAME, $this->votvot_display_name);
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_COUNTRY)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_COUNTRY, $this->votvot_country);
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_CONNECTION)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_CONNECTION, $this->votvot_connection);
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_ENABLE)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_ENABLE, $this->votvot_enable);
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_CREATED_AT)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_CREATED_AT, $this->votvot_created_at);
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_UPDATED_AT)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_UPDATED_AT, $this->votvot_updated_at);
        }
        if ($this->isColumnModified(VoterTableMap::COL_VOTVOT_SLUG)) {
            $criteria->add(VoterTableMap::COL_VOTVOT_SLUG, $this->votvot_slug);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildVoterQuery::create();
        $criteria->add(VoterTableMap::COL_VOTVOT_ID, $this->votvot_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (votvot_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VoteBundle\Model\Voter (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setDisplayName($this->getDisplayName());
        $copyObj->setCountry($this->getCountry());
        $copyObj->setConnection($this->getConnection());
        $copyObj->setEnable($this->getEnable());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setVotvotSlug($this->getVotvotSlug());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getScores() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addScore($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VoteBundle\Model\Voter Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Score' == $relationName) {
            $this->initScores();
            return;
        }
    }

    /**
     * Clears out the collScores collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addScores()
     */
    public function clearScores()
    {
        $this->collScores = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collScores collection loaded partially.
     */
    public function resetPartialScores($v = true)
    {
        $this->collScoresPartial = $v;
    }

    /**
     * Initializes the collScores collection.
     *
     * By default this just sets the collScores collection to an empty array (like clearcollScores());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initScores($overrideExisting = true)
    {
        if (null !== $this->collScores && !$overrideExisting) {
            return;
        }

        $collectionClassName = ScoreTableMap::getTableMap()->getCollectionClassName();

        $this->collScores = new $collectionClassName;
        $this->collScores->setModel('\IiMedias\VoteBundle\Model\Score');
    }

    /**
     * Gets an array of ChildScore objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildVoter is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildScore[] List of ChildScore objects
     * @throws PropelException
     */
    public function getScores(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collScoresPartial && !$this->isNew();
        if (null === $this->collScores || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collScores) {
                // return empty collection
                $this->initScores();
            } else {
                $collScores = ChildScoreQuery::create(null, $criteria)
                    ->filterByVoter($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collScoresPartial && count($collScores)) {
                        $this->initScores(false);

                        foreach ($collScores as $obj) {
                            if (false == $this->collScores->contains($obj)) {
                                $this->collScores->append($obj);
                            }
                        }

                        $this->collScoresPartial = true;
                    }

                    return $collScores;
                }

                if ($partial && $this->collScores) {
                    foreach ($this->collScores as $obj) {
                        if ($obj->isNew()) {
                            $collScores[] = $obj;
                        }
                    }
                }

                $this->collScores = $collScores;
                $this->collScoresPartial = false;
            }
        }

        return $this->collScores;
    }

    /**
     * Sets a collection of ChildScore objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $scores A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildVoter The current object (for fluent API support)
     */
    public function setScores(Collection $scores, ConnectionInterface $con = null)
    {
        /** @var ChildScore[] $scoresToDelete */
        $scoresToDelete = $this->getScores(new Criteria(), $con)->diff($scores);


        $this->scoresScheduledForDeletion = $scoresToDelete;

        foreach ($scoresToDelete as $scoreRemoved) {
            $scoreRemoved->setVoter(null);
        }

        $this->collScores = null;
        foreach ($scores as $score) {
            $this->addScore($score);
        }

        $this->collScores = $scores;
        $this->collScoresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Score objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Score objects.
     * @throws PropelException
     */
    public function countScores(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collScoresPartial && !$this->isNew();
        if (null === $this->collScores || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collScores) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getScores());
            }

            $query = ChildScoreQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByVoter($this)
                ->count($con);
        }

        return count($this->collScores);
    }

    /**
     * Method called to associate a ChildScore object to this object
     * through the ChildScore foreign key attribute.
     *
     * @param  ChildScore $l ChildScore
     * @return $this|\IiMedias\VoteBundle\Model\Voter The current object (for fluent API support)
     */
    public function addScore(ChildScore $l)
    {
        if ($this->collScores === null) {
            $this->initScores();
            $this->collScoresPartial = true;
        }

        if (!$this->collScores->contains($l)) {
            $this->doAddScore($l);

            if ($this->scoresScheduledForDeletion and $this->scoresScheduledForDeletion->contains($l)) {
                $this->scoresScheduledForDeletion->remove($this->scoresScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildScore $score The ChildScore object to add.
     */
    protected function doAddScore(ChildScore $score)
    {
        $this->collScores[]= $score;
        $score->setVoter($this);
    }

    /**
     * @param  ChildScore $score The ChildScore object to remove.
     * @return $this|ChildVoter The current object (for fluent API support)
     */
    public function removeScore(ChildScore $score)
    {
        if ($this->getScores()->contains($score)) {
            $pos = $this->collScores->search($score);
            $this->collScores->remove($pos);
            if (null === $this->scoresScheduledForDeletion) {
                $this->scoresScheduledForDeletion = clone $this->collScores;
                $this->scoresScheduledForDeletion->clear();
            }
            $this->scoresScheduledForDeletion[]= clone $score;
            $score->setVoter(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Voter is new, it will return
     * an empty collection; or if this Voter has previously
     * been saved, it will retrieve related Scores from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Voter.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildScore[] List of ChildScore objects
     */
    public function getScoresJoinContestCandidate(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildScoreQuery::create(null, $criteria);
        $query->joinWith('ContestCandidate', $joinBehavior);

        return $this->getScores($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->votvot_id = null;
        $this->votvot_username = null;
        $this->votvot_display_name = null;
        $this->votvot_country = null;
        $this->votvot_connection = null;
        $this->votvot_enable = null;
        $this->votvot_created_at = null;
        $this->votvot_updated_at = null;
        $this->votvot_slug = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collScores) {
                foreach ($this->collScores as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collScores = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(VoterTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildVoter The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[VoterTableMap::COL_VOTVOT_UPDATED_AT] = true;

        return $this;
    }

    // sluggable behavior

    /**
     * Wrap the setter for slug value
     *
     * @param   string
     * @return  $this|Voter
     */
    public function setSlug($v)
    {
        return $this->setVotvotSlug($v);
    }

    /**
     * Wrap the getter for slug value
     *
     * @return  string
     */
    public function getSlug()
    {
        return $this->getVotvotSlug();
    }

    /**
     * Create a unique slug based on the object
     *
     * @return string The object slug
     */
    protected function createSlug()
    {
        $slug = $this->createRawSlug();
        $slug = $this->limitSlugSize($slug);
        $slug = $this->makeSlugUnique($slug);

        return $slug;
    }

    /**
     * Create the slug from the appropriate columns
     *
     * @return string
     */
    protected function createRawSlug()
    {
        return '' . $this->cleanupSlugPart($this->getUsername()) . '';
    }

    /**
     * Cleanup a string to make a slug of it
     * Removes special characters, replaces blanks with a separator, and trim it
     *
     * @param     string $slug        the text to slugify
     * @param     string $replacement the separator used by slug
     * @return    string               the slugified text
     */
    protected static function cleanupSlugPart($slug, $replacement = '-')
    {
        // transliterate
        if (function_exists('iconv')) {
            $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
        }

        // lowercase
        if (function_exists('mb_strtolower')) {
            $slug = mb_strtolower($slug);
        } else {
            $slug = strtolower($slug);
        }

        // remove accents resulting from OSX's iconv
        $slug = str_replace(array('\'', '`', '^'), '', $slug);

        // replace non letter or digits with separator
        $slug = preg_replace('/[^\w\/]+/u', $replacement, $slug);

        // trim
        $slug = trim($slug, $replacement);

        if (empty($slug)) {
            return 'n-a';
        }

        return $slug;
    }


    /**
     * Make sure the slug is short enough to accommodate the column size
     *
     * @param    string $slug            the slug to check
     *
     * @return string                        the truncated slug
     */
    protected static function limitSlugSize($slug, $incrementReservedSpace = 3)
    {
        // check length, as suffix could put it over maximum
        if (strlen($slug) > (255 - $incrementReservedSpace)) {
            $slug = substr($slug, 0, 255 - $incrementReservedSpace);
        }

        return $slug;
    }


    /**
     * Get the slug, ensuring its uniqueness
     *
     * @param    string $slug            the slug to check
     * @param    string $separator       the separator used by slug
     * @param    int    $alreadyExists   false for the first try, true for the second, and take the high count + 1
     * @return   string                   the unique slug
     */
    protected function makeSlugUnique($slug, $separator = '-', $alreadyExists = false)
    {
        if (!$alreadyExists) {
            $slug2 = $slug;
        } else {
            $slug2 = $slug . $separator;
        }

        $adapter = \Propel\Runtime\Propel::getServiceContainer()->getAdapter('default');
        $col = 'q.VotvotSlug';
        $compare = $alreadyExists ? $adapter->compareRegex($col, '?') : sprintf('%s = ?', $col);

        $query = \IiMedias\VoteBundle\Model\VoterQuery::create('q')
            ->where($compare, $alreadyExists ? '^' . $slug2 . '[0-9]+$' : $slug2)
            ->prune($this)
        ;

        if (!$alreadyExists) {
            $count = $query->count();
            if ($count > 0) {
                return $this->makeSlugUnique($slug, $separator, true);
            }

            return $slug2;
        }

        $adapter = \Propel\Runtime\Propel::getServiceContainer()->getAdapter('default');
        // Already exists
        $object = $query
            ->addDescendingOrderByColumn($adapter->strLength('votvot_slug'))
            ->addDescendingOrderByColumn('votvot_slug')
        ->findOne();

        // First duplicate slug
        if (null == $object) {
            return $slug2 . '1';
        }

        $slugNum = substr($object->getVotvotSlug(), strlen($slug) + 1);
        if (0 == $slugNum[0]) {
            $slugNum[0] = 1;
        }

        return $slug2 . ($slugNum + 1);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
