<?php

namespace IiMedias\VoteBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VoteBundle\Model\Voter as ChildVoter;
use IiMedias\VoteBundle\Model\VoterQuery as ChildVoterQuery;
use IiMedias\VoteBundle\Model\Map\VoterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vote_voter_votvot' table.
 *
 *
 *
 * @method     ChildVoterQuery orderById($order = Criteria::ASC) Order by the votvot_id column
 * @method     ChildVoterQuery orderByUsername($order = Criteria::ASC) Order by the votvot_username column
 * @method     ChildVoterQuery orderByDisplayName($order = Criteria::ASC) Order by the votvot_display_name column
 * @method     ChildVoterQuery orderByCountry($order = Criteria::ASC) Order by the votvot_country column
 * @method     ChildVoterQuery orderByConnection($order = Criteria::ASC) Order by the votvot_connection column
 * @method     ChildVoterQuery orderByEnable($order = Criteria::ASC) Order by the votvot_enable column
 * @method     ChildVoterQuery orderByCreatedAt($order = Criteria::ASC) Order by the votvot_created_at column
 * @method     ChildVoterQuery orderByUpdatedAt($order = Criteria::ASC) Order by the votvot_updated_at column
 * @method     ChildVoterQuery orderByVotvotSlug($order = Criteria::ASC) Order by the votvot_slug column
 *
 * @method     ChildVoterQuery groupById() Group by the votvot_id column
 * @method     ChildVoterQuery groupByUsername() Group by the votvot_username column
 * @method     ChildVoterQuery groupByDisplayName() Group by the votvot_display_name column
 * @method     ChildVoterQuery groupByCountry() Group by the votvot_country column
 * @method     ChildVoterQuery groupByConnection() Group by the votvot_connection column
 * @method     ChildVoterQuery groupByEnable() Group by the votvot_enable column
 * @method     ChildVoterQuery groupByCreatedAt() Group by the votvot_created_at column
 * @method     ChildVoterQuery groupByUpdatedAt() Group by the votvot_updated_at column
 * @method     ChildVoterQuery groupByVotvotSlug() Group by the votvot_slug column
 *
 * @method     ChildVoterQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVoterQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVoterQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVoterQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVoterQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVoterQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVoterQuery leftJoinScore($relationAlias = null) Adds a LEFT JOIN clause to the query using the Score relation
 * @method     ChildVoterQuery rightJoinScore($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Score relation
 * @method     ChildVoterQuery innerJoinScore($relationAlias = null) Adds a INNER JOIN clause to the query using the Score relation
 *
 * @method     ChildVoterQuery joinWithScore($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Score relation
 *
 * @method     ChildVoterQuery leftJoinWithScore() Adds a LEFT JOIN clause and with to the query using the Score relation
 * @method     ChildVoterQuery rightJoinWithScore() Adds a RIGHT JOIN clause and with to the query using the Score relation
 * @method     ChildVoterQuery innerJoinWithScore() Adds a INNER JOIN clause and with to the query using the Score relation
 *
 * @method     \IiMedias\VoteBundle\Model\ScoreQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVoter findOne(ConnectionInterface $con = null) Return the first ChildVoter matching the query
 * @method     ChildVoter findOneOrCreate(ConnectionInterface $con = null) Return the first ChildVoter matching the query, or a new ChildVoter object populated from the query conditions when no match is found
 *
 * @method     ChildVoter findOneById(int $votvot_id) Return the first ChildVoter filtered by the votvot_id column
 * @method     ChildVoter findOneByUsername(string $votvot_username) Return the first ChildVoter filtered by the votvot_username column
 * @method     ChildVoter findOneByDisplayName(string $votvot_display_name) Return the first ChildVoter filtered by the votvot_display_name column
 * @method     ChildVoter findOneByCountry(string $votvot_country) Return the first ChildVoter filtered by the votvot_country column
 * @method     ChildVoter findOneByConnection(boolean $votvot_connection) Return the first ChildVoter filtered by the votvot_connection column
 * @method     ChildVoter findOneByEnable(boolean $votvot_enable) Return the first ChildVoter filtered by the votvot_enable column
 * @method     ChildVoter findOneByCreatedAt(string $votvot_created_at) Return the first ChildVoter filtered by the votvot_created_at column
 * @method     ChildVoter findOneByUpdatedAt(string $votvot_updated_at) Return the first ChildVoter filtered by the votvot_updated_at column
 * @method     ChildVoter findOneByVotvotSlug(string $votvot_slug) Return the first ChildVoter filtered by the votvot_slug column *

 * @method     ChildVoter requirePk($key, ConnectionInterface $con = null) Return the ChildVoter by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOne(ConnectionInterface $con = null) Return the first ChildVoter matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVoter requireOneById(int $votvot_id) Return the first ChildVoter filtered by the votvot_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOneByUsername(string $votvot_username) Return the first ChildVoter filtered by the votvot_username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOneByDisplayName(string $votvot_display_name) Return the first ChildVoter filtered by the votvot_display_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOneByCountry(string $votvot_country) Return the first ChildVoter filtered by the votvot_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOneByConnection(boolean $votvot_connection) Return the first ChildVoter filtered by the votvot_connection column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOneByEnable(boolean $votvot_enable) Return the first ChildVoter filtered by the votvot_enable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOneByCreatedAt(string $votvot_created_at) Return the first ChildVoter filtered by the votvot_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOneByUpdatedAt(string $votvot_updated_at) Return the first ChildVoter filtered by the votvot_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVoter requireOneByVotvotSlug(string $votvot_slug) Return the first ChildVoter filtered by the votvot_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVoter[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildVoter objects based on current ModelCriteria
 * @method     ChildVoter[]|ObjectCollection findById(int $votvot_id) Return ChildVoter objects filtered by the votvot_id column
 * @method     ChildVoter[]|ObjectCollection findByUsername(string $votvot_username) Return ChildVoter objects filtered by the votvot_username column
 * @method     ChildVoter[]|ObjectCollection findByDisplayName(string $votvot_display_name) Return ChildVoter objects filtered by the votvot_display_name column
 * @method     ChildVoter[]|ObjectCollection findByCountry(string $votvot_country) Return ChildVoter objects filtered by the votvot_country column
 * @method     ChildVoter[]|ObjectCollection findByConnection(boolean $votvot_connection) Return ChildVoter objects filtered by the votvot_connection column
 * @method     ChildVoter[]|ObjectCollection findByEnable(boolean $votvot_enable) Return ChildVoter objects filtered by the votvot_enable column
 * @method     ChildVoter[]|ObjectCollection findByCreatedAt(string $votvot_created_at) Return ChildVoter objects filtered by the votvot_created_at column
 * @method     ChildVoter[]|ObjectCollection findByUpdatedAt(string $votvot_updated_at) Return ChildVoter objects filtered by the votvot_updated_at column
 * @method     ChildVoter[]|ObjectCollection findByVotvotSlug(string $votvot_slug) Return ChildVoter objects filtered by the votvot_slug column
 * @method     ChildVoter[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VoterQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VoteBundle\Model\Base\VoterQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VoteBundle\\Model\\Voter', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVoterQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVoterQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildVoterQuery) {
            return $criteria;
        }
        $query = new ChildVoterQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVoter|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VoterTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VoterTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVoter A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT votvot_id, votvot_username, votvot_display_name, votvot_country, votvot_connection, votvot_enable, votvot_created_at, votvot_updated_at, votvot_slug FROM vote_voter_votvot WHERE votvot_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVoter $obj */
            $obj = new ChildVoter();
            $obj->hydrate($row);
            VoterTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildVoter|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the votvot_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE votvot_id = 1234
     * $query->filterById(array(12, 34)); // WHERE votvot_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE votvot_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(VoterTableMap::COL_VOTVOT_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(VoterTableMap::COL_VOTVOT_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_ID, $id, $comparison);
    }

    /**
     * Filter the query on the votvot_username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE votvot_username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE votvot_username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the votvot_display_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDisplayName('fooValue');   // WHERE votvot_display_name = 'fooValue'
     * $query->filterByDisplayName('%fooValue%', Criteria::LIKE); // WHERE votvot_display_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $displayName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByDisplayName($displayName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($displayName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_DISPLAY_NAME, $displayName, $comparison);
    }

    /**
     * Filter the query on the votvot_country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE votvot_country = 'fooValue'
     * $query->filterByCountry('%fooValue%', Criteria::LIKE); // WHERE votvot_country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the votvot_connection column
     *
     * Example usage:
     * <code>
     * $query->filterByConnection(true); // WHERE votvot_connection = true
     * $query->filterByConnection('yes'); // WHERE votvot_connection = true
     * </code>
     *
     * @param     boolean|string $connection The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByConnection($connection = null, $comparison = null)
    {
        if (is_string($connection)) {
            $connection = in_array(strtolower($connection), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_CONNECTION, $connection, $comparison);
    }

    /**
     * Filter the query on the votvot_enable column
     *
     * Example usage:
     * <code>
     * $query->filterByEnable(true); // WHERE votvot_enable = true
     * $query->filterByEnable('yes'); // WHERE votvot_enable = true
     * </code>
     *
     * @param     boolean|string $enable The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByEnable($enable = null, $comparison = null)
    {
        if (is_string($enable)) {
            $enable = in_array(strtolower($enable), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_ENABLE, $enable, $comparison);
    }

    /**
     * Filter the query on the votvot_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE votvot_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE votvot_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE votvot_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(VoterTableMap::COL_VOTVOT_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(VoterTableMap::COL_VOTVOT_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the votvot_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE votvot_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE votvot_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE votvot_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(VoterTableMap::COL_VOTVOT_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(VoterTableMap::COL_VOTVOT_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the votvot_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByVotvotSlug('fooValue');   // WHERE votvot_slug = 'fooValue'
     * $query->filterByVotvotSlug('%fooValue%', Criteria::LIKE); // WHERE votvot_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $votvotSlug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterByVotvotSlug($votvotSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($votvotSlug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_SLUG, $votvotSlug, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VoteBundle\Model\Score object
     *
     * @param \IiMedias\VoteBundle\Model\Score|ObjectCollection $score the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildVoterQuery The current query, for fluid interface
     */
    public function filterByScore($score, $comparison = null)
    {
        if ($score instanceof \IiMedias\VoteBundle\Model\Score) {
            return $this
                ->addUsingAlias(VoterTableMap::COL_VOTVOT_ID, $score->getVoterId(), $comparison);
        } elseif ($score instanceof ObjectCollection) {
            return $this
                ->useScoreQuery()
                ->filterByPrimaryKeys($score->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByScore() only accepts arguments of type \IiMedias\VoteBundle\Model\Score or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Score relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function joinScore($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Score');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Score');
        }

        return $this;
    }

    /**
     * Use the Score relation Score object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VoteBundle\Model\ScoreQuery A secondary query class using the current class as primary query
     */
    public function useScoreQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinScore($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Score', '\IiMedias\VoteBundle\Model\ScoreQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildVoter $voter Object to remove from the list of results
     *
     * @return $this|ChildVoterQuery The current query, for fluid interface
     */
    public function prune($voter = null)
    {
        if ($voter) {
            $this->addUsingAlias(VoterTableMap::COL_VOTVOT_ID, $voter->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vote_voter_votvot table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VoterTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VoterTableMap::clearInstancePool();
            VoterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VoterTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VoterTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VoterTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VoterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildVoterQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildVoterQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(VoterTableMap::COL_VOTVOT_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildVoterQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(VoterTableMap::COL_VOTVOT_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildVoterQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(VoterTableMap::COL_VOTVOT_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildVoterQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildVoterQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(VoterTableMap::COL_VOTVOT_CREATED_AT);
    }

    // sluggable behavior

    /**
     * Filter the query on the slug column
     *
     * @param     string $slug The value to use as filter.
     *
     * @return    $this|ChildVoterQuery The current query, for fluid interface
     */
    public function filterBySlug($slug)
    {
        return $this->addUsingAlias(VoterTableMap::COL_VOTVOT_SLUG, $slug, Criteria::EQUAL);
    }

    /**
     * Find one object based on its slug
     *
     * @param     string $slug The value to use as filter.
     * @param     ConnectionInterface $con The optional connection object
     *
     * @return    ChildVoter the result, formatted by the current formatter
     */
    public function findOneBySlug($slug, $con = null)
    {
        return $this->filterBySlug($slug)->findOne($con);
    }

} // VoterQuery
