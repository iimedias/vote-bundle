<?php

namespace IiMedias\VoteBundle\Model;

use IiMedias\VoteBundle\Model\Base\Contest as BaseContest;

/**
 * Skeleton subclass for representing a row from the 'pbf_contest_pbfcts' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Contest extends BaseContest
{
    private $candidateList = array();

    public function getCandidateList()
    {
        return $this->candidateList;
    }

    public function setCandidateList($candidateList)
    {
        $this->candidateList = $candidateList;
        return $this;
    }
}
