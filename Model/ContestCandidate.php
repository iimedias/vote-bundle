<?php

namespace IiMedias\VoteBundle\Model;

use IiMedias\VoteBundle\Model\Base\ContestCandidate as BaseContestCandidate;

/**
 * Skeleton subclass for representing a row from the 'pbf_contest_candidate_pbfccd' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ContestCandidate extends BaseContestCandidate
{

}
