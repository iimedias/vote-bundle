<?php

namespace IiMedias\VoteBundle\Model;

use IiMedias\VoteBundle\Model\Base\ContestQuery as BaseContestQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'pbf_contest_pbfcts' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ContestQuery extends BaseContestQuery
{

}
