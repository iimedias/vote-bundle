<?php

namespace IiMedias\VoteBundle\Model\Map;

use IiMedias\VoteBundle\Model\ContestCandidate;
use IiMedias\VoteBundle\Model\ContestCandidateQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'vote_contest_candidate_votccd' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ContestCandidateTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VoteBundle.Model.Map.ContestCandidateTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'vote_contest_candidate_votccd';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VoteBundle\\Model\\ContestCandidate';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VoteBundle.Model.ContestCandidate';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the votccd_id field
     */
    const COL_VOTCCD_ID = 'vote_contest_candidate_votccd.votccd_id';

    /**
     * the column name for the votccd_votcts_id field
     */
    const COL_VOTCCD_VOTCTS_ID = 'vote_contest_candidate_votccd.votccd_votcts_id';

    /**
     * the column name for the votccd_votcdd_id field
     */
    const COL_VOTCCD_VOTCDD_ID = 'vote_contest_candidate_votccd.votccd_votcdd_id';

    /**
     * the column name for the votccd_enable field
     */
    const COL_VOTCCD_ENABLE = 'vote_contest_candidate_votccd.votccd_enable';

    /**
     * the column name for the votccd_show_dance field
     */
    const COL_VOTCCD_SHOW_DANCE = 'vote_contest_candidate_votccd.votccd_show_dance';

    /**
     * the column name for the votccd_show_vote field
     */
    const COL_VOTCCD_SHOW_VOTE = 'vote_contest_candidate_votccd.votccd_show_vote';

    /**
     * the column name for the votccd_show_result field
     */
    const COL_VOTCCD_SHOW_RESULT = 'vote_contest_candidate_votccd.votccd_show_result';

    /**
     * the column name for the votccd_show_sort field
     */
    const COL_VOTCCD_SHOW_SORT = 'vote_contest_candidate_votccd.votccd_show_sort';

    /**
     * the column name for the votccd_scene_score field
     */
    const COL_VOTCCD_SCENE_SCORE = 'vote_contest_candidate_votccd.votccd_scene_score';

    /**
     * the column name for the votccd_technic_score field
     */
    const COL_VOTCCD_TECHNIC_SCORE = 'vote_contest_candidate_votccd.votccd_technic_score';

    /**
     * the column name for the votccd_couple_score field
     */
    const COL_VOTCCD_COUPLE_SCORE = 'vote_contest_candidate_votccd.votccd_couple_score';

    /**
     * the column name for the votccd_music_score field
     */
    const COL_VOTCCD_MUSIC_SCORE = 'vote_contest_candidate_votccd.votccd_music_score';

    /**
     * the column name for the votccd_style_score field
     */
    const COL_VOTCCD_STYLE_SCORE = 'vote_contest_candidate_votccd.votccd_style_score';

    /**
     * the column name for the votccd_total_score field
     */
    const COL_VOTCCD_TOTAL_SCORE = 'vote_contest_candidate_votccd.votccd_total_score';

    /**
     * the column name for the votccd_created_at field
     */
    const COL_VOTCCD_CREATED_AT = 'vote_contest_candidate_votccd.votccd_created_at';

    /**
     * the column name for the votccd_updated_at field
     */
    const COL_VOTCCD_UPDATED_AT = 'vote_contest_candidate_votccd.votccd_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ContestId', 'CandidateId', 'Enable', 'ShowDance', 'ShowVote', 'ShowResult', 'ShowSort', 'SceneScore', 'TechnicScore', 'CoupleScore', 'MusicScore', 'StyleScore', 'TotalScore', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'contestId', 'candidateId', 'enable', 'showDance', 'showVote', 'showResult', 'showSort', 'sceneScore', 'technicScore', 'coupleScore', 'musicScore', 'styleScore', 'totalScore', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ContestCandidateTableMap::COL_VOTCCD_ID, ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID, ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID, ContestCandidateTableMap::COL_VOTCCD_ENABLE, ContestCandidateTableMap::COL_VOTCCD_SHOW_DANCE, ContestCandidateTableMap::COL_VOTCCD_SHOW_VOTE, ContestCandidateTableMap::COL_VOTCCD_SHOW_RESULT, ContestCandidateTableMap::COL_VOTCCD_SHOW_SORT, ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE, ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE, ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE, ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE, ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE, ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE, ContestCandidateTableMap::COL_VOTCCD_CREATED_AT, ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('votccd_id', 'votccd_votcts_id', 'votccd_votcdd_id', 'votccd_enable', 'votccd_show_dance', 'votccd_show_vote', 'votccd_show_result', 'votccd_show_sort', 'votccd_scene_score', 'votccd_technic_score', 'votccd_couple_score', 'votccd_music_score', 'votccd_style_score', 'votccd_total_score', 'votccd_created_at', 'votccd_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ContestId' => 1, 'CandidateId' => 2, 'Enable' => 3, 'ShowDance' => 4, 'ShowVote' => 5, 'ShowResult' => 6, 'ShowSort' => 7, 'SceneScore' => 8, 'TechnicScore' => 9, 'CoupleScore' => 10, 'MusicScore' => 11, 'StyleScore' => 12, 'TotalScore' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'contestId' => 1, 'candidateId' => 2, 'enable' => 3, 'showDance' => 4, 'showVote' => 5, 'showResult' => 6, 'showSort' => 7, 'sceneScore' => 8, 'technicScore' => 9, 'coupleScore' => 10, 'musicScore' => 11, 'styleScore' => 12, 'totalScore' => 13, 'createdAt' => 14, 'updatedAt' => 15, ),
        self::TYPE_COLNAME       => array(ContestCandidateTableMap::COL_VOTCCD_ID => 0, ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID => 1, ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID => 2, ContestCandidateTableMap::COL_VOTCCD_ENABLE => 3, ContestCandidateTableMap::COL_VOTCCD_SHOW_DANCE => 4, ContestCandidateTableMap::COL_VOTCCD_SHOW_VOTE => 5, ContestCandidateTableMap::COL_VOTCCD_SHOW_RESULT => 6, ContestCandidateTableMap::COL_VOTCCD_SHOW_SORT => 7, ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE => 8, ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE => 9, ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE => 10, ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE => 11, ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE => 12, ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE => 13, ContestCandidateTableMap::COL_VOTCCD_CREATED_AT => 14, ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT => 15, ),
        self::TYPE_FIELDNAME     => array('votccd_id' => 0, 'votccd_votcts_id' => 1, 'votccd_votcdd_id' => 2, 'votccd_enable' => 3, 'votccd_show_dance' => 4, 'votccd_show_vote' => 5, 'votccd_show_result' => 6, 'votccd_show_sort' => 7, 'votccd_scene_score' => 8, 'votccd_technic_score' => 9, 'votccd_couple_score' => 10, 'votccd_music_score' => 11, 'votccd_style_score' => 12, 'votccd_total_score' => 13, 'votccd_created_at' => 14, 'votccd_updated_at' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('vote_contest_candidate_votccd');
        $this->setPhpName('ContestCandidate');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VoteBundle\\Model\\ContestCandidate');
        $this->setPackage('src.IiMedias.VoteBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('votccd_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('votccd_votcts_id', 'ContestId', 'INTEGER', 'vote_contest_votcts', 'votcts_id', true, null, null);
        $this->addForeignKey('votccd_votcdd_id', 'CandidateId', 'INTEGER', 'vote_candidate_votcdd', 'votcdd_id', true, null, null);
        $this->addColumn('votccd_enable', 'Enable', 'BOOLEAN', true, 1, true);
        $this->addColumn('votccd_show_dance', 'ShowDance', 'BOOLEAN', true, 1, false);
        $this->addColumn('votccd_show_vote', 'ShowVote', 'BOOLEAN', true, 1, false);
        $this->addColumn('votccd_show_result', 'ShowResult', 'BOOLEAN', true, 1, false);
        $this->addColumn('votccd_show_sort', 'ShowSort', 'BOOLEAN', true, 1, false);
        $this->addColumn('votccd_scene_score', 'SceneScore', 'INTEGER', true, null, 0);
        $this->addColumn('votccd_technic_score', 'TechnicScore', 'INTEGER', true, null, 0);
        $this->addColumn('votccd_couple_score', 'CoupleScore', 'INTEGER', true, null, 0);
        $this->addColumn('votccd_music_score', 'MusicScore', 'INTEGER', true, null, 0);
        $this->addColumn('votccd_style_score', 'StyleScore', 'INTEGER', true, null, 0);
        $this->addColumn('votccd_total_score', 'TotalScore', 'INTEGER', true, null, 0);
        $this->addColumn('votccd_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('votccd_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Contest', '\\IiMedias\\VoteBundle\\Model\\Contest', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':votccd_votcts_id',
    1 => ':votcts_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Candidate', '\\IiMedias\\VoteBundle\\Model\\Candidate', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':votccd_votcdd_id',
    1 => ':votcdd_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Score', '\\IiMedias\\VoteBundle\\Model\\Score', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':votsco_votccd_id',
    1 => ':votccd_id',
  ),
), 'CASCADE', 'CASCADE', 'Scores', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'votccd_created_at', 'update_column' => 'votccd_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to vote_contest_candidate_votccd     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ScoreTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ContestCandidateTableMap::CLASS_DEFAULT : ContestCandidateTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ContestCandidate object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ContestCandidateTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ContestCandidateTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ContestCandidateTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ContestCandidateTableMap::OM_CLASS;
            /** @var ContestCandidate $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ContestCandidateTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ContestCandidateTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ContestCandidateTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ContestCandidate $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ContestCandidateTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_ID);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_VOTCTS_ID);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_VOTCDD_ID);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_ENABLE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_SHOW_DANCE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_SHOW_VOTE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_SHOW_RESULT);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_SHOW_SORT);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_SCENE_SCORE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_TECHNIC_SCORE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_COUPLE_SCORE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_MUSIC_SCORE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_STYLE_SCORE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_TOTAL_SCORE);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_CREATED_AT);
            $criteria->addSelectColumn(ContestCandidateTableMap::COL_VOTCCD_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.votccd_id');
            $criteria->addSelectColumn($alias . '.votccd_votcts_id');
            $criteria->addSelectColumn($alias . '.votccd_votcdd_id');
            $criteria->addSelectColumn($alias . '.votccd_enable');
            $criteria->addSelectColumn($alias . '.votccd_show_dance');
            $criteria->addSelectColumn($alias . '.votccd_show_vote');
            $criteria->addSelectColumn($alias . '.votccd_show_result');
            $criteria->addSelectColumn($alias . '.votccd_show_sort');
            $criteria->addSelectColumn($alias . '.votccd_scene_score');
            $criteria->addSelectColumn($alias . '.votccd_technic_score');
            $criteria->addSelectColumn($alias . '.votccd_couple_score');
            $criteria->addSelectColumn($alias . '.votccd_music_score');
            $criteria->addSelectColumn($alias . '.votccd_style_score');
            $criteria->addSelectColumn($alias . '.votccd_total_score');
            $criteria->addSelectColumn($alias . '.votccd_created_at');
            $criteria->addSelectColumn($alias . '.votccd_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ContestCandidateTableMap::DATABASE_NAME)->getTable(ContestCandidateTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ContestCandidateTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ContestCandidateTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ContestCandidateTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ContestCandidate or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ContestCandidate object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestCandidateTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VoteBundle\Model\ContestCandidate) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ContestCandidateTableMap::DATABASE_NAME);
            $criteria->add(ContestCandidateTableMap::COL_VOTCCD_ID, (array) $values, Criteria::IN);
        }

        $query = ContestCandidateQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ContestCandidateTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ContestCandidateTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the vote_contest_candidate_votccd table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ContestCandidateQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ContestCandidate or Criteria object.
     *
     * @param mixed               $criteria Criteria or ContestCandidate object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestCandidateTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ContestCandidate object
        }

        if ($criteria->containsKey(ContestCandidateTableMap::COL_VOTCCD_ID) && $criteria->keyContainsValue(ContestCandidateTableMap::COL_VOTCCD_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ContestCandidateTableMap::COL_VOTCCD_ID.')');
        }


        // Set the correct dbName
        $query = ContestCandidateQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ContestCandidateTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ContestCandidateTableMap::buildTableMap();
