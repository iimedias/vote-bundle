<?php

namespace IiMedias\VoteBundle\Model\Map;

use IiMedias\VoteBundle\Model\Contest;
use IiMedias\VoteBundle\Model\ContestQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'vote_contest_votcts' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ContestTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VoteBundle.Model.Map.ContestTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'vote_contest_votcts';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VoteBundle\\Model\\Contest';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VoteBundle.Model.Contest';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the votcts_id field
     */
    const COL_VOTCTS_ID = 'vote_contest_votcts.votcts_id';

    /**
     * the column name for the votcts_name field
     */
    const COL_VOTCTS_NAME = 'vote_contest_votcts.votcts_name';

    /**
     * the column name for the votcts_current field
     */
    const COL_VOTCTS_CURRENT = 'vote_contest_votcts.votcts_current';

    /**
     * the column name for the votcts_enable field
     */
    const COL_VOTCTS_ENABLE = 'vote_contest_votcts.votcts_enable';

    /**
     * the column name for the votcts_created_at field
     */
    const COL_VOTCTS_CREATED_AT = 'vote_contest_votcts.votcts_created_at';

    /**
     * the column name for the votcts_updated_at field
     */
    const COL_VOTCTS_UPDATED_AT = 'vote_contest_votcts.votcts_updated_at';

    /**
     * the column name for the votcts_slug field
     */
    const COL_VOTCTS_SLUG = 'vote_contest_votcts.votcts_slug';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Current', 'Enable', 'CreatedAt', 'UpdatedAt', 'VotctsSlug', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'current', 'enable', 'createdAt', 'updatedAt', 'votctsSlug', ),
        self::TYPE_COLNAME       => array(ContestTableMap::COL_VOTCTS_ID, ContestTableMap::COL_VOTCTS_NAME, ContestTableMap::COL_VOTCTS_CURRENT, ContestTableMap::COL_VOTCTS_ENABLE, ContestTableMap::COL_VOTCTS_CREATED_AT, ContestTableMap::COL_VOTCTS_UPDATED_AT, ContestTableMap::COL_VOTCTS_SLUG, ),
        self::TYPE_FIELDNAME     => array('votcts_id', 'votcts_name', 'votcts_current', 'votcts_enable', 'votcts_created_at', 'votcts_updated_at', 'votcts_slug', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Current' => 2, 'Enable' => 3, 'CreatedAt' => 4, 'UpdatedAt' => 5, 'VotctsSlug' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'current' => 2, 'enable' => 3, 'createdAt' => 4, 'updatedAt' => 5, 'votctsSlug' => 6, ),
        self::TYPE_COLNAME       => array(ContestTableMap::COL_VOTCTS_ID => 0, ContestTableMap::COL_VOTCTS_NAME => 1, ContestTableMap::COL_VOTCTS_CURRENT => 2, ContestTableMap::COL_VOTCTS_ENABLE => 3, ContestTableMap::COL_VOTCTS_CREATED_AT => 4, ContestTableMap::COL_VOTCTS_UPDATED_AT => 5, ContestTableMap::COL_VOTCTS_SLUG => 6, ),
        self::TYPE_FIELDNAME     => array('votcts_id' => 0, 'votcts_name' => 1, 'votcts_current' => 2, 'votcts_enable' => 3, 'votcts_created_at' => 4, 'votcts_updated_at' => 5, 'votcts_slug' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('vote_contest_votcts');
        $this->setPhpName('Contest');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VoteBundle\\Model\\Contest');
        $this->setPackage('src.IiMedias.VoteBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('votcts_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('votcts_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('votcts_current', 'Current', 'BOOLEAN', true, 1, false);
        $this->addColumn('votcts_enable', 'Enable', 'BOOLEAN', true, 1, true);
        $this->addColumn('votcts_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('votcts_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('votcts_slug', 'VotctsSlug', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ContestCandidate', '\\IiMedias\\VoteBundle\\Model\\ContestCandidate', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':votccd_votcts_id',
    1 => ':votcts_id',
  ),
), 'CASCADE', 'CASCADE', 'ContestCandidates', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'votcts_created_at', 'update_column' => 'votcts_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'sluggable' => array('slug_column' => 'votcts_slug', 'slug_pattern' => '{Name}', 'replace_pattern' => '/[^\w\/]+/u', 'replacement' => '-', 'separator' => '-', 'permanent' => 'true', 'scope_column' => '', 'unique_constraint' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to vote_contest_votcts     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ContestCandidateTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ContestTableMap::CLASS_DEFAULT : ContestTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Contest object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ContestTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ContestTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ContestTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ContestTableMap::OM_CLASS;
            /** @var Contest $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ContestTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ContestTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ContestTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Contest $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ContestTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ContestTableMap::COL_VOTCTS_ID);
            $criteria->addSelectColumn(ContestTableMap::COL_VOTCTS_NAME);
            $criteria->addSelectColumn(ContestTableMap::COL_VOTCTS_CURRENT);
            $criteria->addSelectColumn(ContestTableMap::COL_VOTCTS_ENABLE);
            $criteria->addSelectColumn(ContestTableMap::COL_VOTCTS_CREATED_AT);
            $criteria->addSelectColumn(ContestTableMap::COL_VOTCTS_UPDATED_AT);
            $criteria->addSelectColumn(ContestTableMap::COL_VOTCTS_SLUG);
        } else {
            $criteria->addSelectColumn($alias . '.votcts_id');
            $criteria->addSelectColumn($alias . '.votcts_name');
            $criteria->addSelectColumn($alias . '.votcts_current');
            $criteria->addSelectColumn($alias . '.votcts_enable');
            $criteria->addSelectColumn($alias . '.votcts_created_at');
            $criteria->addSelectColumn($alias . '.votcts_updated_at');
            $criteria->addSelectColumn($alias . '.votcts_slug');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ContestTableMap::DATABASE_NAME)->getTable(ContestTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ContestTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ContestTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ContestTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Contest or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Contest object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VoteBundle\Model\Contest) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ContestTableMap::DATABASE_NAME);
            $criteria->add(ContestTableMap::COL_VOTCTS_ID, (array) $values, Criteria::IN);
        }

        $query = ContestQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ContestTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ContestTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the vote_contest_votcts table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ContestQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Contest or Criteria object.
     *
     * @param mixed               $criteria Criteria or Contest object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContestTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Contest object
        }

        if ($criteria->containsKey(ContestTableMap::COL_VOTCTS_ID) && $criteria->keyContainsValue(ContestTableMap::COL_VOTCTS_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ContestTableMap::COL_VOTCTS_ID.')');
        }


        // Set the correct dbName
        $query = ContestQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ContestTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ContestTableMap::buildTableMap();
