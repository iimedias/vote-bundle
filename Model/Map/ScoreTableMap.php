<?php

namespace IiMedias\VoteBundle\Model\Map;

use IiMedias\VoteBundle\Model\Score;
use IiMedias\VoteBundle\Model\ScoreQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'vote_score_votsco' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ScoreTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VoteBundle.Model.Map.ScoreTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'vote_score_votsco';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VoteBundle\\Model\\Score';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VoteBundle.Model.Score';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the votsco_id field
     */
    const COL_VOTSCO_ID = 'vote_score_votsco.votsco_id';

    /**
     * the column name for the votsco_votccd_id field
     */
    const COL_VOTSCO_VOTCCD_ID = 'vote_score_votsco.votsco_votccd_id';

    /**
     * the column name for the votsco_votvot_id field
     */
    const COL_VOTSCO_VOTVOT_ID = 'vote_score_votsco.votsco_votvot_id';

    /**
     * the column name for the votsco_scene_score field
     */
    const COL_VOTSCO_SCENE_SCORE = 'vote_score_votsco.votsco_scene_score';

    /**
     * the column name for the votsco_technic_score field
     */
    const COL_VOTSCO_TECHNIC_SCORE = 'vote_score_votsco.votsco_technic_score';

    /**
     * the column name for the votsco_couple_score field
     */
    const COL_VOTSCO_COUPLE_SCORE = 'vote_score_votsco.votsco_couple_score';

    /**
     * the column name for the votsco_music_score field
     */
    const COL_VOTSCO_MUSIC_SCORE = 'vote_score_votsco.votsco_music_score';

    /**
     * the column name for the votsco_style_score field
     */
    const COL_VOTSCO_STYLE_SCORE = 'vote_score_votsco.votsco_style_score';

    /**
     * the column name for the votsco_total_score field
     */
    const COL_VOTSCO_TOTAL_SCORE = 'vote_score_votsco.votsco_total_score';

    /**
     * the column name for the votsco_created_at field
     */
    const COL_VOTSCO_CREATED_AT = 'vote_score_votsco.votsco_created_at';

    /**
     * the column name for the votsco_updated_at field
     */
    const COL_VOTSCO_UPDATED_AT = 'vote_score_votsco.votsco_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ContestCandidateId', 'VoterId', 'SceneScore', 'TechnicScore', 'CoupleScore', 'MusicScore', 'StyleScore', 'TotalScore', 'VotscoCreatedAt', 'VotscoUpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'contestCandidateId', 'voterId', 'sceneScore', 'technicScore', 'coupleScore', 'musicScore', 'styleScore', 'totalScore', 'votscoCreatedAt', 'votscoUpdatedAt', ),
        self::TYPE_COLNAME       => array(ScoreTableMap::COL_VOTSCO_ID, ScoreTableMap::COL_VOTSCO_VOTCCD_ID, ScoreTableMap::COL_VOTSCO_VOTVOT_ID, ScoreTableMap::COL_VOTSCO_SCENE_SCORE, ScoreTableMap::COL_VOTSCO_TECHNIC_SCORE, ScoreTableMap::COL_VOTSCO_COUPLE_SCORE, ScoreTableMap::COL_VOTSCO_MUSIC_SCORE, ScoreTableMap::COL_VOTSCO_STYLE_SCORE, ScoreTableMap::COL_VOTSCO_TOTAL_SCORE, ScoreTableMap::COL_VOTSCO_CREATED_AT, ScoreTableMap::COL_VOTSCO_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('votsco_id', 'votsco_votccd_id', 'votsco_votvot_id', 'votsco_scene_score', 'votsco_technic_score', 'votsco_couple_score', 'votsco_music_score', 'votsco_style_score', 'votsco_total_score', 'votsco_created_at', 'votsco_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ContestCandidateId' => 1, 'VoterId' => 2, 'SceneScore' => 3, 'TechnicScore' => 4, 'CoupleScore' => 5, 'MusicScore' => 6, 'StyleScore' => 7, 'TotalScore' => 8, 'VotscoCreatedAt' => 9, 'VotscoUpdatedAt' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'contestCandidateId' => 1, 'voterId' => 2, 'sceneScore' => 3, 'technicScore' => 4, 'coupleScore' => 5, 'musicScore' => 6, 'styleScore' => 7, 'totalScore' => 8, 'votscoCreatedAt' => 9, 'votscoUpdatedAt' => 10, ),
        self::TYPE_COLNAME       => array(ScoreTableMap::COL_VOTSCO_ID => 0, ScoreTableMap::COL_VOTSCO_VOTCCD_ID => 1, ScoreTableMap::COL_VOTSCO_VOTVOT_ID => 2, ScoreTableMap::COL_VOTSCO_SCENE_SCORE => 3, ScoreTableMap::COL_VOTSCO_TECHNIC_SCORE => 4, ScoreTableMap::COL_VOTSCO_COUPLE_SCORE => 5, ScoreTableMap::COL_VOTSCO_MUSIC_SCORE => 6, ScoreTableMap::COL_VOTSCO_STYLE_SCORE => 7, ScoreTableMap::COL_VOTSCO_TOTAL_SCORE => 8, ScoreTableMap::COL_VOTSCO_CREATED_AT => 9, ScoreTableMap::COL_VOTSCO_UPDATED_AT => 10, ),
        self::TYPE_FIELDNAME     => array('votsco_id' => 0, 'votsco_votccd_id' => 1, 'votsco_votvot_id' => 2, 'votsco_scene_score' => 3, 'votsco_technic_score' => 4, 'votsco_couple_score' => 5, 'votsco_music_score' => 6, 'votsco_style_score' => 7, 'votsco_total_score' => 8, 'votsco_created_at' => 9, 'votsco_updated_at' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('vote_score_votsco');
        $this->setPhpName('Score');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VoteBundle\\Model\\Score');
        $this->setPackage('src.IiMedias.VoteBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('votsco_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('votsco_votccd_id', 'ContestCandidateId', 'INTEGER', 'vote_contest_candidate_votccd', 'votccd_id', true, null, null);
        $this->addForeignKey('votsco_votvot_id', 'VoterId', 'INTEGER', 'vote_voter_votvot', 'votvot_id', true, null, null);
        $this->addColumn('votsco_scene_score', 'SceneScore', 'INTEGER', true, null, 0);
        $this->addColumn('votsco_technic_score', 'TechnicScore', 'INTEGER', true, null, 0);
        $this->addColumn('votsco_couple_score', 'CoupleScore', 'INTEGER', true, null, 0);
        $this->addColumn('votsco_music_score', 'MusicScore', 'INTEGER', true, null, 0);
        $this->addColumn('votsco_style_score', 'StyleScore', 'INTEGER', true, null, 0);
        $this->addColumn('votsco_total_score', 'TotalScore', 'INTEGER', true, null, 0);
        $this->addColumn('votsco_created_at', 'VotscoCreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('votsco_updated_at', 'VotscoUpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ContestCandidate', '\\IiMedias\\VoteBundle\\Model\\ContestCandidate', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':votsco_votccd_id',
    1 => ':votccd_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Voter', '\\IiMedias\\VoteBundle\\Model\\Voter', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':votsco_votvot_id',
    1 => ':votvot_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'votsco_created_at', 'update_column' => 'votsco_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ScoreTableMap::CLASS_DEFAULT : ScoreTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Score object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ScoreTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ScoreTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ScoreTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ScoreTableMap::OM_CLASS;
            /** @var Score $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ScoreTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ScoreTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ScoreTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Score $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ScoreTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_ID);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_VOTCCD_ID);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_VOTVOT_ID);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_SCENE_SCORE);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_TECHNIC_SCORE);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_COUPLE_SCORE);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_MUSIC_SCORE);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_STYLE_SCORE);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_TOTAL_SCORE);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_CREATED_AT);
            $criteria->addSelectColumn(ScoreTableMap::COL_VOTSCO_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.votsco_id');
            $criteria->addSelectColumn($alias . '.votsco_votccd_id');
            $criteria->addSelectColumn($alias . '.votsco_votvot_id');
            $criteria->addSelectColumn($alias . '.votsco_scene_score');
            $criteria->addSelectColumn($alias . '.votsco_technic_score');
            $criteria->addSelectColumn($alias . '.votsco_couple_score');
            $criteria->addSelectColumn($alias . '.votsco_music_score');
            $criteria->addSelectColumn($alias . '.votsco_style_score');
            $criteria->addSelectColumn($alias . '.votsco_total_score');
            $criteria->addSelectColumn($alias . '.votsco_created_at');
            $criteria->addSelectColumn($alias . '.votsco_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ScoreTableMap::DATABASE_NAME)->getTable(ScoreTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ScoreTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ScoreTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ScoreTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Score or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Score object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ScoreTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VoteBundle\Model\Score) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ScoreTableMap::DATABASE_NAME);
            $criteria->add(ScoreTableMap::COL_VOTSCO_ID, (array) $values, Criteria::IN);
        }

        $query = ScoreQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ScoreTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ScoreTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the vote_score_votsco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ScoreQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Score or Criteria object.
     *
     * @param mixed               $criteria Criteria or Score object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ScoreTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Score object
        }

        if ($criteria->containsKey(ScoreTableMap::COL_VOTSCO_ID) && $criteria->keyContainsValue(ScoreTableMap::COL_VOTSCO_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ScoreTableMap::COL_VOTSCO_ID.')');
        }


        // Set the correct dbName
        $query = ScoreQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ScoreTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ScoreTableMap::buildTableMap();
