<?php

namespace IiMedias\VoteBundle\Model\Map;

use IiMedias\VoteBundle\Model\Voter;
use IiMedias\VoteBundle\Model\VoterQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'vote_voter_votvot' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class VoterTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VoteBundle.Model.Map.VoterTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'vote_voter_votvot';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VoteBundle\\Model\\Voter';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VoteBundle.Model.Voter';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the votvot_id field
     */
    const COL_VOTVOT_ID = 'vote_voter_votvot.votvot_id';

    /**
     * the column name for the votvot_username field
     */
    const COL_VOTVOT_USERNAME = 'vote_voter_votvot.votvot_username';

    /**
     * the column name for the votvot_display_name field
     */
    const COL_VOTVOT_DISPLAY_NAME = 'vote_voter_votvot.votvot_display_name';

    /**
     * the column name for the votvot_country field
     */
    const COL_VOTVOT_COUNTRY = 'vote_voter_votvot.votvot_country';

    /**
     * the column name for the votvot_connection field
     */
    const COL_VOTVOT_CONNECTION = 'vote_voter_votvot.votvot_connection';

    /**
     * the column name for the votvot_enable field
     */
    const COL_VOTVOT_ENABLE = 'vote_voter_votvot.votvot_enable';

    /**
     * the column name for the votvot_created_at field
     */
    const COL_VOTVOT_CREATED_AT = 'vote_voter_votvot.votvot_created_at';

    /**
     * the column name for the votvot_updated_at field
     */
    const COL_VOTVOT_UPDATED_AT = 'vote_voter_votvot.votvot_updated_at';

    /**
     * the column name for the votvot_slug field
     */
    const COL_VOTVOT_SLUG = 'vote_voter_votvot.votvot_slug';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Username', 'DisplayName', 'Country', 'Connection', 'Enable', 'CreatedAt', 'UpdatedAt', 'VotvotSlug', ),
        self::TYPE_CAMELNAME     => array('id', 'username', 'displayName', 'country', 'connection', 'enable', 'createdAt', 'updatedAt', 'votvotSlug', ),
        self::TYPE_COLNAME       => array(VoterTableMap::COL_VOTVOT_ID, VoterTableMap::COL_VOTVOT_USERNAME, VoterTableMap::COL_VOTVOT_DISPLAY_NAME, VoterTableMap::COL_VOTVOT_COUNTRY, VoterTableMap::COL_VOTVOT_CONNECTION, VoterTableMap::COL_VOTVOT_ENABLE, VoterTableMap::COL_VOTVOT_CREATED_AT, VoterTableMap::COL_VOTVOT_UPDATED_AT, VoterTableMap::COL_VOTVOT_SLUG, ),
        self::TYPE_FIELDNAME     => array('votvot_id', 'votvot_username', 'votvot_display_name', 'votvot_country', 'votvot_connection', 'votvot_enable', 'votvot_created_at', 'votvot_updated_at', 'votvot_slug', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Username' => 1, 'DisplayName' => 2, 'Country' => 3, 'Connection' => 4, 'Enable' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, 'VotvotSlug' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'username' => 1, 'displayName' => 2, 'country' => 3, 'connection' => 4, 'enable' => 5, 'createdAt' => 6, 'updatedAt' => 7, 'votvotSlug' => 8, ),
        self::TYPE_COLNAME       => array(VoterTableMap::COL_VOTVOT_ID => 0, VoterTableMap::COL_VOTVOT_USERNAME => 1, VoterTableMap::COL_VOTVOT_DISPLAY_NAME => 2, VoterTableMap::COL_VOTVOT_COUNTRY => 3, VoterTableMap::COL_VOTVOT_CONNECTION => 4, VoterTableMap::COL_VOTVOT_ENABLE => 5, VoterTableMap::COL_VOTVOT_CREATED_AT => 6, VoterTableMap::COL_VOTVOT_UPDATED_AT => 7, VoterTableMap::COL_VOTVOT_SLUG => 8, ),
        self::TYPE_FIELDNAME     => array('votvot_id' => 0, 'votvot_username' => 1, 'votvot_display_name' => 2, 'votvot_country' => 3, 'votvot_connection' => 4, 'votvot_enable' => 5, 'votvot_created_at' => 6, 'votvot_updated_at' => 7, 'votvot_slug' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('vote_voter_votvot');
        $this->setPhpName('Voter');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VoteBundle\\Model\\Voter');
        $this->setPackage('src.IiMedias.VoteBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('votvot_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('votvot_username', 'Username', 'VARCHAR', true, 255, null);
        $this->addColumn('votvot_display_name', 'DisplayName', 'VARCHAR', false, 255, null);
        $this->addColumn('votvot_country', 'Country', 'VARCHAR', false, 255, null);
        $this->addColumn('votvot_connection', 'Connection', 'BOOLEAN', true, 1, null);
        $this->addColumn('votvot_enable', 'Enable', 'BOOLEAN', true, 1, null);
        $this->addColumn('votvot_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('votvot_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('votvot_slug', 'VotvotSlug', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Score', '\\IiMedias\\VoteBundle\\Model\\Score', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':votsco_votvot_id',
    1 => ':votvot_id',
  ),
), 'CASCADE', 'CASCADE', 'Scores', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'votvot_created_at', 'update_column' => 'votvot_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'sluggable' => array('slug_column' => 'votvot_slug', 'slug_pattern' => '{Username}', 'replace_pattern' => '/[^\w\/]+/u', 'replacement' => '-', 'separator' => '-', 'permanent' => 'true', 'scope_column' => '', 'unique_constraint' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to vote_voter_votvot     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ScoreTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? VoterTableMap::CLASS_DEFAULT : VoterTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Voter object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = VoterTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = VoterTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + VoterTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = VoterTableMap::OM_CLASS;
            /** @var Voter $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            VoterTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = VoterTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = VoterTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Voter $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                VoterTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_ID);
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_USERNAME);
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_DISPLAY_NAME);
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_COUNTRY);
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_CONNECTION);
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_ENABLE);
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_CREATED_AT);
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_UPDATED_AT);
            $criteria->addSelectColumn(VoterTableMap::COL_VOTVOT_SLUG);
        } else {
            $criteria->addSelectColumn($alias . '.votvot_id');
            $criteria->addSelectColumn($alias . '.votvot_username');
            $criteria->addSelectColumn($alias . '.votvot_display_name');
            $criteria->addSelectColumn($alias . '.votvot_country');
            $criteria->addSelectColumn($alias . '.votvot_connection');
            $criteria->addSelectColumn($alias . '.votvot_enable');
            $criteria->addSelectColumn($alias . '.votvot_created_at');
            $criteria->addSelectColumn($alias . '.votvot_updated_at');
            $criteria->addSelectColumn($alias . '.votvot_slug');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(VoterTableMap::DATABASE_NAME)->getTable(VoterTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(VoterTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(VoterTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new VoterTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Voter or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Voter object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VoterTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VoteBundle\Model\Voter) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(VoterTableMap::DATABASE_NAME);
            $criteria->add(VoterTableMap::COL_VOTVOT_ID, (array) $values, Criteria::IN);
        }

        $query = VoterQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            VoterTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                VoterTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the vote_voter_votvot table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return VoterQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Voter or Criteria object.
     *
     * @param mixed               $criteria Criteria or Voter object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VoterTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Voter object
        }

        if ($criteria->containsKey(VoterTableMap::COL_VOTVOT_ID) && $criteria->keyContainsValue(VoterTableMap::COL_VOTVOT_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.VoterTableMap::COL_VOTVOT_ID.')');
        }


        // Set the correct dbName
        $query = VoterQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // VoterTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
VoterTableMap::buildTableMap();
